package main

import "core:fmt"
import "core:mem"

// NOTE (sio): these are unchanged from their C++ versions for the moment
// NOTE (sio): the original code is largely uncommented and I'm unsure I understand the intent properly
base_allocate__inner :: proc(
    allocator: ^Base_Allocator,
    size: u64,
    location: String_Const_u8
    ) -> (result: Raw_Slice)
{
    result.data =
        allocator.reserve(
            allocator.user_data,
            size,
            transmute(^u64) &result.len,
            location
            );
    allocator.commit(allocator.user_data, result.data, u64(result.len))
    return
}

base_free :: proc(allocator: ^Base_Allocator, ptr: rawptr) {
    if ptr != nil {
        allocator.free(allocator.user_data, ptr)
    }
}

arena__new_node :: proc(
    arena: ^Arena,
    min_size: u64,
    location: String_Const_u8
    ) -> (cursor_node: [^]Cursor_Node)
{
    min_size: u64 = max(min_size, arena.chunk_size)
    memory :=
        base_allocate__inner(
            arena.base_allocator,
            min_size + size_of(Cursor_Node),
            location
            )
    cursor_node = transmute([^]Cursor_Node) memory.data
    assert(memory.len >= size_of(Cursor_Node))
    cursor_node[0].cursor = {
        transmute([^]u8) &(cursor_node[1]),
        0,
        u64(memory.len - size_of(Cursor_Node))
    }
    
    // NOTE: sll_push, written out
    cursor_node[0].prev = arena.cursor_node
    arena.cursor_node = transmute(^Cursor_Node) cursor_node
    
    return
}

linalloc_push_arena :: proc(
    arena: ^Arena,
    size: u64,
    location: String_Const_u8
    ) -> (result: Raw_Slice)
{
    if size > 0 {
        cursor_node: ^Cursor_Node = arena.cursor_node
        if cursor_node == nil {
            cursor_node = arena__new_node(arena, size, location)
        }
        
        result = linalloc_push(&(cursor_node.cursor), size, location)
        if result.data == nil {
            cursor_node = arena__new_node(arena, size, location)
            result = linalloc_push(&(cursor_node.cursor), size, location)
        }
        
        alignment_data := linalloc_align(&(cursor_node.cursor), arena.alignment)
        result.len += alignment_data.len
    }
    return
}

linalloc_pop_arena :: proc(arena: ^Arena, size: u64) {
    size := size
    allocator: ^Base_Allocator = arena.base_allocator
    cursor_node: ^Cursor_Node = arena.cursor_node
    for prev: ^Cursor_Node = nil; cursor_node != nil && size != 0; cursor_node = prev {
        prev = cursor_node.prev
        if size >= cursor_node.cursor.pos {
            size -= cursor_node.cursor.pos
            base_free(allocator, cursor_node)
        } else {
            linalloc_pop(&(cursor_node.cursor), size)
            break
        }
    }
    arena.cursor_node = cursor_node
}

linalloc_align_arena :: proc(arena: ^Arena, alignment: u64) -> (rv: Raw_Slice) {
    arena.alignment = alignment
    cursor_node: ^Cursor_Node = arena.cursor_node
    if cursor_node != nil {
        rv = linalloc_align(&(cursor_node.cursor), arena.alignment)
    }
    return
}

linalloc_push_cursor :: proc(
    cursor: ^Cursor,
    size: u64,
    location: String_Const_u8
    ) -> (result: Raw_Slice)
{
    if cursor.pos + size <= cursor.cap {
        result.data = &(cursor.base[cursor.pos])
        result.len = int(size)
        cursor.pos += size
    }
    
    return
}

linalloc_pop_cursor :: proc(cursor: ^Cursor, size: u64) {
    if cursor.pos > size {
        cursor.pos -= size
    } else {
        cursor.pos = 0
    }
}

linalloc_align_cursor :: proc(cursor: ^Cursor, alignment: u64) -> Raw_Slice {
    pos: u64 = u64(mem.align_forward_uint(uint(cursor.pos), uint(alignment)))
    new_size: u64 = pos - cursor.pos
        return linalloc_push_cursor(cursor, new_size, #procedure)
}

linalloc_push :: proc { linalloc_push_arena, linalloc_push_cursor }
linalloc_pop :: proc { linalloc_pop_arena, linalloc_pop_cursor }
linalloc_align :: proc { linalloc_align_arena, linalloc_align_cursor }
