package main

import "core:c/libc"
import "core:dynlib"
import "core:fmt"
import "core:math/bits"
import "core:mem"
import "core:os"
import rt "core:runtime"
import "core:strings"
import "core:time"

import "vendor:sdl2"

// NOTE (sio): needed to execve(2) properly. Defined by either Odin or the C++ side,
// but exists in the program already, somewhere.
// refer to environ(7) for documentation if needed
// mostly used through get_env/set_env

foreign import syslibc "system:c"

// do something for each _valid_ stored key/value/hash triple (i.e. one for which the hash isn't 0)
map_foreach :: proc(m: $T/map[$K]$V, fn: proc(data: uintptr, k: ^K, v: ^V, h: ^uintptr), data: uintptr) {
    keys, values, hashes := rt.map_kvh_data_static(m)
    keys_per_cell := len(keys[0].data)
    values_per_cell := len(values[0].data)

    capacity := cap(m)
    for idx := 0; idx < capacity; idx += 1 {
        hash := hashes[idx]
        if hash != 0 {
            keys_cell_idx := idx / keys_per_cell
            keys_into_cell_idx := idx % keys_per_cell

            values_cell_idx := idx / values_per_cell
            values_into_cell_idx := idx % values_per_cell

            k: ^K = &(keys[keys_cell_idx].data[keys_into_cell_idx])
            v: ^V = &(values[values_cell_idx].data[values_into_cell_idx])
            h: ^uintptr = &(hashes[idx])

            fn(data, k, v, h)
        }
    }
}

error_box :: proc(
    message: string, 
    title: string = "",
    do_quit: bool = true,
    flags: sdl2.MessageBoxFlags = { .ERROR }
    )
{
    title := title
    if title == "" {
        title = message
    }
    
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    
    ctitle := strings.clone_to_cstring(title, allocator = context.temp_allocator)
    cmsg := strings.clone_to_cstring(message, allocator = context.temp_allocator)
    
    ret_val: i32 = sdl2.ShowSimpleMessageBox({ .ERROR }, ctitle, cmsg, nil)
    if ret_val < 0 {
        fmt.eprintln("Failed to show message box with title", title, "and message", message, "requesting quit:", do_quit, "; reason:", sdl2.GetError())
        os.exit(1)
    }
    
    if do_quit {
        os.exit(1)
    }
}

slice_to_cxx_slice :: proc(slice: $S/[]$T, first: ^[^]T, last: ^[^]T, count: ^i32) {
    raw := transmute(Raw_Slice) slice
   
    // length should fit inside a signed 32-bit integer
    assert(raw.len <= bits.I32_MAX)
    
    // length should never be negative
    assert(raw.len >= 0)
    
    // either data is valid and length is arbitrary,
    // OR data is a null pointer and length is 0
    assert(raw.data != nil || (raw.data == nil && raw.len == 0))
    
    count^ = i32(raw.len)
   
    first^ = transmute([^]$T) raw.data
    if raw.len > 0 {
        // TODO: test whether last is like C++ end(), i.e. one past the end, or if it is genuinely the last. I'm going with the safer interpretation, here
        last^ = &((transmute([^]$T) raw.data)[raw.len - 1])
    } else {
        last^ = transmute([^]$T) raw.data
    }
    
    return
}

arena_alloc_func :: proc(
    allocator_data: rawptr, mode: rt.Allocator_Mode,
    size: int, alignment: int,
    old_memory: rawptr, old_size: int,
    location := #caller_location
    ) -> (mu8: []u8, err: rt.Allocator_Error)
{
    arena := transmute(^Arena) allocator_data
    switch mode {
        case .Alloc, .Alloc_Non_Zeroed:
            if size < 0 {
                err = .Invalid_Argument
                return
            }
        
            linalloc_align_arena(arena, u64(alignment if alignment > 0 else 1))
            memory :=
                linalloc_push_arena(arena, u64(size), String_Const_u8(location.procedure))
            if memory.data == nil {
                err = .Out_Of_Memory
                return
            }
        
            if mode == .Alloc {
                mem.zero(memory.data, memory.len)
            }
        
            mu8 = transmute([]u8) memory
        
        case .Free:
            err = .Mode_Not_Implemented
        
        case .Free_All:
            cnode: ^Cursor_Node = arena.cursor_node
            for cnode != nil {
                nnode := cnode.prev
                base_free(arena.base_allocator, transmute(rawptr) cnode)
                cnode = nnode
            }
        
        case .Resize:
            err = .Mode_Not_Implemented
        
        case .Query_Features:
            set := transmute(^rt.Allocator_Mode_Set) old_memory
            if set != nil {
                set^ = { .Alloc, .Alloc_Non_Zeroed, .Free_All, .Query_Features }
            }

        case .Query_Info:
            err = .Mode_Not_Implemented
    }        
    
    return
}

odin_alloc_from_arena :: proc(arena: ^Arena) -> rt.Allocator {
    return rt.Allocator { procedure = arena_alloc_func, data = transmute(rawptr) arena }
}

Ordering :: enum u8 { A_GT_B, A_EQ_B, A_LT_B }

compare_file_infos :: proc(a: ^os.File_Info, b: ^os.File_Info) -> (res: Ordering) {
    a_hidden := a.name[0] == '.'
    b_hidden := b.name[0] == '.'
    
    if a_hidden != b_hidden {
        if !a_hidden {
            res = .A_GT_B
        } else {
            res = .A_LT_B
        }
        return
    }
    
    context.allocator = context.temp_allocator
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    
    acstr := strings.clone_to_cstring(a.name)
    bcstr := strings.clone_to_cstring(b.name)
    
    strcoll_res := libc.strcoll(acstr, bcstr)
    
    if strcoll_res < 0 {
        res = .A_LT_B
    } else if strcoll_res > 0 {
        res = .A_GT_B
    } else {
        res = .A_EQ_B
    }
    
    return
}

odin_file_info_to_internal_file_info :: proc(
    arena: ^Arena,
    info: os.File_Info,
    next_to_set: ^File_Info = nil
    ) -> (out: File_Info)
{
    using out
    using attributes
    
    next = next_to_set
    // NOTE: assuming it's name we do want. Might be this should be full path, but I think it's just name
    file_name =
        String_Const_u8(
            strings.clone(
                string(info.name),
                odin_alloc_from_arena(arena)
                )
            )
    
    size = u64(info.size)
    last_write_time = u64(info.modification_time._nsec / 1000)
    flags = .IsDirectory if info.is_dir else File_Attribute_Flag(0)
    
    return
}

odin_time_to_date_time :: proc(intime: time.Time) -> (dt: Date_Time) {
    using dt
    
    yyyy, MM, dd := time.date(intime)
    hh, mm, ss := time.clock_from_time(intime)
    
    year = u32(yyyy)
    mon = u8(MM)
    day = u8(dd)
    hour = u8(hh)
    min = u8(mm)
    sec = u8(ss)
    
    without_nsec, ok :=
        time.datetime_to_time(int(yyyy), int(mm), int(dd), int(hh), int(mm), int(ss))
    assert(ok)
    
    msec = u16(time.duration_nanoseconds(time.diff(without_nsec, intime)) / 1000000)
    
    return
}

date_time_to_odin_time :: proc(intime: Date_Time) -> (t: time.Time, ok: bool) {
    using intime
    t, ok =
        time.datetime_to_time(
            int(year), int(mon), int(day),
            int(hour), int(min), int(sec),
            int(msec) * 1000 * 1000
            )
   return        
}

// returns offset in seconds
// NOTE: positive east of the greenwich meridian, negative west of it
// TODO (sio): figure out if this works fine on Windows
localtime_offset :: proc () -> i64 {
    
    @(default_calling_convention="cdecl")
    foreign syslibc {
        // environ: [^]cstring
        @(link_name="timelocal")
        localttt :: proc(tm: ^libc.tm) -> libc.time_t ---
        timegm :: proc(tm: ^libc.tm) -> libc.time_t ---
    }
    
    tmt := i64(time.time_to_unix(time.now()))
    tlocal := i64(localttt(transmute(^libc.tm) &tmt))
    tutc := i64(timegm(transmute(^libc.tm) &tmt))
    return tlocal - tutc
}

default_temp_allocator_temp_begin :: rt.default_temp_allocator_temp_begin
default_temp_allocator_temp_end :: rt.default_temp_allocator_temp_end
