#!/bin/sh

set -u
set -e

source $stdenv/setup

export PATH=$stdenv/bin:$pkg-config/bin:$binutils/bin:$gnumake/bin:$clang/bin:$odin/bin:$ispc/bin:$cmake/bin:$ninja/bin:$fontconfig/bin:$PATH
export LD_LIBRARY_PATH=$pkg-config/lib:$SDL2/lib:$glew/lib:$libX11/lib:$compiler-rt/lib:$libXfixes/lib:$freetype/lib:$fontconfig/lib:$stdenv/lib:$LD_LIBRARY_PATH
export INCLUDE_PATHS=$SDL2/include:$glew/include:$libX11/include:$compiler-rt/include:$libXfixes/include:$freetype/include:$fontconfig/include

echo "copying src to working directory and setting working directory read-write"
cp -R $src $(pwd)
cd *-sgedit
chmod -R a+rw $(pwd)

export CC=`which clang`
export CMAKE_C_COMPILER=$CC

export CXX=`which clang++`
export CMAKE_CXX_COMPILER=$CXX

export ODIN=`which odin`
export CMAKE_ODIN_COMPILER=$ODIN

echo "configuring"
./configure.sh

echo "building"
./build.sh

echo "moving/installing"
./install.sh

echo "cleaning up install directories"
rm -rf $out/bin/sgedit

echo "making sure install directories are present"
mkdir -p $out
mkdir -p $out/bin

echo "moving binaries and data to install directories"
mv bin/sgedit $out/bin/sgedit

chmod -R a+rx $out
