/*
* Mr. 4th Dimention - Allen Webster
*
* 23.07.2019
*
* Type for organizating the set of all loaded font faces.
*
*/

// Sio Kreuzer, 2024-01-28: this file and it's corresponding .cpp file have been
// largely discarded in the Odin rewrite.

// TOP

#if !defined(FRED_FONT_SET_H)
#define FRED_FONT_SET_H

#ifndef AUTO_CAST_IMPL
#define AUTO_CAST_IMPL
// NOTE (sio): horrible hack
template<typename T, typename U>
void auto_cast(T* x, U** y) {
    *y = (U*) x;
}
#endif // AUTO_CAST_IMPL

#include "odin_types.h"

struct Font_Set {
    Face_ID next_id_counter;
    Odin_Dynamic_Array id_freelist;
    Odin_Dynamic_Array id_to_face_table;
    f32 scale_factor; // possibly referred to, but not in custom
};

static Face_ID (*font_set__alloc_face_id)(Font_Set *set) = nullptr;

static void (*font_set__free_face_id)(Font_Set *set, Face_ID id) = nullptr;

static void (*font_set_init)(Font_Set *set) = nullptr;

static Face* (*font_set_new_face)(Font_Set *set, Face_Description *description) = nullptr;

static b32 (*font_set_release_face)(Font_Set *set, Face_ID id) = nullptr;

static Face* (*font_set_face_from_id)(Font_Set *set, Face_ID id) = nullptr;

static Face_ID (*font_set_get_fallback_face)(Font_Set *set) = nullptr;

static Face_ID (*font_set_get_largest_id)(Font_Set *set) = nullptr;

static b32 (*font_set_modify_face)(Font_Set *set, Face_ID id, Face_Description *description) = nullptr;

extern "C" void font_set_setup(
    void* fs_alloc,
    void* fs_free,
    void* fs_init,
    void* fs_new,
    void* fs_release,
    void* from_id,
    void* fallback,
    void* largest,
    void* modify
    )
{
    auto_cast(fs_alloc, &font_set__alloc_face_id);
    auto_cast(fs_free, &font_set__free_face_id);
    auto_cast(fs_init, &font_set_init);
    auto_cast(fs_new, &font_set_new_face);
    auto_cast(fs_release, &font_set_release_face);
    auto_cast(from_id, &font_set_face_from_id);
    auto_cast(fallback, &font_set_get_fallback_face);
    auto_cast(largest, &font_set_get_largest_id);
    auto_cast(modify, &font_set_modify_face);
}

#endif

// BOTTOM

