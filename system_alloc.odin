package main

import "core:mem"
import rt "core:runtime"
import "core:strings"
import "core:sync"

// TODO (sio): have a way of easily showing the system_memory_annotation stuff
// perhaps a special kind of buffer?

// max. allocation count: 25K (this shouldn't ever be reached, realistically)
System_Allocator :: struct {
    allocations: ^Memory_Annotation_Node, // slab allocated/string_allocator allocated
    allocations_slab: Slab(Memory_Annotation_Node, rt.Megabyte),
    string_allocator: rt.Allocator,
    allocations_mutex: sync.Mutex,
}

system_allocator_init :: proc(
    string_alloc: rt.Allocator
    ) -> (sysalloc: System_Allocator, err: rt.Allocator_Error)
{
    sysalloc.string_allocator = string_alloc
    sysalloc.allocations_slab, err = make_slab(type_of(sysalloc.allocations_slab))
    return
}

system_allocator_destroy :: proc(sysalloc: ^System_Allocator) {
    if sysalloc.allocations != nil {
        log_os(
            "Freeing system allocator, but there are non-freed allocations left.",
            "\nLEAKING THE FOLLOWING MEMORY ALLOCATIONS TO PREVENT A SEGFAULT:"
            )
        for node := sysalloc.allocations; node != nil; node = node.next {
            log_os(node^)
            mem.delete_string(string(node.location), allocator = sysalloc.string_allocator)
        }
        log_os(
            "\nA note to the user: the OS should clear these allocations up.",
            "\nThis is a warning for developers."
            )
    }
    
    sysalloc.allocations = nil
    free_slab(&sysalloc.allocations_slab)
}

System_Memory_Allocate_Sig :: #type proc "cdecl"(
    size: u64,
    location: String_Const_u8
    ) -> rawptr
system_memory_allocate :: proc "cdecl" (size: u64, location: String_Const_u8) -> rawptr {
    context = default_context()
    slice, err := system_allocator_allocate(&(system_globals.system_allocator), size, location)
    return slice.data // works because will always be set to nil on error
}

system_allocator_allocate :: proc(
    sysalloc: ^System_Allocator,
    size: u64,
    location: String_Const_u8
    ) -> (raw_mem: Raw_Slice, err: rt.Allocator_Error)
{
    using sysalloc
    
    u8m: []u8
    u8m, err = virtual_alloc(size)
    if err != .None {
        log_os(
            "Failed to allocate",
            size,
            "bytes. Location:",
            location,
            "; got error",
            err
            )
        return
    }
    
    raw_mem = transmute(Raw_Slice) u8m
    
    defer if err != .None {
        virtual_free(raw_mem.data, u64(raw_mem.len))
        raw_mem.data = nil
        raw_mem.len = 0
    }
    
    if sync.mutex_guard(&allocations_mutex) {
        annotation: ^Memory_Annotation_Node
        annotation, err = slab_alloc(&allocations_slab)
        if err != .None {
            log_os(
                   "Failed to allocate memory annotation for allocation with size",
                   size,
                   "bytes, location",
                   location,
                   "; got error",
                   err
                   )
            return
        }
        
        defer if err != .None {
            slab_free(&allocations_slab, annotation)
        }
        
        annot_location: string
        annot_location, err = strings.clone(string(location), allocator = string_allocator)
        if err != .None {
            log_os(
                "Failed to clone location string",
                location,
                "for memory annotation for allocation with size",
                size,
                "; got error",
                err
                )
            return
        }
        
        // NOTE (sio): we have to keep holding the mutex until end-of-procedure
        // in order to do cleanup properly
        defer if err != .None {
            log_if(
                rt.Allocator_Error.None,
                mem.delete_string(annot_location, allocator = string_allocator),
                "failed to deallocate memory annotation location string"
                )
            annotation.location = ""
            allocations = annotation.next
        }
        
        annotation.location = String_Const_u8(annot_location)
        annotation.size     = u64(raw_mem.len) // actual size
        annotation.address  = raw_mem.data
        annotation.next     = allocations
    
        allocations = annotation
    }
    
    return
}

System_Memory_Set_Protection_Sig :: #type proc "cdecl"(
    ptr: rawptr,
    size: u64,
    flags: u32 // or'd together from Mem_Protect
    ) -> b32
system_memory_set_protection :: proc "cdecl" (ptr: rawptr, size: u64, flags: u32) -> b32 {
    /*
    context = default_context()
    
    prot_flags: virtual.Protect_Flags = {}
    if 0 != (flags & u32(Access_Flag.Read)) {
        prot_flags += { .Read }
    }
    
    if 0 != (flags & u32(Access_Flag.Write)) {
        prot_flags += { .Write }
    }
    
    if 0 != (flags & u32(Access_Flag.Exec)) {
        prot_flags += { .Execute }
    }
    
    return b32(virtual.protect(ptr, uint(size), prot_flags))*/
    return true
}

System_Memory_Free_Sig :: #type proc "cdecl" (ptr: rawptr, size: u64)

// NOTE (sio): we ignore the size argument because it may not accurately represent the size
// that was actually allocated
system_memory_free :: proc "cdecl" (ptr: rawptr, size: u64) {
    context = default_context()
        
    err := system_allocator_free(&(system_globals.system_allocator), ptr)
        
    log_if(
        rt.Allocator_Error.None,
        system_allocator_free(&(system_globals.system_allocator), ptr),
        "failed to free allocation at address",
        ptr,
        "with specified size",
        size,
        "using system allocator"
        )
}

system_allocator_free :: proc(
    sysalloc: ^System_Allocator,
    ptr: rawptr
    ) -> rt.Allocator_Error
{ 
    using sysalloc
        
    annotation: ^Memory_Annotation_Node
        
    if sync.mutex_guard(&allocations_mutex) {
        prev_annot := allocations
        for annot := allocations; annot != nil; {
            if annot.address == ptr {
                annotation = annot
            }
            prev_annot = annot
            annot = annot.next
        }
        
        if annotation == nil { // we didn't allocate this
            log_os(
                #procedure,
                "potential double free/free of memory not allocated by system allocator",
                "at address",
                ptr
                )
            return .Invalid_Pointer
        }
    
        // knit the list back together
        if prev_annot == annotation { // start of list, no previous element
            allocations = annotation.next
        } else { // prev exists
            prev_annot.next = annotation.next
        }
    }
    
    // NOTE (sio): annotation will be valid past this point
    assert(annotation != nil)
    annotation.next = nil
        
    virtual_free(annotation.address, u64(annotation.size))
        
    log_if(
        rt.Allocator_Error.None,
        mem.delete_string(string(annotation.location), allocator = string_allocator),
        "failed to deallocate memory annotation location string"
        )
    log_if(
        rt.Allocator_Error.None,
        slab_free(&allocations_slab, annotation),
        "failed to deallocate memory annotation"
        )
    
    return .None
}

System_Memory_Annotation_Sig :: #type proc "cdecl" (arena: ^Arena) -> Memory_Annotation
system_memory_annotation :: proc "cdecl" (arena: ^Arena) -> (ma: Memory_Annotation) {
    context = default_context()
    using system_globals.system_allocator
    
    nodes: []Memory_Annotation_Node
    if sync.mutex_guard(&allocations_mutex) {
        count := 0
        for annotation := allocations; annotation != nil; annotation = annotation.next {
            count += 1
        }
        
        err: rt.Allocator_Error
        nodes, err =
            make([]Memory_Annotation_Node, count, allocator = odin_alloc_from_arena(arena))
        if err != .None {
            log_os(
                "Failed to allocate space for memory annotations from provided arena:", 
                 err
                 )
            return
        }
        
        // copy
        {
            annotation := allocations
            index := 0
            for index < count && annotation != nil {
                nodes[index] = annotation^
                if index < count - 1 {
                    nodes[index].next = &(nodes[index + 1])
                } else {
                    nodes[index].next = nil
                }
                
                index += 1
                annotation = annotation.next
            }
        }
    }
    
    raw_nodes := transmute(Raw_Slice) nodes
    ma.first = transmute([^]Memory_Annotation_Node) raw_nodes.data
    ma.last = &(ma.first[raw_nodes.len - 1])
    ma.count = i32(raw_nodes.len)
    
    return
}

system_allocator_odin_alloc_proc :: proc(
    priv_data: rawptr,
    mode: rt.Allocator_Mode,
    size: int, alignment: int,
    old_memory: rawptr, old_size: int,
    location := #caller_location
    ) -> (result: []u8, err: rt.Allocator_Error)
{
    sysalloc := transmute(^System_Allocator) priv_data;
    
    switch mode {
        case .Alloc, .Alloc_Non_Zeroed:
            assert(size > 0)
        
            slice: Raw_Slice
            slice, err =
                system_allocator_allocate(
                    sysalloc,
                    u64(size),
                    String_Const_u8(location.procedure)
                    )
            if err != .None {
                return
            }
        
            // NOTE (sio): zeroed allocation MAY cause all pages to become fully allocated
            // on platforms that don't support kernel same page merging (because well, that
            // is how we're handling the possibility of this case)
            if mode != .Alloc_Non_Zeroed {
                mem.zero(slice.data, slice.len)
            }
        
        case .Free:
            err = system_allocator_free(sysalloc, old_memory)
            
        case .Free_All, .Resize: // , .Resize_Non_Zeroed:
            err = .Mode_Not_Implemented
            
        case .Query_Features:
            set := transmute(^rt.Allocator_Mode_Set) old_memory
                if set != nil {
                    set^ = {
                        .Alloc, .Alloc_Non_Zeroed,
                        .Free,
                        .Query_Features,
                        .Query_Info
                 }
             }
        
        case .Query_Info:
            info := transmute(^rt.Allocator_Query_Info) old_memory;
            if info != nil && info.pointer != nil {
                annotation: ^Memory_Annotation_Node;
                for annot := sysalloc.allocations; annot != nil; annot = annot.next {
                    if annot.address == info.pointer {
                        annotation = annot
                    }
                }
                if annotation != nil {
                    info.size = int(annotation.size)
                    // everything allocated using this allocator is page-aligned
                    info.alignment = int(DEFAULT_PAGE_SIZE)
                    raw_info: Raw_Slice = {
                        data = transmute(rawptr) info,
                        len = size_of(info^)
                    }
                    result = transmute([]u8) raw_info
                } else {
                    err = .Invalid_Pointer
                }
            }
    }
    
    return
}

system_allocator_to_odin_allocator :: proc "contextless" (
    sysalloc: ^System_Allocator
    ) -> rt.Allocator
{
    return rt.Allocator {
        procedure = system_allocator_odin_alloc_proc,
        data = rawptr(sysalloc)
    }
}
