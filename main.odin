// right now, this just replaces the platform layer in function. More functionality *will* be folded into it, though.
package main

import "core:dynlib"
import "core:fmt"
import "core:mem"
import "core:os"
import rt "core:runtime"
import "core:strings"
import "core:sync"
import "core:thread"
import "core:time"

import gl "vendor:OpenGL"
import "vendor:sdl2"

when ODIN_OS == .Linux {
    foreign import app "libsgedit_app.a"
} else { // assume Windows
    // TODO (sio): figure out if this is correct
    foreign import app "sgedit_app.lib"
}

// NOTE (sio) version decl lives both here and elsewhere simultaneously, which is a bit dodgy tbh?
MAJOR :: 0
MINOR :: 1
PATCH :: 0

foreign app {
    @(link_name="app_get_functions")
    app_get_functions :: proc "cdecl" () -> App_Functions ---
        
    @(link_name="get_base_allocator_system")
    get_base_allocator_system :: proc "cdecl" () -> ^Base_Allocator ---
        
    @(link_name="thread_ctx_init")
    thread_ctx_init :: proc "cdecl" (
        tctx: ^Thread_Context,
        kind: Thread_Kind,
        allocator: ^Base_Allocator,
        prof_allocator: ^Base_Allocator
        ) ---
        
    @(link_name="codepoint_index_map_setup")
    codepoint_index_map_setup :: proc "cdecl" (
        map_read: rawptr,
        map_count: rawptr,
        glyph_advance: rawptr,
        max_glyph_advance: rawptr,
        average_glyph_advance: rawptr
        ) ---
        
    @(link_name="font_set_setup")
    font_set_setup :: proc "cdecl" (
        fs_alloc: rawptr,
        fs_free: rawptr,
        fs_init: rawptr,
        fs_new: rawptr,
        fs_release: rawptr,
        from_id: rawptr,
        fallback: rawptr,
        largest: rawptr,
        modify: rawptr
        ) ---
}

// TODO (sio): allow for multiple windows; this shouldn't be too hard, all in all.
// Mild adjustments to input handling, as well as output I think?
// ah, right. Multi-Window OpenGL was a pain. Possible, but a massive pain.
// So I should probably switch to Vulkan if I wanna do this?
//
// NOTE (sio): it's apparently possible to do both of:
// 1. multi-window with one GL context per window
// 2. multi-window with one GL context for all windows
// It should also be possible to have GL calls happen outside the main thread,
// but I'm unsure how safe or workable this is. Though in any case,
// I think I can likely shift most work out of the main thread.

lnx_override_user_directory: Maybe(String_Const_u8)

when true {
    Object_Allocator :: struct {
        alloc_count: u64,
    }
    
    init_object_allocator :: proc() {}
    destroy_object_allocator :: proc() {}
    
    mutex_alloc :: proc() -> (mtx: System_Mutex) {
        using system_globals.object_allocator
        u8m, err := rt.mem_alloc(size_of(sync.Mutex), allocator = rt.default_allocator())
        assert(err == .None)
        mtx.mutex = transmute(^sync.Mutex) raw_data(u8m)
        alloc_count += 1
        return
    }
    
    mutex_free :: proc(mutex: System_Mutex) {
        free(mutex.mutex, allocator = rt.default_allocator())
    }
    
    cond_alloc :: proc() -> (scv: System_Condition_Variable) {
        using system_globals.object_allocator
        u8m, err := rt.mem_alloc(size_of(sync.Cond), allocator = rt.default_allocator())
        assert(err == .None)
        scv.cond = transmute(^sync.Cond) raw_data(u8m)
        alloc_count += 1
        return
    }
    
    cond_free :: proc(cond: System_Condition_Variable) {
        free(cond.cond, allocator = rt.default_allocator())
    }
    
    timer_alloc :: proc() -> (st: System_Timer) {
        using system_globals.object_allocator
        err: rt.Allocator_Error
        st.fd, err = new(i32, allocator = rt.default_allocator())
        assert(err == .None)
        alloc_count += 1
        return
    }
    
    timer_free :: proc(timer: System_Timer) {
        free(timer.fd, allocator = rt.default_allocator())
    }
} else {
// slab allocator
Object_Allocator :: struct {
    mtx: sync.Mutex,
    mutexes: Slab(sync.Mutex),
    conds: Slab(sync.Cond),
    // no slab for threads, Odin runtime handles that
    timers: Slab(i32),
}

// TODO: slab allocators are fucky (FIXME)
init_object_allocator :: proc() {
    using system_globals.object_allocator
    err: rt.Allocator_Error
    
    mutexes, err = make_slab(type_of(mutexes))
    assert(err == .None)
    
    conds, err = make_slab(type_of(conds))
    assert(err == .None)
    
    timers, err = make_slab(type_of(timers))
    assert(err == .None)
}

destroy_object_allocator :: proc() {
    using system_globals.object_allocator
    free_slab(&mutexes)
    free_slab(&conds)
    free_slab(&timers)
}

mutex_alloc :: proc() -> (mutex: System_Mutex) {
    using system_globals.object_allocator
    if sync.mutex_guard(&mtx) {
        err: rt.Allocator_Error
        mutex.mutex, err = slab_alloc(&mutexes)
        assert(err == .None)
    }
    return
}

mutex_free :: proc(mutex: System_Mutex) {
    using system_globals.object_allocator
    if sync.mutex_guard(&mtx) {
        err := slab_free(&mutexes, mutex.mutex)
        assert(err == .None)
    }
}

cond_alloc :: proc() -> (cond: System_Condition_Variable) {
    using system_globals.object_allocator
    if sync.mutex_guard(&mtx) {
        err: rt.Allocator_Error
        cond.cond, err = slab_alloc(&conds)
        assert(err == .None)
    }
    return
}

cond_free :: proc(cond: System_Condition_Variable) {
    using system_globals.object_allocator
    if sync.mutex_guard(&mtx) {
        err := slab_free(&conds, cond.cond)
        assert(err == .None)
    }
}

timer_alloc :: proc() -> (timer: System_Timer) {
    using system_globals.object_allocator
    if sync.mutex_guard(&mtx) {
        err: rt.Allocator_Error
        timer.fd, err = slab_alloc(&timers)
        assert(err == .None)
    }
    return
}

timer_free :: proc(timer: System_Timer) {
    using system_globals.object_allocator
    if sync.mutex_guard(&mtx) {
        err := slab_free(&timers, timer.fd)
        assert(err == .None)
    }
}
}

Vars :: struct {
    tctx: Thread_Context,

    window: ^sdl2.Window,
    window_title: cstring,
    window_id: u32,
    gl_context: sdl2.GLContext,
    
    cursors: [Application_Mouse_Cursor]^sdl2.Cursor,
    current_cursor: Application_Mouse_Cursor,
    
    input: Input_Chunk,
    prev_filtered_key: KeyCode,
    key_mode: Key_Mode,
    
    // Mutexes, Condition Variables, Timers
    object_allocator: Object_Allocator,
    
    // global allocator handed through, virtual memory-backed, with allocation tracking
    system_allocator: System_Allocator,
    
    plat_settings: Plat_Settings,
    
    global_frame_mutex: sync.Mutex,
    
    // NOTE (sio): we ignore all clipboard events and just let SDL handle that stuff,
    // since it saves it internally anyway and we don't need to duplicate that work

    log_string: Log_Function,
    
    app: App_Functions,
    custom: Custom_API,
    custom_lib: dynlib.Library,
}

default_context :: proc "contextless" () -> (ctx: rt.Context) {
    ctx = rt.default_context()
    ctx.allocator = get_system_allocator()
    return
}

get_system_allocator :: proc "contextless" () -> rt.Allocator {
    return system_allocator_to_odin_allocator(&(system_globals.system_allocator))
}

init_cursors :: proc() {
    using system_globals;
    
    tbl: [sdl2.SystemCursor]Application_Mouse_Cursor = #partial {
        .ARROW = .ARROW,
        .IBEAM = .IBEAM,
        .SIZEWE = .LEFTRIGHT,
        .SIZENS = .UPDOWN,
    }
        
    cursor_types := [?]sdl2.SystemCursor {
        .ARROW, .IBEAM, .SIZEWE, .SIZENS
    }
    for type in cursor_types {
        idx := tbl[type]
        cursors[idx] = sdl2.CreateSystemCursor(type)
        if cursors[idx] == nil {
            log_os("Failed to get system cursor of systm type", type, "and 4c type", idx)
        }
    }
    cursors[.DEFAULT] = cursors[.ARROW]
    
    set_cursor(.DEFAULT)
    sdl2.ShowCursor(sdl2.ENABLE)
}

set_cursor :: proc(type: Application_Mouse_Cursor) {
    using system_globals;
    sdl2.SetCursor(cursors[type])
    current_cursor = type
    
    // NOTE (sio): force cursor redraw for testing purposes.
    // TODO (sio): figure out if this is really necessary, and if not: remove
    sdl2.SetCursor(nil)
}

system_globals: Vars
render_target: Render_Target

system_vtable: API_VTable_system
graphics_vtable: API_VTable_graphics
font_vtable: API_VTable_font
custom_vtable: API_VTable_custom

main:: proc() {
    args := transmute(rt.Raw_Slice) rt.args__
    argc := i32(args.len)
    argv := transmute([^]cstring) args.data
    using system_globals

    // TODO (sio): only enable log_os if -L is in command line params
    log_os_enabled = true

    // ensure SDL2 can be used anywhere in the program by loading it first (we do not want this configurable anyway)
    if 0 != sdl2.Init({.TIMER, .VIDEO, .EVENTS}) {
        error_box(fmt.tprintln("Failed to initialize SDL2:", sdl2.GetError()))
    }
    
    init_object_allocator()
    defer destroy_object_allocator()
    
    {
        err: rt.Allocator_Error
        system_allocator, err = system_allocator_init(rt.default_allocator())
        
        if err != .None {
            log_os("Failed to initialize system allocator:", err)
            os.exit(1)
        }
    }
    defer system_allocator_destroy(&system_allocator)
        
    {
        alloc: ^Base_Allocator = get_base_allocator_system();
        //thread_ctx_init(&tctx, .Main, alloc, alloc)
        
        // TODO (sio): BUG: thread_ctx_init doesn't actually setup thread context properly
        // this is likely the C++ compiler misbehaving
        using tctx;
        kind = .Main;
        
        allocator = alloc;
        node_arena = Arena { alloc, nil, 4 * rt.Kilobyte, 8 };
        used_first = nil;
        used_last = nil;
        free_arenas = nil;
        
        prof_allocator = alloc;
        prof_id_counter = 1;
        prof_arena = Arena { alloc, nil, 16 * rt.Kilobyte, 8 };;
        prof_first = nil;
        prof_last = nil;
        prof_record_count = 0;
        
        user_data = nil;
    }

    system_api_fill_vtable(&system_vtable)
    graphics_api_fill_vtable(&graphics_vtable)
    font_api_fill_vtable(&font_vtable)
        
    // setup font api for app
    font_setup_api(codepoint_index_map_setup, font_set_setup)
        
    // NOTE: no reason to load app library, we directly link it statically and just produce per-OS executables
     
    app = app_get_functions()

    app.load_vtables(&system_vtable, &font_vtable, &graphics_vtable)
        
    log_string = app.get_logger()

    base_ptr: rawptr
    // have the app read the command line
    {
        alloc := context.allocator
        context.allocator = context.temp_allocator
        defer context.allocator = alloc

        save_point := default_temp_allocator_temp_begin()
        defer default_temp_allocator_temp_end(save_point)
            
        curdir: String_Const_u8 = String_Const_u8(os.get_current_directory())
        files: [^]cstring
        file_count: [^]i32
        base_ptr =
            app.read_command_line(
                &tctx, 
                curdir,
                &plat_settings,
                &files,
                &file_count,
                argc,
                argv
                )
        // TODO (sio): I do believe we should actually... open and show the files?
        // TODO (sio): Do we do that? find out
    }

    if plat_settings.user_directory != nil {
        lnx_override_user_directory =
            String_Const_u8(strings.clone(string(plat_settings.user_directory)))
    }

    
    // custom := Custom_API { init_apis = init_apis, get_version = get_version }
    // TODO (sio): do this more properly
    {
        ok: bool;
        custom_lib, ok = load_lib("build/custom_sgedit.so");
        if !ok {
            custom_lib, ok = load_lib("custom_sgedit.so");
        }
        
        assert(ok);
        
        addr: rawptr;
        addr, ok = dynlib.symbol_address(custom_lib, "init_apis");
        assert(ok && addr != nil);
        if addr < rawptr(uintptr(0x1000)) {
            fmt.eprintln("got symbol address", addr, "for init_apis in custom_sgedit.so, dlerror says", os.dlerror())
        }
        
        custom.init_apis = transmute(Init_APIs_Type) addr;
        
        addr, ok = dynlib.symbol_address(custom_lib, "get_version");
        assert(ok && addr != nil);
        
        custom.get_version = transmute(Get_Version_Type) addr;
        
        font_setup_api_in_lib(custom_lib);
    }
    
    /*
    @(link_name="init_apis")
        init_apis :: proc "cdecl"(custom: ^API_VTable_custom, system: ^API_VTable_system) -> Custom_Layer_Init_Type ---
        
        @(link_name="get_version")
        get_version :: proc "cdecl" (maj: i32, min: i32, pat: i32) -> b32 ---
    */
    if !init_render_window() {
        return
    }
    defer destroy_render_window()
        
    window_id = sdl2.GetWindowID(window)
        
    // app init
    {
        // TODO: does this do custom API init and custom vtable fill?
        save_point := default_temp_allocator_temp_begin()
        defer default_temp_allocator_temp_end(save_point)
            
        alloc := context.allocator
        context.allocator = context.temp_allocator
        defer context.allocator = alloc
        
        curdir: String_Const_u8 =
            String_Const_u8(os.get_current_directory())
        
        app.init(&tctx, &render_target, base_ptr, curdir, &custom)
    }
    
    ticks: time.Tick = time.tick_now()
    
    frame_lap_time :: proc(t: ^time.Tick) -> f64 {
        d := time.tick_lap_time(t)
        if time.duration_nanoseconds(d) > 100000 { // 100 microseconds
            return time.duration_seconds(d)
        } else { // <=100usec
            return 1.0 / 60
            // NOTE (sio): pretend this is a 60Hz display for just the first frame so
            // we have a useful frametime delta immediately
        }
    }
    
    input_init()
        
    init_cursors()
        
    first_step: b32 = true
        
    sync.mutex_lock(&global_frame_mutex)
    main_loop: for {
        temp_alloc_frame_save_point := default_temp_allocator_temp_begin()
        defer default_temp_allocator_temp_end(temp_alloc_frame_save_point)
        
        system_process_input_events()

        step_input: Application_Step_Input
        step_input.first_step = first_step
        first_step = false
            
        {
            using step_input
            using input

            dt = f32(frame_lap_time(&ticks))
            events = trans.event_list
            trying_to_kill = trans.trying_to_kill
            mouse.out_of_window = pers.mouse_out_of_window
            mouse.p = pers.mouse
            mouse.l = pers.mouse_l
            mouse.r = pers.mouse_r
            mouse.press_l = trans.mouse_l_press
            mouse.release_l = trans.mouse_l_release
            mouse.press_r = trans.mouse_r_press
            mouse.release_r = trans.mouse_r_release
            mouse.wheel = trans.mouse_wheel
        }
        
        step_result: Application_Step_Result
        if app.step != nil {
            step_result = app.step(&tctx, &render_target, base_ptr, &step_input)
        }

        if step_result.perform_kill {
            break main_loop;
        }

        if step_result.has_new_title {
            old_window_title := window_title
            
            // NOTE (sio): okay, yeah, this is a bit obtuse, but it should work fine
            window_title = strings.clone_to_cstring(string(step_result.title_string))
            
            sdl2.SetWindowTitle(window, window_title)
            
            delete(old_window_title)
        }

        if step_result.mouse_cursor_type != current_cursor /*&& input.pers.mouse_l != 0*/ {
            set_cursor(step_result.mouse_cursor_type)
        }

        render(&render_target)
            
        // release global frame mutex while we wait for vsync
        sync.mutex_unlock(&global_frame_mutex)

        
        sdl2.GL_SwapWindow(window)
            
        sync.mutex_lock(&global_frame_mutex)

        if step_result.animating {
            //schedule_step() // TODO: timer step?
            // NOTE (sio) I think this timer business here is literally just vsync emulation
        }
        
        mem.zero(transmute(rawptr) &input.trans, size_of(input.trans))
    }
    
    // TODO (sio): is there more cleanup we need to run? If so, goes here
    
    // ensure there's at least *some* cleanup run properly
    destroy_render_window()
}
