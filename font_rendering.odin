package main

import "core:math"
import "core:mem"
import "core:mem/virtual"
import "core:os"
import rt "core:runtime"
import "core:strings"

import "vendor:stb/truetype"

import ft "freetype"

// TODO (sio): just use stb_tt, render... uh. Dunno. Need to figure that out. It should
// support hinting and not just antialiasing via prerendered atlas 
// NOTE (sio): prerendered atlas is a method that is honestly perfectly adequate for all
// languages I use (English, German, Bulgarian, Chinese), and since I despise ligatures
// and dislike non-fixed-width fonts... I could take the easy route, since this is just for
// my use, especially since all languages I use are reasonably strictly left-to-right...
// NOTE (sio): if you want to do text editing in e.g. Thai or Arabic or want to use
// non-fixed-width fonts and/or ligatures I recommend using HarfBuzz and a better
// rendering method here. PRs to this effect are welcome, as well (as long as I can turn
// ligatures off somehow)

// TODO: ensure the app can actually see all these symbols correctly

font_set_init :: proc "cdecl" (set: ^Font_Set) {
    context = default_context()
    mem.zero(rawptr(set), size_of(Font_Set))
    set.next_id_counter = 1
        
    err: rt.Allocator_Error
    set.id_freelist, err =
        rt.make_dynamic_array_len_cap(
            type_of(set.id_freelist),
            len = 0, cap = virtual.DEFAULT_PAGE_SIZE / size_of(Face_ID),
            allocator = get_system_allocator()
            )
    if err != .None {
        log_os(
            "Failed to allocate id_freelist of font_set at",
            transmute(rawptr) set,
            "; got error",
            err
            )
        os.exit(1)
    }
    
    slot_count := virtual.DEFAULT_PAGE_SIZE / size_of(Face)
    set.id_to_face_table, err =
        rt.make_dynamic_array_len_cap(
            type_of(set.id_to_face_table),
            len = slot_count, cap = slot_count,
            allocator = get_system_allocator()
            )
    if err != .None {
        log_os(
               "Failed to allocate id_to_face_table of font_set at",
               transmute(rawptr) set,
               "; got error",
               err
               )
            os.exit(1)
    }
    set.scale_factor = system_get_screen_scale_factor()
}


font_set__alloc_face_id :: proc "cdecl" (set: ^Font_Set) -> (id: Face_ID) {
    context = default_context()
    using set
    if len(id_freelist) > 0 {
        id = rt.pop(&id_freelist)
    } else {
        id = next_id_counter
        next_id_counter += 1
    }
    return
}


font_set__free_face_id :: proc "cdecl"(set: ^Font_Set, id: Face_ID) {
    context = default_context()
    using set
    if id + 1 == next_id_counter {
        next_id_counter -= 1
    } else {
        rt.append(&id_freelist, id)
    }
}


font_set_new_face :: proc "cdecl" (
    set: ^Font_Set,
    description: ^Face_Description
    ) -> ^Face
{
    context = default_context()
    using set
    
    face, face_ok := font_make_face(description, scale_factor)
    
    if face_ok {
        new_id := font_set__alloc_face_id(set)
        face.id = new_id
        id_to_face_table[int(new_id)] = face
    } else {
        face = nil
    }
    
    return face
}


font_set_release_face :: proc "cdecl" (
    set: ^Font_Set,
    id: Face_ID
    ) -> (result: b32 = b32false)
{
    context = default_context()
    using set
    facep := font_set_faceptr_from_id(set, id)
    if facep != nil {
        facep^ = nil
        font_set__free_face_id(set, id)
        result = b32true
    }
    
    return
}

@(private="file")
font_set_faceptr_from_id :: proc(set: ^Font_Set, id: Face_ID) -> (f: ^^Face = nil) {
    using set
    if int(id) < len(id_to_face_table) {
        f = &(id_to_face_table[int(id)])
        assert(f != nil && f^ != nil)
        if f^.id != id {
            f = nil
        }
        return
    }
    return
}


font_set_face_from_id :: proc "cdecl" (set: ^Font_Set, id: Face_ID) -> (f: ^Face = nil) {
    context = default_context()
    ff := font_set_faceptr_from_id(set, id)
    if ff != nil {
        f = ff^
    }
    return
}

// NOTE (sio): currently, we just get the first face we can find that's valid.
// This may not be desirable if we plan on having more thorough e.g. CJK support.

font_set_get_fallback_face :: proc "cdecl" (set: ^Font_Set) -> (id: Face_ID = INVALID_FACE_ID) {
    context = default_context()
    using set
    for x in id_to_face_table {
        if x.id != INVALID_FACE_ID {
            id = x.id
            return
        }
    }
    return
}


font_set_get_largest_id :: proc "cdecl" (set: ^Font_Set) -> Face_ID {
    context = default_context()
    using set
    return next_id_counter - 1
}


font_set_modify_face :: proc "cdecl" (
    set: ^Font_Set,
    id: Face_ID,
    description: ^Face_Description
    ) -> (result: b32 = b32false)
{
    context = default_context()
    using set

    face := font_set_faceptr_from_id(set, id)
    if face != nil {
        version_number := face^.version_number
        new_face, new_face_ok := font_make_face(description, set.scale_factor)
        if new_face_ok {
            old_face := face^
            
            face^ = new_face
            face^.version_number = version_number + 1
            face^.id = id
              
            font_release_face(old_face)
                
            result = b32true
        }
    }
    
    return
}

character_is_whitespace :: proc "contextless" (c: u64) -> bool {
    return c == u64(' ') || c == u64('\n') || c == u64('\r') || c == u64('\t') || c == u64('\f') || c == u64('\v')
}


font_get_glyph_advance :: proc "cdecl" (
    cim: ^Face_Advance_Map,
    metrics: ^Face_Metrics,
    codepoint: u64,
    tab_multiplier: f32
    ) -> (result: f32)
{
    codepoint := codepoint
    if codepoint == u64('\t') {
        result = metrics.space_advance * tab_multiplier
    } else {
        if character_is_whitespace(codepoint) {
            codepoint = ' '
        }
        index, ok := lookup_ci_map(&(cim.codepoint_to_index), codepoint);
        if ok {
            result = cim.advance[index]
        }
    }
    return
}

font_get_max_avg_glyph_advance_range :: proc "contextless" (
    fam: ^Face_Advance_Map,
    metrics: ^Face_Metrics,
    codepoint_first: u64,
    codepoint_last: u64,
    tab_multiplier: f32
    ) -> (maximum: f32 = 0.0, average: f32 = 0.0)
{   
    for i: u64 = codepoint_first + 1; i <= codepoint_last; i += 1 {
        x := font_get_glyph_advance(fam, metrics, i, tab_multiplier)
        maximum = x
        average += x
    }
    average = average / f32(codepoint_last - codepoint_first + 1)
    return
}

// TODO (sio): these two are defined in custom/4coder_codepoint_map.cpp,
// and their sole callsites are in custom/4coder_helper.cpp:2500 onwards

font_get_max_glyph_advance_range :: proc "cdecl" (
    fam: ^Face_Advance_Map,
    metrics: ^Face_Metrics,
    codepoint_first: u64,
    codepoint_last: u64,
    tab_multiplier: f32
    ) -> (maximum: f32)
{
    maximum, _ =
        font_get_max_avg_glyph_advance_range(
            fam,
            metrics,
            codepoint_first,
            codepoint_last,
            tab_multiplier
            )
    return
}


font_get_average_glyph_advance_range :: proc "cdecl" (
    fam: ^Face_Advance_Map,
    metrics: ^Face_Metrics,
    codepoint_first: u64,
    codepoint_last: u64,
    tab_multiplier: f32
    ) -> (average: f32)
{
    _, average =
        font_get_max_avg_glyph_advance_range(
            fam,
            metrics,
            codepoint_first,
            codepoint_last,
            tab_multiplier
            )
    return
}

// TODO (sio): destroy font set maybe?

// TODO (sio): move font set init/destroy stuff to main from app init,
// and have it be *checked* and return useful errors

ft_load_flags :: proc(use_hinting: bool) -> (flags: ft.Load_Flags = { .Render }) {
    if use_hinting {
        // NOTE(inso): FT_LOAD_TARGET_LIGHT does hinting only vertically, which looks nicer imo
        // maybe it could be exposed as an option for hinting, instead of just on/off.
        flags += { .Force_Autohint, .Target_Light }
    } else { // !use_hinting
        flags += { .No_Autohint, .No_Hinting }
    }
    return
}

ft_get_codepoint_index_pairs :: proc(
    face: ft.Face
    ) -> (array: []FT_Codepoint_Index_Pair, maximum_index: u32, err: rt.Allocator_Error)
{
    glyph_count := face.num_glyphs
    
    array, err = make([]FT_Codepoint_Index_Pair, glyph_count)
    if err != .None {
        log_os("Failed to allocate memory for ft codepoint-index pairs, got allocator error:", err)
        return
    }
    
    counter: i64 = 0
    index: u32 = 0
    codepoint: u64 = ft.get_first_char(face, &index)
    array[counter].codepoint = codepoint
    array[counter].index = index
    maximum_index = max(maximum_index, index)
    counter += 1
    for ; counter < glyph_count; counter += 1 {
        codepoint = ft.get_next_char(face, codepoint, &index)
        array[counter].codepoint = codepoint
        array[counter].index = index
        maximum_index = max(maximum_index, index)
    }
    
    return
}

ft_get_codepoint_index_map :: proc(
    face: ft.Face
    ) -> (cim: Codepoint_Index_Map, err: rt.Allocator_Error)
{
    mem.zero(&cim, size_of(Codepoint_Index_Map))
    using cim
    
    save_point := rt.default_temp_allocator_temp_begin()
    defer rt.default_temp_allocator_temp_end(save_point)
    
    
    array: []FT_Codepoint_Index_Pair
    array_alloc_err: rt.Allocator_Error
    {
        alloc := context.allocator
        defer context.allocator = alloc
        
        context.allocator = context.temp_allocator
        
        array, max_index, array_alloc_err = ft_get_codepoint_index_pairs(face)
    }
    
    if array_alloc_err != .None {
        log_os("Failed to allocate memory, retrying with slower allocator")
        array, max_index, err = ft_get_codepoint_index_pairs(face)
        
        if err != .None {
            log_os(
                   "Failed to allocate memory for codepoint indexes,",
                   "can't build codepoint index map",
                   sep = "\n"
                   )
            max_index = 0
            return
        }
    }
    
    // had an alloc failure on temp alloc
    defer if array_alloc_err != .None { delete(array) }
    
    unicode, err = make(type_of(unicode))
    
    if err != .None {
        log_os("Failed to allocate memory for codepoint index map")
        return
    }
    
    defer if err != .None { delete(unicode) }
    
    for pair in array {
        if pair.codepoint >= CIM_ASCII_MIN && pair.codepoint <= CIM_ASCII_MAX {
            ascii[pair.codepoint].index = pair.index
            ascii[pair.codepoint].populated = true
        } else {
            unicode[pair.codepoint] = pair.index
        }
    }
    
    return
}

Bad_Rect_Pack :: struct {
    max_dim: [2]i32,
    dim: [3]i32,
    p: [3]i32,
    current_line_h: i32,
}

ft_bad_rect_pack_init :: proc(max_dim: [2]i32) -> (pack: Bad_Rect_Pack) {
    pack.max_dim = max_dim
    pack.dim = [3]i32 { 0, 0, 1 }
    pack.p = [3]i32 { 0, 0, 0 }
    pack.current_line_h = 0
    return pack
}

ft_bad_rect_pack_end_line :: proc(pack: ^Bad_Rect_Pack) {
    using pack
    p.y = current_line_h
    dim.y = max(dim.y, p.y)
    current_line_h = 0
    p.x = 0
}

ft_bad_rect_pack_next :: proc(pack: ^Bad_Rect_Pack, dim: [2]i32) -> (result: [3]i32) {
    if dim.x <= pack.max_dim.x && dim.y <= pack.max_dim.y {
        if pack.current_line_h < dim.y {
            pack.current_line_h = dim.y
        }
        
        if pack.current_line_h > pack.max_dim.y {
            ft_bad_rect_pack_end_line(pack)
            pack.p.y = 0
            pack.dim.z += 1
            pack.p.z += 1
        } else { // pack.current_line_h <= pack.max_dim.y
            if pack.p.x + dim.x > pack.max_dim.x {
                ft_bad_rect_pack_end_line(pack)
            }
            
            result = pack.p;
            pack.p.x += dim.x;
            
            if pack.current_line_h < dim.y {
                pack.current_line_h = dim.y
            }
            
            if pack.p.x > pack.dim.x {
                pack.dim.x = pack.p.x
            }
        }
    }
    return
}

ft_bad_rect_store_finish :: proc(pack: ^Bad_Rect_Pack) {
    ft_bad_rect_pack_end_line(pack)
}

ft_glyph_bounds_store_uv_raw :: proc(p: [3]i32, dim: [2]i32, bounds: ^Glyph_Bounds) {
    bounds.uv = Rect(f32) {
        x0 = f32(p.x), y0 = f32(p.y),
        x1 = f32(dim.x), y1 = f32(dim.y)
    }
    bounds.w = f32(p.z)
}

ft_font_make_face :: proc(
    description: ^Face_Description,
    scale_factor: f32
    ) -> (fptr: ^Face = nil, res: ft.Error = .Ok)
{
    // NOTE (sio): these two are backed by exactly the same array (!)
    file_name: String_Const_u8
    file_name_cstr: cstring
    
    {
        err: rt.Allocator_Error = .None
        file_name_cstr, err =
            strings.clone_to_cstring(
                 string(description.font.file_name),
                 allocator = rt.default_allocator()
                 )
        if err != .None {
            log_os(
                "Failed to clone file name string \"",
                description.font.file_name,
                "\"with default heap allocator; got error",
                err,
                sep = ""
                )
            return
        }
        file_name = String_Const_u8(file_name_cstr)
    }
    defer if res != .Ok { delete(file_name_cstr, allocator = rt.default_allocator()) }
    
    lib: ft.Library
    res = ft.init_free_type(&lib)
    if res != .Ok {
        log_os("Failed to initialize ft library, got error", res)
        return
    }
    
    defer {
        // NOTE (sio): ignore error (for now)
        done_res := ft.done_free_type(lib)
        if done_res != .Ok {
            log_os("Got error while uninitializing ft:", done_res)
        }
    }
    
    ft_face: ft.Face
    res = ft.new_face(lib, file_name_cstr, 0, &ft_face)
    
    if res != .Ok {
        log_os(
            "Got error while loading font face",
            description.font.file_name,
            "; got error",
            res
            )
        return
    }
    
    // TODO (sio): figure out if this is actually correct, I suspect this is what
    // we're supposed to do, but I'm not 100% sure
    defer {
        // NOTE (sio): ignore error (for now)
        done_res := ft.done_face(ft_face)
        if done_res != .Ok {
            log_os(
                "Got error while disposing of ft font face", 
                description.font.file_name,
                "; error:",
                done_res
                )
        }
    }
    
    face, err := new(Face)
    if err != .None {
        res = .Out_Of_Memory
        log_os(
            "Got error while allocating space for font face",
            description.font.file_name,
            "; got error:",
            err
            )
        return
    }
    defer if res != .Ok { free(face) }
    
    pt_size_unscaled: u32 = max(description.parameters.pt_size, 8)
    pt_size: u32 = u32(f32(pt_size_unscaled) * scale_factor)
    hinting: b32 = b32(description.parameters.hinting)
    
    size: ft.Size_Request_Rec
    size.type = .Nominal
    size.height = i64(pt_size) << 6
    
    res = ft.request_size(ft_face, &size)
    if res != .Ok {
        log_os(
            "Failed to request size for font face",
            description.font.file_name,
            "; got error",
            res)
        return
    }
    
    face.description.font.file_name = file_name
    face.description.parameters = description.parameters
    
    {
        using face.metrics
        ft_met := &(ft_face.size.metrics)
        
        max_advance = math.ceil(f32(ft_met.max_advance) / f32(64))
        ascent = math.ceil(f32(ft_met.ascender) / f32(64))
        descent = math.floor(f32(ft_met.descender) / f32(64))
        text_height = math.ceil(f32(ft_met.height) / f32(64))
        line_skip = max(f32(1), text_height - (ascent - descent))
        line_height = text_height + line_skip
        
        real_over_notional := line_height / f32(ft_face.height)
        relative_center := f32(-1) * real_over_notional * f32(ft_face.underline_position)
        relative_thickness := real_over_notional * f32(ft_face.underline_thickness)
        
        center := math.floor(ascent + relative_center)
        thickness := max(f32(1), relative_thickness)
        
        underline_yoff1 = center - (thickness * f32(0.5))
        underline_yoff2 = center + (thickness * f32(0.5))
    }
    
    face.advance_map.codepoint_to_index, err = ft_get_codepoint_index_map(ft_face)
    
    if err != .None {
        log_os(
            "Failed to get/build codepoint index map for font face",
            file_name,
            "; got error:",
            err
            )
        res = .Out_Of_Memory
        return
    }
    
    index_count: u32 = face.advance_map.codepoint_to_index.max_index + 1
    
    face.advance_map.index_count = index_count
    {
        slice: []f32
        slice, err = make([]f32, index_count)
        if err != .None {
            log_os(
                "Failed to allocate space for indexes for font face",
                file_name,
                "; got error:",
                err
                )
            res = .Out_Of_Memory
            return
        }
        face.advance_map.advance = raw_data(slice)
    }
    
    {   
        slice: []Glyph_Bounds
        slice, err = make([]Glyph_Bounds, index_count)
        if err != .None {
            log_os(
                "Failed to allocate space for glyph bounds for font face",
                file_name,
                "; got error:",
                err
                )
            res = .Out_Of_Memory
            return
        }
        face.bounds = raw_data(slice)
    }
    
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    
    Bitmap :: struct { dim: [2]i32, data: [^]u8 }
    
    glyph_bitmaps: []Bitmap
    glyph_bitmaps, err = make([]Bitmap, index_count, allocator = context.temp_allocator)
    if err != .None {
        log_os(
            "Failed to allocate scratch space to render glyph bitmaps of font face",
            file_name,
            "onto; got error:",
            err
            )
        res = .Out_Of_Memory
        return
    }
    
    load_flags: ft.Load_Flags = ft_load_flags(bool(hinting))
    
    
    for i: u32 = 0; i < index_count; i += 1 {
        bitmap: ^Bitmap = &(glyph_bitmaps[i])
        
        res = ft.load_glyph(ft_face, i, load_flags)
        log_if(ft.Error.Ok, res, "Failed to load glyph:", i, "from face", ft_face);
        // NOTE (sio): ignore error, just log
        
        ft_glyph: ft.Glyph_Slot = ft_face.glyph
        dim := [2]i32 { i32(ft_glyph.bitmap.width), i32(ft_glyph.bitmap.rows) }
        bitmap.dim = dim
        
        {
            data: []u8
            data, err = make([]u8, dim.x * dim.y, allocator = context.temp_allocator)
            if err != .None {
                log_os(
                    "Failed to allocate scratch space for rendering glyph bitmaps",
                    "of font face",
                    file_name,
                    "; got error",
                    err
                    )
                res = .Out_Of_Memory
                return
            }
            bitmap.data = raw_data(data)
        }
        
        {
            using face.bounds[i].xy_off
            x0 = f32(ft_face.glyph.bitmap_left)
            y0 = f32(face.metrics.ascent) - f32(ft_face.glyph.bitmap_top)
            x1 = x0 + f32(dim.x)
            y1 = y0 + f32(dim.y)
        }
        
        #partial switch ft_glyph.bitmap.pixel_mode {
            case .Mono:
                log_os("Mono pixel mode encountered, but not implemented");
            
            case .Gray:
                aa_1bit_mono := description.parameters.aa_mode == .OneBitMono;
                
                src_line: ^u8 = ft_glyph.bitmap.buffer
                if ft_glyph.bitmap.pitch < 0 {
                    src_line =
                        mem.ptr_offset(
                            ft_glyph.bitmap.buffer,
                            (-(ft_glyph.bitmap.pitch)) * (dim.y - 1)
                            )
                }
                
                dst: ^u8 = bitmap.data
                for y: i32 = 0; y < dim.y; y += 1 {
                    src_pixel: ^u8 = src_line
                    for x: i32 = 0; x < dim.x; x += 1 {
                        if aa_1bit_mono {
                            s: u8 = src_pixel^
                            if s > 0 {
                                s = 255
                            }
                            dst^ = s
                        } else {
                            dst^ = src_pixel^
                        }
                        dst = mem.ptr_offset(dst, 1)
                        src_pixel = mem.ptr_offset(src_pixel, 1)
                    }
                    src_line = mem.ptr_offset(src_line, ft_glyph.bitmap.pitch)
                }
            
            case:
                log_os(
                    "ft pixel mode used by ft, but not implemented:",
                    ft_glyph.bitmap.pixel_mode
                    )
        }
        
        face.advance_map.advance[i] = math.ceil(f32(ft_glyph.advance.x) / 64.0)
    }
    
    white_data := [16]u8 {
        0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,
    }
    
    white: Bitmap
    white.dim = [2]i32 { 4, 4 }
    white.data = transmute([^]u8) &white_data
    
    pack: Bad_Rect_Pack = ft_bad_rect_pack_init([2]i32 { 1024, 1024 })
    
    ft_glyph_bounds_store_uv_raw(
        ft_bad_rect_pack_next(&pack, white.dim), white.dim, &(face.white)
        )
    
    for i: u32 = 0; i < index_count; i += 1 {
        dim := glyph_bitmaps[i].dim
        ft_glyph_bounds_store_uv_raw(
            ft_bad_rect_pack_next(&pack, dim), dim, &(face.bounds[i])
            )
    }
    ft_bad_rect_store_finish(&pack)
    
    texture_kind := Texture_Kind.Mono
    texture: u32 = graphics_get_texture(pack.dim, texture_kind)
    
    face.texture_kind = texture_kind
    face.texture = texture
    
    texture_dim := [3]f32 { f32(pack.dim[0]), f32(pack.dim[1]), f32(pack.dim[2]) }
    face.texture_dim = texture_dim
    
    
    {
        using face.white
        
        p := [3]i32 { i32(uv.x0), i32(uv.y0), i32(w) }
        dim := [3]i32 { white.dim.x, white.dim.y, 1 }
        graphics_fill_texture(texture_kind, texture, p, dim, white.data)
        
        uv.x1 = (uv.x0 + uv.x1) / texture_dim.x
        uv.y1 = (uv.y0 + uv.y1) / texture_dim.y
        uv.x0 =  uv.x0 / texture_dim.x
        uv.y0 =  uv.y0 / texture_dim.y
        w = w / texture_dim.z
    }
    
    for i: u32 = 0; i < index_count; i += 1 {
        fb := face.bounds[i]
        using fb
        
        p := [3]i32{i32(uv.x0), i32(uv.y0), i32(w) }
        dim := [3]i32 { glyph_bitmaps[i].dim.x, glyph_bitmaps[i].dim.y, 1 }
        
        graphics_fill_texture(texture_kind, texture, p, dim, glyph_bitmaps[i].data)
        
        uv.x1 = (uv.x0 + uv.x1) / texture_dim.x
        uv.y1 = (uv.y0 + uv.y1) / texture_dim.y
        uv.x0 =  uv.x0 / texture_dim.x
        uv.y0 =  uv.y0 / texture_dim.y
        w = w / texture_dim.z
    }
    
    {
        advance_map: ^Face_Advance_Map = &(face.advance_map)
        met: ^Face_Metrics = &(face.metrics)
        using face.metrics
        
        space_advance = font_get_glyph_advance(advance_map, met, ' ', 0)
        decimal_digit_advance =
            font_get_max_glyph_advance_range(advance_map, met, '0', '9', 0)
        hex_digit_advance =
            font_get_max_glyph_advance_range(advance_map, met, 'A', 'F', 0)
        hex_digit_advance =
            max(hex_digit_advance, decimal_digit_advance)
        byte_sub_advances[0] =
            font_get_glyph_advance(advance_map, met, '\\', 0)
        byte_sub_advances[1] = hex_digit_advance
        byte_sub_advances[2] = hex_digit_advance
        byte_advance =
            byte_sub_advances[0] +
            byte_sub_advances[1] +
            byte_sub_advances[2]
        normal_lowercase_advance =
            font_get_average_glyph_advance_range(advance_map, met, 'a', 'z', 0)
        normal_uppercase_advance =
            font_get_average_glyph_advance_range(advance_map, met, 'A', 'Z', 0)
        normal_advance = (26.0 * normal_lowercase_advance +
                          26.0 * normal_uppercase_advance +
                          10.0 * decimal_digit_advance
                          ) / 62.0
    }
    
    return
}

ft_font_release_face :: proc(face: ^Face) {
    // TODO
}
