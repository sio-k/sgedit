#!/bin/sh

# stop on error
set -e

# make sure we're running in the directory where the script is
cd "$(dirname "$(realpath "$0")")"

source ./setup_compiler.sh

cmake -P build/cmake_install.cmake
chmod +x sgedit
