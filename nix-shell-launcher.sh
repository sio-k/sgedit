#!/bin/sh

cd "$(dirname "$(realpath "$0")")"

nix-shell -p pkg-config binutils gnumake cmake ninja \
    odin ispc clang_6 \
    SDL2 \
    llvmPackages.compiler-rt\
    freetype \
    --run "$*"
