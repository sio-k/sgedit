#version 460
// this is exactly the same vertex shader as originally used

uniform vec2 view_t;
uniform mat2x2 view_m;

in vec2 vertex_p;
in vec3 vertex_t;
in uint vertex_c;
in float vertex_ht;

smooth out vec4 fragment_color;
smooth out vec3 uvw;
smooth out vec2 xy;
smooth out vec2 adjusted_half_dim;
smooth out float half_thickness;

void main(void) {
    gl_Position = vec4(view_m*(vertex_p - view_t), 0.0, 1.0);

    fragment_color.b = (float((vertex_c     )&0xFFu))/255.0;
    fragment_color.g = (float((vertex_c>> 8u)&0xFFu))/255.0;
    fragment_color.r = (float((vertex_c>>16u)&0xFFu))/255.0;
    fragment_color.a = (float((vertex_c>>24u)&0xFFu))/255.0;

    uvw = vertex_t;

	xy = vertex_p;

    vec2 center = vertex_t.xy;

    vec2 half_dim = abs(vertex_p - center);
    adjusted_half_dim = half_dim - vertex_t.zz + vec2(0.5, 0.5);

    half_thickness = vertex_ht;
}