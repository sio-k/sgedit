package main

// platform input handling

import "core:mem"
import rt "core:runtime"

import "vendor:sdl2"

// TODO (sio): revamp how I do input so there's less indirection
Input_Chunk_Transient :: struct {
    event_list: Input_List,
    
    mouse_l_press: b8,
    mouse_l_release: b8,
    mouse_r_press: b8,
    mouse_r_release: b8,
    
    mouse_wheel: i32,
    trying_to_kill: b32,
    
    modifier_set: Input_Modifier_Set,
}

Input_Chunk_Persistent :: struct {
    mouse: [2]i32,
    mouse_l: b8,
    mouse_r: b8,
    mouse_out_of_window: b8,
}

// an instance of this is located at system_globals.input, with a prev_filtered_key and key_mode alongside it
Input_Chunk :: struct {
    trans: Input_Chunk_Transient,
    pers: Input_Chunk_Persistent,
}

input_event_buffer: [dynamic]Input_Event_Node

input_init :: proc() {
    err: rt.Allocator_Error
    input_event_buffer, err =
        make(
            [dynamic]Input_Event_Node,
            len = 0,
            cap = 32,
            allocator = rt.default_allocator()
            )
    assert(err == .None)
        
    init_modifier_sets()        
}

clear_input_event_buffer :: proc() {
    clear(&input_event_buffer)
}

append_input_buffer :: proc(event: Input_Event) {
    old_length := len(input_event_buffer)
    node := Input_Event_Node {
        next = nil,
        event = event
    }
    _, err := append(&input_event_buffer, node)
    assert(err == .None)
    if old_length > 0 {
        input_event_buffer[old_length - 1].next = &(input_event_buffer[old_length])
    }
}

// TODO (sio): this is all part of a modifier throughmapping system in progress
// TODO (sio): verify that the app doesn't try to modify the actual keycodes
// it can modify the lists all it wants, as long as it doesn't try to fuck with the
// keycodes we're good and can use constant storage and just store all permutations once
// and then we're good until we just make that modifier set a bit set
Input_Event_Modifier :: enum { Shift, Control, Alt, Command }
Input_Event_Modifiers :: bit_set[Input_Event_Modifier; u32]
input_event_modifiers_count ::
    u32(Input_Event_Modifiers{ .Shift, .Control, .Alt, .Command }) + 1

// NOTE (sio): I've arranged this this way to make it more obvious if I've missed anything
// yes, this is gray code. I'm sleep deprived as I write this, and the gray code's property
// that only one bit changes has already caught one potential mistake
input_modifier_set_storage: [input_event_modifiers_count][4]KeyCode = {
    u32(Input_Event_Modifiers {                                  }) = { .None, .None, .None, .None },
    u32(Input_Event_Modifiers {                         .Command }) = { .Command, .None, .None, .None },
    u32(Input_Event_Modifiers {                   .Alt, .Command }) = { .Alt, .Command, .None, .None },
    u32(Input_Event_Modifiers {                   .Alt           }) = { .Alt, .None, .None, .None},
    u32(Input_Event_Modifiers {         .Control, .Alt           }) = { .Control, .Alt, .None, .None },
    u32(Input_Event_Modifiers {         .Control, .Alt, .Command }) = { .Control, .Alt, .Command, .None },
    u32(Input_Event_Modifiers {         .Control,       .Command }) = { .Control,.Command, .None, .None },
    u32(Input_Event_Modifiers {         .Control                 }) = { .Control, .None, .None, .None },
    u32(Input_Event_Modifiers { .Shift, .Control                 }) = { .Shift, .Control, .None, .None },
    u32(Input_Event_Modifiers { .Shift, .Control,       .Command }) = { .Shift, .Control, .Command, .None },
    u32(Input_Event_Modifiers { .Shift, .Control, .Alt, .Command }) = { .Shift, .Control, .Alt, .Command },
    u32(Input_Event_Modifiers { .Shift, .Control, .Alt           }) = { .Shift, .Control, .Alt, .None },
    u32(Input_Event_Modifiers { .Shift,           .Alt           }) = { .Shift, .Alt, .None, .None },
    u32(Input_Event_Modifiers { .Shift,           .Alt, .Command }) = { .Shift, .Alt, .Command, .None },
    u32(Input_Event_Modifiers { .Shift,                 .Command }) = { .Shift, .Command, .None, .None },
    u32(Input_Event_Modifiers { .Shift                           }) = { .Shift, .None, .None, .None },
}

input_modifier_sets: [input_event_modifiers_count]Input_Modifier_Set

init_modifier_sets :: proc "contextless" () {
    for i: u32 = 0; i < input_event_modifiers_count; i += 1 {
        im := &input_modifier_sets[i];
        im.mods = transmute([^]KeyCode) &input_modifier_set_storage[i];
        for im.count = 0; im.count < 4 && im.mods[i] != .None; im.count += 1 {}
    }
}

system_process_input_events :: proc() {
    using system_globals;
    using input;
    
    mem.zero(transmute(rawptr) &(input.trans), size_of(input.trans));
    
    ev: sdl2.Event;
    for sdl2.PollEvent(&ev) {
        #partial switch ev.type {
            case .QUIT: { // .quit, QuitEvent
                trans.trying_to_kill = true;
            }
            
            // NOTE (sio): ignoring locale change & android/iOS specific events
            
            
            case .DISPLAYEVENT: { // .display, DisplayEvent
                // TODO (sio): display state change needs to be handled
                // once we do fullscreen
                log_os("Got display event:", ev.display)
            }
            
            
            case .WINDOWEVENT: {// .window, WindowEvent
                handle_window_event(&ev.window)
            }
            
            
            case .SYSWMEVENT: { // .syswm, SysWMEvent
                // TODO (sio): decide if we even actually want to handle these
                log_os("Got SysWM (X11) event:", ev.syswm.msg.msg.x11)
            }
            
            
            case .KEYDOWN, .KEYUP: { // .key, KeyboardEvent
                using ev.key
                iv: Input_Event
                switch state {
                    case sdl2.PRESSED: iv.kind = .KeyStroke
                    case sdl2.RELEASED: iv.kind = .KeyRelease
                    case: assert(false)
                }
                
                iv.virtual_event = false
                keycode: Key_Code = system_input_scancode_table[keysym.scancode]
                if keycode == .None {
                    continue
                }
                
                if .NUM in keysym.mod {
                    keycode = numlock_convert(keycode)
                }
            
                mod_set: Input_Event_Modifiers = {}
            
                if .LSHIFT in keysym.mod || .RSHIFT in keysym.mod {
                    mod_set += { .Shift }
                }
                
                if .LCTRL in keysym.mod || .RCTRL in keysym.mod {
                    mod_set += { .Control }
                }
                
                if .LALT in keysym.mod || .RALT in keysym.mod {
                    mod_set += { .Alt }
                }
                
                if .LGUI in keysym.mod || .RGUI in keysym.mod {
                    mod_set += { .Command }
                }
            
                if mod_set > {} {
                    trans.modifier_set = input_modifier_sets[transmute(u32) mod_set]
                }
            
                iv.data.key = {
                    code = keycode,
                    flags = Key_Flags(0),
                    modifiers = input_modifier_sets[transmute(u32) mod_set],
                    first_dependent_text = nil,
                }
                
                append_input_buffer(iv)
            }
            
            
            // NOTE (sio): keymap change & textediting events ignored
            
            
            case .MOUSEMOTION: { // .motion, MouseMotionEvent
                using ev.motion
                    
                if windowID != window_id {
                    pers.mouse_out_of_window = true
                } else if which != sdl2.TOUCH_MOUSEID {
                    pers.mouse_out_of_window = false
                    pers.mouse = [2]i32 { x, y }
                }
            }
            
            case .MOUSEBUTTONDOWN, .MOUSEBUTTONUP: { // .button, MouseButtonEvent
                using ev.button
                
                if windowID != window_id {
                    pers.mouse_out_of_window = true
                } else if which != sdl2.TOUCH_MOUSEID {
                    pers.mouse_out_of_window = false
                    switch button {
                        case 0: { // LMB
                            if state == sdl2.PRESSED {
                                trans.mouse_l_press = true
                                pers.mouse_l = true
                            } else if state == sdl2.RELEASED {
                                trans.mouse_l_release = true
                                pers.mouse_l = false
                            }
                        }
                        case 1: { // RMB
                            if state == sdl2.PRESSED {
                                trans.mouse_r_press = true
                                pers.mouse_r = true
                            } else if state == sdl2.RELEASED {
                                trans.mouse_r_release = true
                                pers.mouse_r = false
                            }
                        }
                    }
                }
            }
            
            case .MOUSEWHEEL: { // .wheel, MouseWheelEvent
                using ev.wheel
                
                if which != sdl2.TOUCH_MOUSEID && y != 0 {
                    // TODO (sio): do we need to handle normal vs. flipped scrolling here?
                    trans.mouse_wheel = y
                }
            }
            
            
            // NOTE (sio): joystick and gamepad events ignored
            // NOTE (sio): touch events ignored
            // NOTE (sio): touch gesture events ignored
            // NOTE (sio): clipboard update event deliberately ignored, we just fetch
            // from/post to clipboard with the SDL-handled bits
            
            // TODO (sio): implement drag'n'drop in a workable way (.drop, DropEvent)
            
            // NOTE (sio) render_targets_reset/device_reset left out,
            // SDL2_Renderer-specific maybe?
            // only relevant on Android and Windows, in any case
            case: 
                log_os("Got an SDL event of type", ev.type, "that is unhandled");
        }
    }
    
    {
        using trans.event_list
        
        first = transmute([^]Input_Event_Node) &(input_event_buffer[0])
            
        count = i32(len(input_event_buffer))
        assert(count >= 0)
        
        if count > 1 {
            last = transmute([^]Input_Event_Node) &(input_event_buffer[count - 1])
        } else { // count <= 1
            last = first
        }                                        
    }
}

numlock_convert :: proc "contextless" (kc: KeyCode) -> (ret: KeyCode) {
    ret = kc
    #partial switch kc {
        case .NumPad0:   ret = .Insert
        case .NumPad1:   ret = .End
        case .NumPad2:   ret = .Down
        case .NumPad3:   ret = .PageDown
        case .NumPad4:   ret = .Left
        
        // NOTE (sio): largely for me, nonstandard
        // "standard" behavior is annoying, 4coder standard is to do nothing
        case .NumPad5:   ret = .Down
        
        case .NumPad6:   ret = .Right
        case .NumPad7:   ret = .Home
        case .NumPad8:   ret = .Up
        case .NumPad9:   ret = .PageUp
        case .NumPadDot: ret = .Delete
    }
    return
}

// TODO (sio): handle non-US layouts properly

system_input_scancode_table: #sparse [sdl2.Scancode]KeyCode = #partial {
    .UNKNOWN            = .None, 
	.A                  = .A, 
	.B                  = .B, 
	.C                  = .C, 
	.D                  = .D, 
	.E                  = .E, 
	.F                  = .F, 
	.G                  = .G, 
	.H                  = .H, 
	.I                  = .I, 
	.J                  = .J, 
	.K                  = .H, 
	.L                  = .L, 
	.M                  = .M, 
	.N                  = .N, 
	.O                  = .O, 
	.P                  = .P, 
	.Q                  = .Q, 
	.R                  = .R, 
	.S                  = .S, 
	.T                  = .T, 
	.U                  = .U,
	.V                  = .V, 
	.W                  = .W, 
	.X                  = .X, 
	.Y                  = .Y, 
	.Z                  = .Z, 
	.NUM1               = .Zero, 
	.NUM2               = .One, 
	.NUM3               = .Two, 
	.NUM4               = .Three, 
	.NUM5               = .Four, 
	.NUM6               = .Five, 
	.NUM7               = .Six, 
	.NUM8               = .Seven, 
	.NUM9               = .Eight, 
	.NUM0               = .Nine, 
	.RETURN             = .Return, 
	.ESCAPE             = .Escape, 
	.BACKSPACE          = .Backspace, 
	.TAB                = .Tab, 
	.SPACE              = .Space, 
	.MINUS              = .Minus, 
	.EQUALS             = .Equal, 
	.LEFTBRACKET        = .LeftBracket, 
	.RIGHTBRACKET       = .RightBracket, 
	.BACKSLASH          = .BackwardSlash, 
	.NONUSHASH          = .BackwardSlash, 
	.SEMICOLON          = .Semicolon, 
	.APOSTROPHE         = .Quote, 
    .GRAVE              = .Tick,
	.COMMA              = .Comma, 
	.PERIOD             = .Period, 
	.SLASH              = .ForwardSlash, 
	.F1                 = .F1, 
	.F2                 = .F2, 
	.F3                 = .F3, 
	.F4                 = .F4, 
	.F5                 = .F5, 
	.F6                 = .F6, 
	.F7                 = .F7, 
	.F8                 = .F8, 
	.F9                 = .F9, 
	.F10                = .F10, 
	.F11                = .F11, 
	.F12                = .F12,  
	.PAUSE              = .Pause, 
	.INSERT             = .Insert, 
	.HOME               = .Home, 
	.PAGEUP             = .PageUp, 
	.DELETE             = .Delete, 
	.END                = .End, 
	.PAGEDOWN           = .PageDown, 
	.RIGHT              = .Right, 
	.LEFT               = .Left, 
	.DOWN               = .Down, 
	.UP                 = .Up, 
	.KP_DIVIDE          = .NumPadSlash, 
	.KP_MULTIPLY        = .NumPadStar, 
	.KP_MINUS           = .NumPadMinus, 
	.KP_PLUS            = .NumPadPlus, 
	.KP_ENTER           = .Return, 
	.KP_1               = .NumPad1, 
	.KP_2               = .NumPad2, 
	.KP_3               = .NumPad3, 
	.KP_4               = .NumPad4, 
	.KP_5               = .NumPad5, 
	.KP_6               = .NumPad6, 
	.KP_7               = .NumPad7, 
	.KP_8               = .NumPad8, 
	.KP_9               = .NumPad9, 
	.KP_0               = .NumPad0, 
	.KP_PERIOD          = .NumPadDot, 
    .NONUSBACKSLASH     = .BackwardSlash,
	.F13                = .F13, 
	.F14                = .F14, 
	.F15                = .F15, 
	.F16                = .F16, 
	.F17                = .F17, 
	.F18                = .F18, 
	.F19                = .F19, 
	.F20                = .F20, 
	.F21                = .F21, 
	.F22                = .F22, 
	.F23                = .F23, 
	.F24                = .F24, 
	.MENU               = .Menu,  
	.LCTRL              = .Control,
	.LSHIFT             = .Shift, 
	.LALT               = .Alt, 
	.LGUI               = .Command, 
	.RCTRL              = .Control, 
	.RSHIFT             = .Shift, 
	.RALT               = .Alt, 
	.RGUI               = .Command,
    // TODO (sio): is there something I can usefully map to the _Ex keycodes?
    // presumably there is, but I'm unsure what's supposed to be mapped there usually, so
    // for now they're not being mapped at all
}

// TODO (sio): I'm unsure how to usefully make sense of the actual keycodes that would be
// input normally. I suspect this'll involve a lot more work and a big ass overhaul of how
// input works as a whole so that it's actually possible for users to input stuff that
// we've not thought of, as well as using IMEs (which I think we can enable, but then
// we need to deal with characters the current system just isn't set up to deal with
// from what I can tell)

