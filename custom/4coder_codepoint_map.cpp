/*
4coder_codepoint_map.cpp - Codepoint map to index
*/

// TOP

// TODO (sio): move these into the vtable
#ifndef AUTO_CAST_IMPL
#define AUTO_CAST_IMPL
// NOTE (sio): horrible hack
template<typename T, typename U>
void auto_cast(T* x, U** y) {
    *y = (U*) x;
}
#endif // AUTO_CAST_IMPL

static b32 (*codepoint_index_map_read)(Codepoint_Index_Map *map, u64 codepoint, u16 *index_out) = nullptr;

static u16 (*codepoint_index_map_count)(Codepoint_Index_Map *map) = nullptr;

static f32 (*font_get_glyph_advance)(Face_Advance_Map *map, Face_Metrics *metrics, u64 codepoint, f32 tab_multiplier) = nullptr;
     
static f32 (*font_get_max_glyph_advance_range)(Face_Advance_Map *map, Face_Metrics *metrics,
                                     u64 codepoint_first, u64 codepoint_last,
                                        f32 tab_multiplier) = nullptr;
static f32 (*font_get_average_glyph_advance_range)(Face_Advance_Map *map, Face_Metrics *metrics,
                                         u64 codepoint_first, u64 codepoint_last,
                                            f32 tab_multiplier) = nullptr;

extern "C" void codepoint_index_map_setup(
    void* map_read,
    void* map_count,
    void* glyph_advance,
    void* max_glyph_advance,
    void* average_glyph_advance
    )
{
    auto_cast(map_read, &codepoint_index_map_read);
    auto_cast(map_count, &codepoint_index_map_count);
    auto_cast(glyph_advance, &font_get_glyph_advance);
    auto_cast(max_glyph_advance, &font_get_max_glyph_advance_range);
    auto_cast(average_glyph_advance, &font_get_average_glyph_advance_range);
}

// BOTTOM

