#!/bin/sh

# set working directory of this script to be the directory the script is in
cd "$(dirname "$(realpath "$0")")"

source ./setup_compiler.sh

cmake --build build --target sgedit --config Debug
