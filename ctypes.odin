// right now, this just replaces the platform layer in function. More functionality *will* be folded into it, though.
package main

// NOTE: all of this needs to be kept in sync with the C++ side

import "core:dynlib"
import "core:fmt"
import "core:mem"
import "core:os"
import rt "core:runtime"
import "core:sync"
import "core:thread"

import "vendor:sdl2"

Raw_Slice :: rt.Raw_Slice

// TODO: allow for multiple windows; this shouldn't be too hard, all in all. Mild adjustments to input handling, as well as output I think?
// ah, right. Multi-Window OpenGL was a pain. Possible, but a massive pain. So I should probably switch to Vulkan if I wanna do this?

// TODO: split this up into several files in a separate directory, with some useful sorting or somesuch so it's easier to find the bits one needs

Void_Func :: #type proc "cdecl" ()

// TODO: get rid of every occurence of this monstrosity in structs and memory

// NOTE: needs to be b32 because exported as such, and possibly expected to be this type by app? Unclear. If not, I can just make this a standard Odin bool and not export it.
@export
log_os_enabled: b32 = false

b32false :: b32(false)
b32true :: b32(true)

// NOTE: this was originally a macro, but Odin doesn't have those and in any case the runtime overhead is pretty much the same
@export
log_os :: proc(tolog: ..any, sep := " ") {
    if log_os_enabled == true {
        fmt.eprintln(tolog, sep = sep)
    }
}
;
@export
log_if :: proc(
    expected: $T,
    value: $U,
    to_log: ..any,
    sep := " ",
    loc := #caller_location
    )
{
    if expected != value {
        log_os(
            "error at",
            loc,
            ":\n",
            to_log,
            "; expected",
            expected,
            ", but got errno/error",
            value,
            sep = " "
            )
    }
}

b8false :: b8(false)
b8true :: b8(true)

// from custom/4coder_base_types.h

// linked list node
Node :: struct {
    next: ^Node,
    prev: ^Node,
}

// equivalent to Node_type
List_Node :: struct($T: typeid) {
    next: ^NodeT($T),
    elem: T,
}

// equivalent to List_type
List :: struct($T: typeid) {
    first: ^List_Node($T),
    last: ^List_Node($T),
    total_size: u64,
    node_count: i32,
}

// stack node?
SNode :: struct {
    next: ^SNode,
    prev: ^SNode,
}

// NOTE: can't implement sll_* ops as intended even if I want to

Pair :: struct($T: typeid) {
    left, right: T,
}

Range_MinMax :: struct($T: typeid) {
    min, max: T,
}

Range_StartEnd :: struct($T: typeid) {
    start, end: T,
}

Range_FirstLast :: struct($T: typeid) {
    first, one_past_last: T,
}

Range :: struct($T: typeid) {
    min, max: T,
}

Rect :: struct($T: typeid) {
    x0: T,
    y0: T,
    x1: T,
    y1: T,
}

// NOTE: can't necessarily be a slice because alignment restrictions, but conversion is trivial
Array :: struct($T: typeid) {
    vals: [^]T,
    count: i32,
}

String_Fill_Terminate_Rule :: enum i32 { NoTerminate, NullTerminate }
String_Separator_Flag :: enum u32 { NoFlags, BeforeFirst, AfterLast }
String_Match_Rule :: enum i32 { Exact, CaseInsensitive }

String_Const_char :: distinct string
String_Const_u8 :: distinct string
// background: Raw_String is the same thing, with ONE difference: the size field is called 'len' instead and has type i64 instead of u64. That's it, though. Casting between them should be fine, even completely implicitly.

// NOTE: I'm not implementing UTF-16 or UTF-32 support unless I have to.
String_Const_u16 :: distinct []u16
String_Const_u32 :: distinct []u32

Line_Ending_Kind :: enum i32 {
    Binary, LF, CRLF,
}

Character_Consume_Result :: struct {
    inc: u32,
    codepoint: u32,
}

@export
surrogate_min: u32 = 0xD800

@export
surrogate_max: u32 = 0xDFFF

@export
nonchar_min: u32 = 0xFDD0

@export
nonchar_max: u32 = 0xFDEF

// for both memory and files; there's overlap here that we can use
Access_Flag :: enum u32 {
    Always = 0,
    Read = 1,
    Write = 2,
    Exec = 4,
    Visible = 4,
    ReadWrite = 3,
    ReadVisible = 5,
    ReadWriteVisible = 7,
}

Dimension :: enum i32 {
    X, Y, Z, W,
}

Coordinate :: enum i32 {
    X, Y, Z, W
}

Side :: enum i32 { Min, Max }

Scan_Direction :: enum i32 { Backward = -1, Forward = 1 }

// TODO: figure out a way to make this some library's problem instead, ideally one that can cope with weird shit being thrown at it like e.g. Martian dates (Ls system, Sols etc. pp.)
Date_Time :: struct {
    year: u32, // "real" year without adjustment, whatever that means, I've no clue honestly; standard earth year at UTC maybe?
    mon: u8, // [0, 11]
    day: u8, // [0, 30]
    hour: u8, // [0, 23]
    min: u8, // [0, 59]
    sec: u8, // [0, 60]
    msec: u16, // [0,999]
}

// NOTE: I'm not gonna bother with the rest of the datetime stuff here

// NOTE: all VecN_type structs are simply [N]type. This is the only exception, left as a demonstration.
Vec2_i32 :: distinct [2]i32

// allocators from 4coder_base_types.h:1290 ff
Base_Allocator_Reserve_Signature :: #type proc "cdecl" (
    user_data: rawptr,
    size: u64,
    size_out: ^u64,
    location: String_Const_u8
    ) -> rawptr
Base_Allocator_Commit_Signature :: #type proc "cdecl" (
    user_data: rawptr,
    ptr: rawptr,
    size: u64
    )
Base_Allocator_Uncommit_Signature :: #type proc "cdecl" (
    user_data: rawptr,
    ptr: rawptr, 
    size: u64
    )
Base_Allocator_Free_Signature :: #type proc "cdecl" (user_data: rawptr, ptr: rawptr)
Base_Allocator_Set_Access_Signature :: #type proc "cdecl" (
    user_data: rawptr,
    ptr: rawptr,
    size: u64,
    flags: Access_Flag // TODO
    )

Base_Allocator :: struct {
    reserve: Base_Allocator_Reserve_Signature,
    commit: Base_Allocator_Commit_Signature,
    uncommit: Base_Allocator_Uncommit_Signature,
    free: Base_Allocator_Free_Signature,
    set_access: Base_Allocator_Set_Access_Signature,
    user_data: rawptr,
}

Cursor :: struct {
    base: [^]u8,
    pos: u64,
    cap: u64,
}

Temp_Memory_Cursor :: struct {
    cursor: ^Cursor,
    pos: u64,
}

Cursor_Node :: struct {
    prev: ^Cursor_Node,
    cursor: Cursor,
}

Arena :: struct {
    base_allocator: ^Base_Allocator,
    cursor_node: ^Cursor_Node,
    chunk_size: u64,
    alignment: u64,
}

Temp_Memory_Arena :: struct {
    arena: ^Arena,
    cursor_node: ^Cursor_Node,
    pos: u64,
}

Linear_Allocator_Kind :: enum i32 {
    Cursor, Arena
}

Temp_Memory_Fields :: struct #raw_union {
    temp_memory_cursor: Temp_Memory_Cursor,
    temp_memory_arena: Temp_Memory_Arena,
}

Temp_Memory :: struct {
    kind: Linear_Allocator_Kind,
    field: Temp_Memory_Fields,
}

Profile_ID :: distinct u64
Profile_Record :: struct {
    next: ^Profile_Record,
    id: Profile_ID,
    time: u64,
    location: String_Const_u8,
    name: String_Const_u8,
}

Profile_Thread :: struct {
    next: ^Profile_Thread,
    first_record: [^]Profile_Record,
    last_record: [^]Profile_Record,
    record_count: i32,
    thread_id: i32,
    name: String_Const_u8,
}

Profile_Enable_Flag :: enum u32 {
    UserBit = 0x1,
    InspectBit = 0x2,
}

// TODO: Profile_Global_List in 4coder_profile.h

Thread_Kind :: enum i32 {
    Main, MainCoroutine, AsyncTasks
}

Arena_Node :: struct {
    next: ^Arena_Node,
    prev: ^Arena_Node,
    arena: Arena,
    ref_counter: i32,
}

Thread_Context :: struct {
    kind: Thread_Kind,
    allocator: ^Base_Allocator,
    node_arena: Arena,
    used_first: [^]Arena_Node,
    used_last: [^]Arena_Node,
    free_arenas: [^]Arena_Node,

    prof_allocator: ^Base_Allocator,
    prof_id_counter: Profile_ID, // TODO
    prof_arena: Arena,
    prof_first: [^]Profile_Record, // TODO
    prof_last: [^]Profile_Record, // TODO
    prof_record_count: i32,

    user_data: rawptr,
}

// NOTE: NOT POD. This is probably fine as long as we keep disallowing RTTI.
Scratch_Block :: struct {
    tctx: ^Thread_Context,
    arena: ^Arena,
    temp: Temp_Memory,
}

// NOTE: NOT POD
Temp_Memory_Block :: struct {
    temp: Temp_Memory,
}

Heap_Basic_Node :: struct {
    next: ^Heap_Basic_Node,
    prev: ^Heap_Basic_Node,
}

Heap_Node :: struct {
    order: Heap_Basic_Node,
    alloc: Heap_Basic_Node,
    size: u64,
    padding: [64 - size_of(Heap_Basic_Node) - size_of(Heap_Basic_Node) - size_of(u64)]u8, // padding to 64B
}

Heap :: struct {
    arena_: Arena,
    arena: ^Arena,
    in_order: Heap_Basic_Node,
    free_nodes: Heap_Basic_Node,
    used_space: u64,
    total_space: u64,
}

ARGB_Color :: distinct [4]u8

// from custom/4coder_types.h

Thread_Context_Extra_Info :: struct {
    coroutine: rawptr,
    async_thread: rawptr,
}

Application_Links :: struct {
    tctx: ^Thread_Context,
    cmd_context: rawptr,
}

Custom_Layer_Init_Type :: #type proc "cdecl" (app: ^Application_Links)
Get_Version_Type :: #type proc "cdecl" (maj: i32, min: i32, patch: i32) -> b32
Init_APIs_Type :: #type proc "cdecl" (
    custom_vtable: ^API_VTable_custom,
    system_vtable: ^API_VTable_system
    ) -> Custom_Layer_Init_Type

ID_Color :: distinct u16

FColor_padded_a_byte :: struct {
    padding: [3]u8,
    a_byte: u8,
}

FColor_id :: struct {
    id: ID_Color,
    sub_index: u8,
    padding_: u8,
}

FColor :: struct #raw_union {
    an_byte: FColor_padded_a_byte,
    argb: ARGB_Color,
    id: FColor_id,
}

Theme_Color :: struct {
    tag: ID_Color,
    color: ARGB_Color,
}

Color_Picker :: struct {
    title: String_Const_u8,
    dest: ^ARGB_Color,
    finished: ^b32,
}

Fancy_String :: struct {
    next: ^Fancy_String,
    value: String_Const_u8,
    face: Face_ID,
    fore: FColor,
    pre_margin: f32,
    post_margin: f32,
}

Fancy_Line :: struct {
    next: ^Fancy_Line,
    face: Face_ID,
    fore: FColor,
    first: [^]Fancy_String,
    last: [^]Fancy_String,
}

Fancy_Block :: struct {
    first: [^]Fancy_Line,
    last: [^]Fancy_Line,
    line_count: i32,
}

Buffer_ID :: distinct i32
View_ID :: distinct i32
Panel_ID :: distinct i32
Text_Layout_ID :: distinct i32
Child_Process_ID :: distinct i32

UI_Highlight_Level :: enum i32 { None, Hover, Active }

Buffer_Point :: struct {
    line_number: i64,
    pixel_shift: [2]f32,
}

Line_Shift_Vertical :: struct {
    line: i64,
    y_delta: f32,
}

Line_Shift_Character :: struct {
    line: i64,
    character_delta: i64,
}

Child_Process_Set_Target_Flags :: enum u32 {
    FailIfBufferAlreadyAttachedToAProcess = 1,
    FailIfPRocessAlreadyAttachedToABuffer = 2,
    NeverOverrideExistingAttachment = 3,
    CursorAtEnd = 4,
}

Memory_Protect_Flags :: enum u32 {
    Read = 0x1, Write = 0x2, Execute = 0x4,
}

Wrap_Indicator_Mode :: enum i32 {
    Hide,
    Show_After_Line,
    Show_At_Wrap_Edge,
}

Global_Setting_ID :: enum i32 {
    Null, LAltLCtrlIsAltGr,
}

Buffer_Setting_ID :: enum i32 {
    Null, Unimportant, ReadOnly, RecordsHistory, Unkillable,
}

Character_Predicate :: struct {
    b: [32]u8,
}

Frame_Info :: struct {
    index: i32,
    literal_dt: f32,
    animation_dt: f32,
}

View_Setting_ID :: enum i32 {
    Null, ShowWhitespace, ShowScrollbar, ShowFileBar,
}

Buffer_Create_Flag :: enum u32 {
    Background = 0x1,
    AlwaysNew = 0x2,
    NeverNew = 0x4,
    JustChangedFile = 0x8,
    MustAttachToFile = 0x10,
    NeverAttachToFile = 0x20,
    SuppressNewFileHook = 0x40,
}

Buffer_Save_Flag :: enum u32 { IgnoreDirtyFlag = 0x1 }
Buffer_Kill_Flag :: enum u32 { Always_Kill = 0x2 }
Buffer_Reopen_Flag :: enum u32 {}

Buffer_Kill_Result :: enum u32 { Killed, Dirty, Unkillable, DoesNotExist }
Buffer_Reopen_Result :: enum u32 { Reopened, Failed }

Dirty_State :: enum i32 {
    UpToDate, UnsavedChanges, UnloadedChanges, UnsavedChangesAndUnloadedChanges,
}

Command_Line_Interface_Flag :: enum u32 {
    OverlapWithConflict = 0x1,
    AlwaysBindToView = 0x2,
    CursorAtEnd = 0x4,
    SendEndSignal = 0x8,
}

Set_Buffer_Flag :: enum u32 {
    KeepOriginalGUI = 0x1,
}

Mouse_Cursor_Show_Type :: enum i32 { Never, Always }

View_Split_Position :: enum i32 { Top, Bottom, Left, Right }
Panel_Split_Kind :: enum i32 { Ratio_Min, Ratio_Max, FixedPixels_Min, FixedPixels_Max }

Key_Modifier :: distinct u8

Mouse_State :: struct {
    l: b8,
    r: b8,
    press_l: b8,
    press_r: b8,
    release_l: b8,
    release_r: b8,
    out_of_window: b8,
    wheel: i32,
    p: [2]i32, // point/location
}

Parser_String_And_Type :: struct {
    str: cstring,
    length: u32,
    type: u32,
}

File_Attribute_Flag :: enum u32 { IsDirectory = 1 }
File_Attributes :: struct {
    size: u64,
    last_write_time: u64, // unit: microseconds
    flags: File_Attribute_Flag,
}

File_Info :: struct {
    next: ^File_Info,
    file_name: String_Const_u8,
    attributes: File_Attributes,
}

File_List :: struct {
    infos: [^]^File_Info,
    count: u32,
}

Buffer_Identifier :: struct {
    name: cstring,
    name_len: i32,
    id: Buffer_ID,
}

Buffer_Scroll_Rule :: enum i32 { NoCursorChange, SnapCursorIntoView }
Buffer_Scroll :: struct {
    position: Buffer_Point,
    target: Buffer_Point,
}

Basic_Scroll :: struct {
    position: [2]f32,
    target: [2]f32,
}

Buffer_Seek_Type :: enum i32 { pos, line_col }
Buffer_Seek_Pos :: struct { pos: i64 }
Buffer_Seek_Line_Col :: struct { line: i64, col: i64 }
Buffer_Seek_Data :: struct #raw_union {
    seek_pos: Buffer_Seek_Pos,
    seek_line_col: Buffer_Seek_Line_Col
}

Buffer_Seek :: struct {
    type: Buffer_Seek_Type,
    data: Buffer_Seek_Data,    
}

Buffer_Cursor :: struct {
    pos: i64,
    line: i64,
    col: i64,
}

Range_Cursor :: Range(Buffer_Cursor)

Marker :: struct {
    pos: i64,
    lean_right: b32,
}

Managed_Object_Type :: enum i32 { Error, Memory, Markers, COUNT }
Managed_ID :: distinct u64
Managed_Scope :: distinct u64
Managed_Object :: distinct u64

Marker_Visual :: struct {
    scope: Managed_Scope,
    slot_id: u32,
    gen_id: u32,
}

// @{ from custom/4coder_system_types.h
// NOTE: this file is somewhat concerningly generic and trying to cover a bit more bases than I'd like
Key_Mode :: enum i32 {
    LanguageArranged,
    Physical,
}

Plat_Handle :: struct #align(align_of(u64)) {
    d: [4]u32,
}

File_Handle :: struct #align(align_of(Plat_Handle)) {
    handle: os.Handle,
    padding: [size_of(Plat_Handle) - size_of(os.Handle)]u8,
}

System_Library :: struct #align(align_of(Plat_Handle)) {
    data: dynlib.Library,
    padding: [size_of(Plat_Handle) - size_of(dynlib.Library)]u8,
}

Thread_Start_Args :: struct {
    procedure: Thread_Function,
    arg: rawptr,
}

System_Thread :: struct #align(align_of(Plat_Handle)) {
    thrd: ^thread.Thread, // alloc'd (!); NOTE (sio): awkward name because name collision
    args: ^Thread_Start_Args, // allocated using default_allocator
// NOTE (sio): no padding necessary
//padding: [size_of(Plat_Handle) - size_of(^thread.Thread)]u8,
}

#assert(size_of(System_Thread) == size_of(Plat_Handle))

// NOTE (sio): the following can't be moved after init and are slab allocated
// do note this is likely to lead to false sharing. Probably not a big issue right now
// since we don't have many threads, but hey, might become an issue at some point.
System_Mutex :: struct #align(align_of(Plat_Handle)) {
    mutex: ^sync.Mutex, // alloc'd (!)
    padding: [size_of(Plat_Handle) - size_of(sync.Mutex)]u8,
}

System_Condition_Variable :: struct #align(align_of(Plat_Handle)){
    cond: ^sync.Cond, // alloc'd (!)
    padding: [size_of(Plat_Handle) - size_of(sync.Cond)]u8,
}

System_Timer :: struct #align(align_of(Plat_Handle)) {
    fd: ^i32,
    padding: [size_of(Plat_Handle) - size_of(i32)]u8,
}

Thread_Function :: #type proc "cdecl" (_: rawptr)

System_Pid :: struct #align(align_of(Plat_Handle)) {
    pid: Process_ID,
    padding: [size_of(Plat_Handle) - size_of(Process_ID)]u8,
}

CLI_Handles :: struct {
    process: System_Pid,
    out_read: File_Handle,
    out_write: File_Handle,
    in_read: File_Handle,
    in_write: File_Handle,
    scratch_space: [4]u32,
    exit: i32,
}

System_Path_Code :: enum i32 {
    Current_Directory,
    Binary,
    User_Directory,
}

Memory_Annotation_Node :: struct {
    next: ^Memory_Annotation_Node,
    location: String_Const_u8,
    address: rawptr,
    size: u64,
}

Memory_Annotation :: struct {
    first: [^]Memory_Annotation_Node,
    last: [^]Memory_Annotation_Node,
    count: i32,
}

// NOTE: this one isn't actually POD
Mutex_Lock :: struct {
    mutex: System_Mutex,
}
// @}

// from 4ed.h
// NOTE: MUST BE KEPT IN SYNC WITH C++ SIDE
MAX_VIEWS :: 16

Plat_Settings :: struct {
    custom_dll: cstring,
    custom_dll_is_strict: u8,
    fullscreen_window: b8,

    window_w: i32,
    window_h: i32,
    window_x: i32,
    window_y: i32,
    set_window_pos: b8,
    set_window_size: b8,
    maximize_window: b8,

    use_hinting: b8,

    user_directory: cstring,
}

Custom_API :: struct {
    get_version: Get_Version_Type,
    init_apis: Init_APIs_Type,
}

Application_Step_Result :: struct {
    mouse_cursor_type: Application_Mouse_Cursor,
    lctrl_lalt_is_altgr: b32,
    perform_kill: b32,
    animating: b32,
    has_new_title: b32,
    title_string: cstring,
}

Application_Step_Input :: struct {
    first_step: b32,
    dt: f32,
    mouse: Mouse_State,
    events: Input_List,
    clipboard: String_Const_u8,
    trying_to_kill: b32,
}

Log_Function :: #type proc "cdecl" (_: String_Const_u8) -> b32

App_Read_Command_Line_Sig :: #type proc "cdecl" (
    tctx: ^Thread_Context,
    current_directory: String_Const_u8,
    plat_settings: ^Plat_Settings,
    files: ^[^]cstring,
    file_count: ^[^]i32,
    argc: i32,
    argv: [^]cstring
    ) -> rawptr

App_Init_Sig :: #type proc "cdecl" (
    tctx: ^Thread_Context,
    target: ^Render_Target,
    base_ptr: rawptr,
    current_directory: String_Const_u8,
    api: ^Custom_API,
    )

App_Step_Sig :: #type proc "cdecl" (
    tctx: ^Thread_Context,
    target: ^Render_Target,
    base_ptr: rawptr,
    input: ^Application_Step_Input
    ) -> Application_Step_Result

App_Functions :: struct {
    load_vtables: proc "cdecl" (_: ^API_VTable_system, _: ^API_VTable_font, _: ^API_VTable_graphics),
    get_logger: proc "cdecl" () -> Log_Function,
    read_command_line: App_Read_Command_Line_Sig,
    init: App_Init_Sig,
    step: App_Step_Sig,
}

// keycodes (usually generated by 4ed_generate_keycodes.cpp, must be kept in sync with changes to that file)
KeyCode :: enum u32 {
    None,
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine,
    Space,
    Tick, Minus, Equal,
    LeftBracket, RightBracket,
    Semicolon, Quote, Comma, Period, ForwardSlash, BackwardSlash,
    Tab, Escape, Pause, Up, Down, Left, Right, Backspace, Return,
    Delete, Insert, Home, End, PageUp, PageDown,
    CapsLock, NumLock, ScrollLock, Menu,
    Shift, Control, Alt, Command,
    F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
    F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24,
    NumPad0, NumPad1, NumPad2, NumPad3, NumPad4,
    NumPad5, NumPad6, NumPad7, NumPad8, NumPad9,
    NumPadStar, NumPadPlus, NumPadMinus, NumPadDot, NumPadSlash,
    Ex0, Ex1, Ex2, Ex3, Ex4, Ex5, Ex6, Ex7, Ex8, Ex9,
    Ex10, Ex11, Ex12, Ex13, Ex14, Ex15, Ex16, Ex17, Ex18, Ex19,
    Ex20, Ex21, Ex22, Ex23, Ex24, Ex25, Ex26, Ex27, Ex28, Ex29,
}

key_code_name :: [KeyCode]String_Const_u8 {
    .None ="no key",
    
    .A = "A", .B = "B", .C = "C", .D = "D", .E = "E", .F = "F", .G = "G", .H = "H",
    .I = "I", .J = "J", .K = "K", .L = "L", .M = "M", .N = "N", .O = "O", .P = "P",
    .Q = "Q", .R = "R", .S = "S", .T = "T", .U = "U", .V = "V", .W = "W", .X = "X",
    .Y = "Y", .Z = "Z",
    .Zero = "0", .One = "1", .Two   = "2", .Three = "3", .Four = "4",
    .Five = "5", .Six = "6", .Seven = "7", .Eight = "8", .Nine = "9",
    .Space = "Space",
    
    .Tick = "Tick", .Minus = "Minus", .Equal = "Equal",
    .LeftBracket = "LeftBracket", .RightBracket = "RightBracket",
    .Semicolon = "Semicolon", .Quote = "Quote", .Comma = "Comma",.Period = "Period",
    .ForwardSlash = "ForwardSlash", .BackwardSlash = "BackwardSlash",
    
    .Tab = "Tab", .Escape = "Escape", .Pause = "Pause",
    .Up = "Up", .Down = "Down", .Left = "Left", .Right = "Right",
    .Backspace = "Backspace", .Return = "Return",
    .Delete = "Delete", .Insert = "Insert",
    .Home = "Home", .End = "End", .PageUp = "PageUp", .PageDown= "PageDown",
    .CapsLock= "CapsLock", .NumLock = "NumLock", .ScrollLock = "ScrollLock", .Menu = "Menu",
    .Shift = "Shift", .Control = "Control", .Alt = "Alt", .Command = "Command",
    
    .F1 = "F1", .F2 ="F2", .F3 = "F3", .F4 = "F4", .F5 = "F5", .F6 = "F6",
    .F7 = "F7", .F8 = "F8", .F9 = "F9", .F10 = "F10", .F11 = "F11", .F12 = "F12",
    .F13 = "F13", .F14 = "F14", .F15 = "F15", .F16 = "F16", .F17 = "F17", .F18 = "F18",
    .F19 = "F19", .F20 = "F20", .F21 = "F21", .F22 = "F22", .F23 = "F23", .F24 = "F24",
    
    .NumPad0 = "NumPad0",
    .NumPad1 = "NumPad1", .NumPad2 = "NumPad2", .NumPad3 = "NumPad3",
    .NumPad4 = "NumPad4", .NumPad5 = "NumPad5", .NumPad6 = "NumPad6",
    .NumPad7 = "NumPad7", .NumPad8 = "NumPad8", .NumPad9 = "NumPad9",
    .NumPadStar = "NumPadStar", .NumPadPlus = "NumPadPlus", .NumPadMinus = "NumPadMinus",
    .NumPadDot = "NumPadDot", .NumPadSlash = "NumPadSlash",
    
    .Ex0 = "Ex0", .Ex1 = "Ex1", .Ex2 = "Ex2", .Ex3 = "Ex3", .Ex4 = "Ex4",
    .Ex5 = "Ex5", .Ex6 = "Ex6", .Ex7 = "Ex7", .Ex8 = "Ex8", .Ex9 = "Ex9",
    .Ex10 = "Ex10", .Ex11 = "Ex11", .Ex12 = "Ex12", .Ex13 = "Ex13", .Ex14 = "Ex14",
    .Ex15 = "Ex15", .Ex16 = "Ex16", .Ex17 = "Ex17", .Ex18 = "Ex18", .Ex19 = "Ex19",
    .Ex20 = "Ex20", .Ex21 = "Ex21", .Ex22 = "Ex22", .Ex23 = "Ex23", .Ex24 = "Ex24",
    .Ex25 = "Ex25", .Ex26 = "Ex26", .Ex27 = "Ex27", .Ex28 = "Ex28", .Ex29 = "Ex29",
}

MouseCode :: enum u32 { None, Left, Middle, Right }

mouse_code_name :: [MouseCode]String_Const_u8 {
    .None = "no mouse button", .Left = "Left", .Middle = "Middle", .Right = "Right"
}

CoreCode :: enum u32 {
    None,
    Startup,
    Animate,
    Click_Activate_View,
    Click_Deactivate_View,
    Try_Exit,
    File_Externally_Modified,
    New_Clipboard_Contents,
}

core_code_name :: [CoreCode]String_Const_u8 {
    .None                     = "no core event code",
    .Startup                  = "Startup",
    .Animate                  = "Animate",
    .Click_Activate_View      = "Click_Activate_View",
    .Click_Deactivate_View    = "Click_Deactivate_View",
    .Try_Exit                 = "Try_Exit",
    .File_Externally_Modified = "File_Externally_Modified",
    .New_Clipboard_Contents   = "New_Clipboard_Contents",
}

// events
Custom_Command_Function :: #type proc "cdecl" (app: ^Application_Links)

Key_Code :: KeyCode
Mouse_Code :: distinct u32
Core_Code :: distinct u32

Input_Event_Kind :: enum u32 {
    None,
    TextInsert, KeyStroke, KeyRelease,
    MouseButton, MouseButtonRelease, MouseWheel, MouseMove,
    Core, CustomFunction,
    
    COUNT,
}

Key_Flags :: enum u32 {
    KeyFlag_IsDeadKey = (1 << 0),
}

Input_MaxModifierCount: i32 : 8

Input_Modifier_Set :: struct {
    mods: [^]Key_Code,
    count: i32,
}

Input_Modifier_Set_Fixed :: struct {
    mods: [Input_MaxModifierCount]Key_Code,
    count: i32,
}

Input_Event_Text :: struct {
    str: String_Const_u8,
    next_text: ^Input_Event,
    blocked: b32,
}

Input_Event_Key :: struct {
    code: Key_Code,
    flags: Key_Flags,
    modifiers: Input_Modifier_Set,
    
    // used internally (NOTE (sio): by what? relative to what? I presume relative to custom?)
    first_dependent_text: ^Input_Event,
}

Input_Event_Mouse :: struct {
    code: Mouse_Code,
    p: [2]i32,
    modifiers: Input_Modifier_Set,
}

Input_Event_Mouse_Wheel :: struct {
    value: f32,
    p: [2]i32,
    modifiers: Input_Modifier_Set,
}

Input_Event_Mouse_Move :: struct {
    p: [2]i32,
    modifiers: Input_Modifier_Set,
}

Input_Event_Core_Data_Flags_Filenames :: struct {
    flag_strings: Array(String_Const_u8),
    file_names: Array(String_Const_u8),
}

Input_Event_Core_Data :: struct #raw_union {
    str: String_Const_u8,
    id: i32,
    flags_filenames: Input_Event_Core_Data_Flags_Filenames,
}

Input_Event_Core :: struct {
    code: Core_Code,
    data: Input_Event_Core_Data,
}

Input_Event_Data :: struct #raw_union {
    text: Input_Event_Text,
    key: Input_Event_Key,
    mouse: Input_Event_Mouse,
    mouse_wheel: Input_Event_Mouse_Wheel,
    mouse_move: Input_Event_Mouse_Move,
    core: Input_Event_Core,
    custom_func: Custom_Command_Function,
};

Input_Event :: struct {
    kind: Input_Event_Kind,
    virtual_event: b32,
    data: Input_Event_Data,
};

Input_Event_Node :: struct {
    next: ^Input_Event_Node,
    event: Input_Event,
}

Input_List :: struct {
    first: [^]Input_Event_Node,
    last: [^]Input_Event_Node,
    count: i32,
}

Event_Property :: enum u32 {
    Any_Key          = 0x0001,
    Escape           = 0x0002,
    Any_Key_Release  = 0x0004,
    Mouse_Button     = 0x0008,
    Mouse_Release    = 0x0010,
    Mouse_Wheel      = 0x0020,
    Mouse_Move       = 0x0040,
    Animate          = 0x0080,
    View_Activation  = 0x0100,
    Text_Insert      = 0x0200,
    Any_File         = 0x0400,
    Startup          = 0x0800,
    Exit             = 0x1000,
    Clipboard        = 0x2000,
    Custom_Function  = 0x4000,
}

Event_Property_Flags :: enum {
    Any_Key          = 0,
    Escape           = 1,
    Any_Key_Release  = 2,
    Mouse_Button     = 3,
    Mouse_Release    = 4,
    Mouse_Wheel      = 5,
    Mouse_Move       = 6,
    Animate          = 7,
    View_Activation  = 8,
    Text_Insert      = 9,
    Any_File         = 10,
    Startup          = 11,
    Exit             = 12,
    Clipboard        = 13,
    Custom_Function  = 14,
}
Event_Property_Group :: bit_set[Event_Property_Flags]

Any_Keyboard_Event :: Event_Property_Group {
    .Any_Key, .Escape, .Any_Key_Release, .Text_Insert
}

Any_Mouse_Event :: Event_Property_Group {
    .Mouse_Button, .Mouse_Release, .Mouse_Wheel, .Mouse_Move,
}

Any_User_Input :: Any_Keyboard_Event | Any_Mouse_Event

Any_Core :: Event_Property_Group {
    .Animate, .View_Activation, .Any_File, .Startup, .Exit, .Clipboard, .Animate,
}

Any_Event :: Any_User_Input | Any_Core | { .Custom_Function }

Query_Bar :: struct {
    prompt: String_Const_u8,
    string: String_Const_u8,
    string_capacity: u64,
}

Query_Bar_Ptr_Array :: Array(Query_Bar)

// NOT POD
Query_Bar_Group :: struct {
    app: ^Application_Links,
    view: View_ID,
}

// NOTE (sio): I'm not implementing support for 4coder tables if AT ALL avoidable

Process_State :: struct {
    valid: b32,
    is_updating: b32,
    return_code: i64,
}

String_Match_Flag :: enum {
    Case_Sensitive = 0,
    Left_Side_Sloppy = 1,
    Right_Side_Sloppy = 2,
    Straddled = 3
}

String_Match_Flags :: bit_set[String_Match_Flag; u32]

String_Match :: struct {
    next: ^String_Match,
    buffer: Buffer_ID,
    string_id: i32,
    flags: String_Match_Flags,
    range: Range(i64),
}

String_Match_List :: struct {
    first: [^]String_Match,
    last: ^String_Match,
    count: i32,
}

Layout_Item_Flag :: enum {
    Special_Character = 0,
    Ghost_Character = 1,
}

Layout_Item_Flags :: bit_set[Layout_Item_Flag; u32]

Layout_Item :: struct {
    index: i64,
    codepoint: u64,
    flags: Layout_Item_Flags,
    padded_y1: f32,
    rect: Rect(f32),
}

Layout_Item_Block :: struct {
    next: ^Layout_Item_Block,
    items: [^]Layout_Item,
    item_count: i64,
    character_count: i64,
    face: Face_ID,
}

Layout_Item_List :: struct {
    first: [^]Layout_Item_Block,
    last: ^Layout_Item_Block,
    item_count: i64,
    character_count: i64,
    node_count: i32,
    height: f32,
    bottom_padding: f32,
    input_index_range: Range(i64),
    manifested_index_range: Range(i64),
}

Layout_Function :: #type proc "cdecl" (
    app: ^Application_Links,
    arena: ^Arena,
    buffer: Buffer_ID,
    range: Range(i64),
    face: Face_ID,
    width: f32
    ) -> Layout_Item_List

Delta_Rule_Function :: #type proc "cdecl" (
    pending: [2]f32,
    is_new_target: b32,
    dt: f32,
    data: rawptr
    ) -> [2]f32

Render_Caller_Function :: #type proc "cdecl" (
    app: ^Application_Links,
    frame_info: Frame_Info,
    view: View_ID
    )

Whole_Screen_Render_Caller_Function :: #type proc "cdecl" (
    app: ^Application_Links,
    frame_info: Frame_Info
    )

View_Context :: struct {
    render_caller: Render_Caller_Function,
    delta_rule: Delta_Rule_Function,
    delta_rule_memory_size: u64,
    hides_buffer: b32,
    mapping: rawptr, // NOTE (sio): actually a ^Mapping
    map_id: i64,
}

Hook_ID :: enum i32 {
    Tick = 0,
    Render_Caller,
    Whole_Screen_Render_Caller,
    Delta_Rule,
    Buffer_Viewer_Update,
    View_Event_Handler,
    Buffer_Name_Resolver,
    Begin_Buffer,
    End_Buffer,
    New_File,
    Save_File,
    Buffer_Edit_Range,
    Buffer_Region,
    Layout,
    View_Change_Buffer
}

Hook_Function :: #type proc "cdecl" (app: ^Application_Links) -> i32

User_Input :: struct {
    event: Input_Event,
    abort: b32,
}

History_Record_Index :: distinct i32

Record_Error :: enum i32 {
    No_Error = 0,
    Invalid_Buffer,
    No_History_Attached,
    Index_Out_Of_Bounds,
    Sub_Index_Out_Of_Bounds,
    Initial_State_Dummy_Record,
    Wrong_Record_Type_At_Index,
}

Record_Info_Single_String :: struct {
    single_string_forward: String_Const_u8,
    single_string_backward: String_Const_u8,
    single_first: i64,
}

Record_Info_Variants :: struct #raw_union
{
    single: Record_Info_Single_String,
    group_count: i32,
}

Record_Kind :: enum i32 { Single = 0, Group }

Record_Info :: struct {
    error: Record_Error,
    kind: Record_Kind,
    pos_before_edit: i64,
    edit_number: i32,
    variants: Record_Info_Variants,
}

Record_Merge_Flag :: enum {
    State_In_range_Move_State_Forward = 0,
    State_In_Range_Move_State_Backward = 1,
    State_In_Range_Error_Out = 2,
}

Record_Merge_Flags :: bit_set[Record_Merge_Flag; u32]

Edit :: struct {
    text: String_Const_u8,
    range: Range(u64),
}

Batch_Edit :: struct {
    next: ^Batch_Edit,
    edit: Edit,
}

Application_Mouse_Cursor :: enum i32 {
    DEFAULT = 0, ARROW, IBEAM, LEFTRIGHT, UPDOWN, COUNT
}

/*
Mapping :: struct {
    node_arena: Arena,
    heap: Heap, // TODO
    // TODO
}

Binding_Match_Rule :: enum i32 { Strict = 0, Loose }

Map_Event_Breakdown :: struct {
    mod_set: &Input_Modifier_Set,
    key: u64,
    skip_self_mode: Key_Code,
}

Doc_Cluster :: ??? // TODO

Profile_Global_List :: List(Profile_Global) ??? // TODO
*/