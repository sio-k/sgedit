/*
* Sio Kreuzer
*
* 2024-01-28T01:54
*
* typedefs for major Odin stdlib types, for now so I can keep them around in structs here
* not designed to replicate actual contents in any way, or make them accessible, just
* for storage and struct compat reasons
*
* NOTE (sio): POTENTIALLY INCOMPATIBLE WITH 32-BIT (!)
*/

#ifndef ODIN_TYPES_H
#define ODIN_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

    struct Odin_Allocator {
        void* procedure;
        void* private_data;
    };
    
    struct Odin_String {
        void* data;
        void* len;
    };
    
    struct Odin_Slice {
        void* data;
        void* len;
    };
    
    struct Odin_Dynamic_Array {
        void* data;
        void* len;
        void* cap;
        Odin_Allocator allocator;
    };
    
    struct Odin_Any {
        void* rawptr;
        void* id; // type id
    };
    
    struct Odin_Cstring {
        char* data;
    };
    
    struct Odin_Soa_Pointer {
        void* data;
        void* index;
    };
    
    struct Odin_Map {
        // NOTE (sio): THIS IS NOT JUST A POINTER. DO NOT USE IT AS SUCH DIRECTLY.
        // for details, refer to:
        // -> Odin implementation: core:runtime (in core.odin)
        // -> C++ implementation (if that helps you, this is the implementation that this may
        //        very well have come from): github.com/sio-k/LibSio (in HashMap.hpp)
        void* data;
        void* len;
        Odin_Allocator allocator;
    };
    
#ifdef __cplusplus
}
#endif // __cplusplus
    
#endif //ODIN_TYPES_H
