sgedit
------

# What is this?
This is a graphical text editor based on the popular 4coder's open source source code release, which may be found at https://github.com/Dion-Systems/4coder

Do note that a major restructuring of the code is currently in progress, as I'm trying to
slowly change it so it's all Odin instead of C++. This is pretty much just so I can maintain
it better for my own use. Also, it's decent practice in refactoring a codebase that is
showing it's age.

# Why the name?
It stands for *S*io's *g*raphical *edit*or. I couldn't come up with a better name. It was suggested I just call it Theresa, and I think I'll have to consider that some more, amusing as I find it I'm not sure I want to call a text editor Theresa. But if I can't come up with anything better, that's what it'll be called.

# Who made this?
Mostly: Allan Webster, with help from Casey Muratori, "insofaras" Alaex Baines, Yuval Dolev, and Ryan Fleury.

Sio Kreuzer has taken the produced well-functioning editor, and made it less generally well-functioning for their own use.

# Notable changes so far
- Build system switched from custom C++ to CMake. Build works.
- Sound support removed.
- Support for everything but Linux is gone (but should be easier to add in again)
- Optimizations were turned off (with the optional exception of `march=native`), debug symbols are now always present.
- platform layer switch to SDL2/GL4.6
- GL pointer loading handled by vendor:opengl library

# Further plans
- rewrite parts of the source code in Odin over time until everything's converted, starting with the platform layer
- switch to truetype fonts and improved font rendering using stb_truetype.h and GPU-based font rendering
- general efficiency improvements to lower RAM use (CPU time use is already fairly low)
- using ISPC to generate vectorized code where sensible and useful (e.g. parsing)
- allow the platform layer to (optionally automatically) recompile and reload the custom so/DLL

# Build
You've got a few options:
- use the Nix derivation `default.nix` and build using Nix
- build in a Nix shell using `./nix-shell.sh` if you're on NixOS, steps same as for regular build
- install all dependencies using your system package manager and build using `./configure.sh && ./build.sh && ./install.sh`

# OS support
I can neither verify nor am I particularly interested in Windows or Mac support, sorry. It's likely to mostly fall out of the planned platform layer switch to SDL, Odin & GLEW.

Supported OSs: GNU/Linux, specifically NixOS.

# Dependencies (runtime)
- freetype 2
- SDL2

# Dependencies (build time)
- all runtime dependencies (with headers)
- pkg-config
- GNU binutils
- CMake
- GNU Make or Ninja
- LLVM Clang with compiler-rt and clang++ (tested with versions 16 and 6)
- Odin compiler

# Future dependencies (build time, present in .nix file)
- Intel SPMD Program Compiler (ISPC)

# License
MIT licensed, just like the original codebase.

```
Copyright (c) 2022 Allen Webster
Copyright (c) 2024 Sio Kreuzer (new build system, Odin rewrite, misc. changes)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

# Pull requests
I'll gladly accept pull requests.

# Issues
You can report issues, but I will very likely not be fixing them. I do accept pull requests, though, and you can of course always fork this repository and fix the issue for yourself somewhere.
