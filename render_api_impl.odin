// all the data & functions to implement the "render API" to the app bits
package main

Texture_Kind :: enum i32 { Error, Mono }

Graphics_Get_Texture_Sig :: #type proc "cdecl" (
    dim: [3]i32,
    texture_kind: Texture_Kind
    ) -> u32

Graphics_Fill_Texture_Sig :: #type proc "cdecl" (
    texture_kind: Texture_Kind,
    texture: u32,
    p: [3]i32,
    dim: [3]i32,
    data: rawptr
    ) -> b32

API_VTable_graphics :: struct {
    get_texture: Graphics_Get_Texture_Sig,
    fill_texture: Graphics_Fill_Texture_Sig,
}

graphics_get_texture :: proc "cdecl" (dim: [3]i32, texture_kind: Texture_Kind) -> u32 {
    context = default_context()
    return get_texture(dim, texture_kind)
}

graphics_fill_texture :: proc "cdecl" (
    texture_kind: Texture_Kind,
    texture: u32,
    p: [3]i32,
    dim: [3]i32,
    data: rawptr
    ) -> b32
{
    context = default_context()
    return fill_texture(texture_kind, texture, p, dim, data)
}

graphics_api_fill_vtable :: proc "cdecl" (vtable: ^API_VTable_graphics) {
    context = default_context()
    vtable.get_texture = graphics_get_texture
    vtable.fill_texture = graphics_fill_texture
}

graphics_api_read_vtable :: proc "cdecl" (vtable: ^API_VTable_graphics) {
    context = default_context()
    assert(false)
}

Render_Free_Texture :: struct {
    next: ^Render_Free_Texture,
    tex_id: u32,
}

Render_Vertex :: struct {
    xy: [2]f32,
    uvw: [3]f32,
    color: u32,
    half_thickness: f32,
}

Render_Vertex_Array_Node :: struct {
    next: ^Render_Vertex_Array_Node,
    vertices: [^]Render_Vertex,
    vertex_count: i32,
    vertex_max: i32,
}

Render_Vertex_List :: struct {
    first: ^Render_Vertex_Array_Node,
    last: ^Render_Vertex_Array_Node,
    vertex_count: i32,
}

Render_Group :: struct {
    next: ^Render_Group,
    vertex_list: Render_Vertex_List,
    // params
    face_id: Face_ID,
    clip_box: Rect(f32),
}

Render_Target :: struct {
    clip_all: b8,
    width: i32,
    height: i32,
    bound_texture: u32,
    color: u32,
    
    frame_index: i32,
    literal_dt: f32,
    animation_dt: f32,
    
    free_texture_first: [^]Render_Free_Texture,
    free_texture_last: [^]Render_Free_Texture,
    
    // TODO: ensure that the enqueue/dequeue functions don't try to use the arena here
    arena: Arena, // unused by actual renderer
    group_first: [^]Render_Group,
    group_last: [^]Render_Group,
    group_count: i32,
    
    current_face_id: Face_ID,
    current_clip_box: Rect(f32),
    font_set: ^Font_Set,
    fallback_texture_id: u32,
}
