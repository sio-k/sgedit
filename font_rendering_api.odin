package main

import "core:dynlib"
import "core:mem"
import "vendor:stb/truetype"

// TODO: just use stb_tt, render... uh. Dunno. Need to figure that out.

FT_Codepoint_Index_Pair :: struct {
    codepoint: u64,
    index: u32,
}

// size: 40B (min. 36B) stored
Glyph_Bounds :: struct {
    uv: Rect(f32),
    w: f32,
    // possible/likely 4B padding
    xy_off: Rect(f32),
}

Face :: struct {
    description: Face_Description,
    id: Face_ID,
    version_number: i32,
    
    metrics: Face_Metrics,
    
    advance_map: Face_Advance_Map,
    bounds: [^]Glyph_Bounds,
    white: Glyph_Bounds,
    
    texture_kind: Texture_Kind,
    texture: u32,
    texture_dim: [3]f32,
}

Face_ID :: distinct u32
INVALID_FACE_ID :: Face_ID(0)

Glyph_Flag :: enum u32 { None }

Font_Load_Location :: struct {
    // NOTE (sio): allocated using the default heap allocator
    file_name: String_Const_u8,
}

Face_Antialiasing_Mode :: enum u32 { EightBitMono, OneBitMono }

Face_Load_Parameters :: struct {
    pt_size: u32,
    aa_mode: Face_Antialiasing_Mode,
    bold: b8,
    italic: b8,
    underline: b8,
    hinting: b8,
}

Face_Description :: struct {
    font: Font_Load_Location,
    parameters: Face_Load_Parameters,
}

Face_Metrics :: struct {
    text_height: f32,
    line_height: f32,
    ascent: f32,
    descent: f32,
    line_skip: f32,
    
    underline_yoff1: f32,
    underline_yoff2: f32,
    
    max_advance: f32,
    space_advance: f32,
    decimal_digit_advance: f32,
    hex_digit_advance: f32,
    byte_advance: f32,
    byte_sub_advances: [3]f32,
    normal_lowercase_advance: f32,
    normal_uppercase_advance: f32,
    normal_advance: f32,
}

// TODO (sio): I don't think I actually need to implement this since it really only
// lives in the custom/ bits?
// and once I get to that for the switch to Odin, I don't even need it for binary compat
// anymore I think
// NOTE (sio): this is also used in render_target for some reason, I should figure out
// the details of that
when false {
    
    Codepoint_Index_Map :: struct {
        has_zero_index: b32,
        zero_index: u16,
        max_index: u16,
        table: Table_u32_u16, // TODO: Table?
    }
    
}

// TODO: actually get this into the C++ side somehow
CIM_ASCII_MIN :: 0x20 // ' '
CIM_ASCII_MAX :: 0x7F  // DEL - ' '
CIM_ASCII_LENGTH :: (CIM_ASCII_MAX + 1) - CIM_ASCII_MIN

Ascii_Codepoint_Index :: struct {
    index: u32,
    populated: b32,
}

Codepoint_Index_Map :: struct {
    // contains exactly up to 0x7F, i.e. max. all valid ASCII chars
    // lower bound is 0x20, i.e. space, i.e. the lowest printable char
    // specially out here because small & common case
    ascii: [CIM_ASCII_LENGTH]Ascii_Codepoint_Index,
    unicode: map[u64]u32,
    max_index: u32,
}

codepoint_index_map_read :: proc "cdecl" (
    cim: ^Codepoint_Index_Map,
    codepoint: u64,
    index_out: ^u32
    ) -> (ok: b32)
{
    index_out^, ok = lookup_ci_map(cim, codepoint) 
    return
}

codepoint_index_map_count :: proc "cdecl" (cim: ^Codepoint_Index_Map) -> u32 {
    return cim.max_index + 1
}

lookup_ci_map :: proc "contextless" (
    cim: ^Codepoint_Index_Map,
    codepoint: u64
    ) -> (idx: u32, ok: b32)
{
    if codepoint >= CIM_ASCII_MIN && codepoint <= CIM_ASCII_MAX {
        aci := cim.ascii[codepoint]
        idx = aci.index
        ok = aci.populated
    } else {
        k: bool
        idx, k = cim.unicode[codepoint]
        ok = b32(k)
    }
    return
}

Face_Advance_Map :: struct {
    codepoint_to_index: Codepoint_Index_Map,
    advance: [^]f32,
    index_count: u32,
}

Font_Set :: struct {
    // the allocator used by both the map and the freelist is the system allocator
    next_id_counter: Face_ID,
    id_freelist: [dynamic]Face_ID, // likely unnecessary
    id_to_face_table: [dynamic]^Face, // indexed by FaceID
    scale_factor: f32,
}


Font_Make_Face_Sig :: #type proc "cdecl" (
    arena: ^Arena, // ignored
    description: ^Face_Description,
    scale_factor: f32,
    ) -> ^Face

// "interface" side of font_make_face
font_make_face_interface :: proc "cdecl" (
    arena: ^Arena,
    description: ^Face_Description,
    scale_factor: f32
    ) -> ^Face
{
    context = default_context()
    face, ok := font_make_face(description, scale_factor)
    if !ok {
        return nil
    } else {
        return face
    }
}

// actual implementation, main difference: has a context
font_make_face :: proc(
    description: ^Face_Description,
    scale_factor: f32
    ) -> (^Face, bool)
{
    face, err := ft_font_make_face(description, scale_factor);
    return face, err == .Ok
}

font_release_face :: proc(face: ^Face) {
    ft_font_release_face(face)
}

API_VTable_font :: struct {
    make_face: Font_Make_Face_Sig,
    // TODO: add release_face to this vtable
}

font_api_fill_vtable :: proc "cdecl" (vtable: ^API_VTable_font) {
    context = default_context()
    vtable.make_face = font_make_face_interface
}

// NOTE (sio): we implement these functions and do not allow overriding them
font_api_read_vtable :: proc "cdecl" (vtable: ^API_VTable_font) {
    context = default_context()
    assert(false)
}

Cim_Setup_Proc_Type :: #type proc "cdecl" (
    map_read: rawptr,
    map_count: rawptr,
    glyph_advance: rawptr,
    max_glyph_advance: rawptr,
    average_glyph_advance: rawptr
    )
      
Font_Set_Setup_Proc_Type :: #type proc "cdecl" (
    fs_alloc: rawptr,
    fs_free: rawptr,
    fs_init: rawptr,
    fs_new: rawptr,
    fs_release: rawptr,
    from_id: rawptr,
    fallback: rawptr,
    largest: rawptr,
    modify: rawptr
    )

font_setup_api :: proc(cim_setup: Cim_Setup_Proc_Type, font_set_setup: Font_Set_Setup_Proc_Type) {
    if cim_setup != nil {
        cim_setup(
            transmute(rawptr) codepoint_index_map_read,
            transmute(rawptr) codepoint_index_map_count,
            transmute(rawptr) font_get_glyph_advance,
            transmute(rawptr) font_get_max_glyph_advance_range,
            transmute(rawptr)  font_get_average_glyph_advance_range
            );
    }
    
    if font_set_setup != nil {
        font_set_setup(
            transmute(rawptr) font_set__alloc_face_id,
            transmute(rawptr) font_set__free_face_id,
            transmute(rawptr) font_set_init,
            transmute(rawptr) font_set_new_face,
            transmute(rawptr) font_set_release_face,
            transmute(rawptr) font_set_face_from_id,
            transmute(rawptr) font_set_get_fallback_face,
            transmute(rawptr) font_set_get_largest_id,
            transmute(rawptr) font_set_modify_face
            );
   }
}

font_setup_api_in_lib :: proc(lib: dynlib.Library) {
    Cim_Setup_Proc_Type :: #type proc "cdecl" (
        map_read: rawptr,
        map_count: rawptr,
        glyph_advance: rawptr,
        max_glyph_advance: rawptr,
        average_glyph_advance: rawptr
        )
        
    Font_Set_Setup_Proc_Type :: #type proc "cdecl" (
        fs_alloc: rawptr,
        fs_free: rawptr,
        fs_init: rawptr,
        fs_new: rawptr,
        fs_release: rawptr,
        from_id: rawptr,
        fallback: rawptr,
        largest: rawptr,
        modify: rawptr
        )
    
    addr, ok := dynlib.symbol_address(lib, "codepoint_index_map_setup");
    log_if(true, ok, "failed to find codepoint index map setup proc");
    if !ok {
        addr = nil;
    }
    
    cim_setup := transmute(Cim_Setup_Proc_Type) addr;
    
    addr, ok = dynlib.symbol_address(lib, "font_set_setup");
    log_if(true, ok, "failed to find font set setup proc");
    if !ok {
        addr = nil;
    }
    
    font_set_setup := transmute(Font_Set_Setup_Proc_Type) addr;
    
    font_setup_api(cim_setup, font_set_setup);
}
