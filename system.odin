package main

import "core:dynlib"
import "core:fmt"
import "core:mem"
import "core:mem/virtual"
import "core:os"
import rt "core:runtime"
import "core:slice"
import "core:strings"
import "core:sync"
import "core:thread"
import "core:time"

import "vendor:sdl2"

foreign import syslibc "system:c"

// NOTE (sio): this is the maximum amount of files in a directory we handle
// Any more than this and the directory routines just emit an error.
// You're unlikely to hit this limit... I'd hope.
MAX_FILES_IN_DIR :: 8192

System_Error_Box_Sig :: #type proc "cdecl" (msg: cstring)
system_error_box :: proc "cdecl" (msg: cstring) {
    context = default_context()
    error_box(string(msg))
}

System_Get_Path_Sig :: #type proc "cdecl" (
    arena: ^Arena,
    path_code: System_Path_Code
    ) -> String_Const_u8
system_get_path :: proc "cdecl"(
    arena: ^Arena,
    path_code: System_Path_Code
    ) -> (res: String_Const_u8)
{
    context = default_context()
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    context.allocator = odin_alloc_from_arena(arena)
        
    switch path_code {
        case .Current_Directory:
            res = String_Const_u8(os.get_current_directory())
            
            case .Binary:
            str, err := os.absolute_path_from_relative("/proc/self/exe")
            if err != os.ERROR_NONE {
                res = String_Const_u8(str)
            }
            
        case .User_Directory:
            user_dir_override, do_override := lnx_override_user_directory.?
            if !do_override {
                res =
                    String_Const_u8(
                        fmt.aprintf(
                            "$s/.sgedit/",
                            os.get_env("HOME", context.temp_allocator)
                            )
                        )
            } else {
                res = String_Const_u8(strings.clone(string(user_dir_override)))
            }
    }
    
    return
}

System_Get_Canonical_Sig :: #type proc "cdecl" (
    arena: ^Arena,
    name: String_Const_u8
    ) -> String_Const_u8
system_get_canonical :: proc "cdecl"(
    arena: ^Arena,
    name: String_Const_u8
    ) -> (res: String_Const_u8 = "")
{
    context = default_context()
    context.allocator = odin_alloc_from_arena(arena)
    str, err := os.absolute_path_from_relative(string(name))
    if err == os.ERROR_NONE {
        res = String_Const_u8(str)
    }    
    return
}

// returns { infos = nil, count = 0 } on error
System_Get_File_List_Sig :: #type proc "cdecl" (
    arena: ^Arena,
    directory: String_Const_u8
    ) -> File_List
system_get_file_list :: proc "cdecl"(
    arena: ^Arena,
    directory: String_Const_u8
    ) -> (ret: File_List)
{
    context = default_context()
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    
    context.allocator = context.temp_allocator
    
    dir_listing: []os.File_Info
    // obtain directory listing
    {
        handle, err := os.open(string(directory))
        if handle == os.INVALID_HANDLE {
            log_os("Failed to open file", directory, "to get directory contents; got error:", err)
            return
        }
        defer os.close(handle)
        
        dir_stat_info: os.File_Info
        dir_stat_info, err = os.fstat(handle)
        if !dir_stat_info.is_dir {
            log_os("Attempt to get directory contents of non-directory file", directory)
            return
        }
        
        dir_listing, err = os.read_dir(handle, MAX_FILES_IN_DIR);
        if err != os.ERROR_NONE {
            log_os("Failed to get contents of directory", directory, "; reason:", err)
        }
    }
    
    // sort dir_listing in-place by ... however compare_file_infos sorts it?
    // using insertion sort because I'm confident I can implement that correctly right now
    {
        for i := 1; i < len(dir_listing); i += 1 {
            for j := i; j > 0; j -= 1 {
                if compare_file_infos(&dir_listing[j - 1], &dir_listing[j]) != .A_GT_B {
                    break
                }
                tmp := dir_listing[j]
                dir_listing[j] = dir_listing[j - 1]
                dir_listing[j - 1] = tmp
            }
        }
    }
    
    // allocate ret.infos, converting and inserting into ret
    {
        ret_infos_mem, err :=
            make([]File_Info, len(dir_listing), odin_alloc_from_arena(arena))
        if err != .None {
            log_os("Failed to allocate space for directory contents in arena:", err)
            return
        }
        defer if err != .None { delete(ret_infos_mem, odin_alloc_from_arena(arena)) }
        
        // allocate array-of-pointers, stuff pointers into that I guess
        ret_infos: []^File_Info
        ret_infos, err = make([]^File_Info, len(dir_listing), odin_alloc_from_arena(arena))
            
        if err != .None {
            log_os(
                "Failed to allocate space for directory contents pointers in arena:",
                err
                )
            return
        }
        defer if err != .None { delete(ret_infos, odin_alloc_from_arena(arena)) }
            
        ret.count = u32(len(dir_listing))
        ret.infos = raw_data(ret_infos)
        for os_info, index in dir_listing {
            next: ^File_Info = nil
                
            if index < len(dir_listing) - 1 {
                next = &(ret_infos_mem[index + 1])
            }
            
            ret.infos[index] = &ret_infos_mem[index]
            ret.infos[index]^ = odin_file_info_to_internal_file_info(arena, os_info, next)
        }
    }
    
    return
}

System_Quick_File_Attributes_Sig :: #type proc "cdecl" (
    scratch: ^Arena,
    file_name: String_Const_u8
    ) -> File_Attributes
system_quick_file_attributes :: proc "cdecl"(
    scratch: ^Arena,
    file_name: String_Const_u8
    ) -> File_Attributes
{
    context = default_context()
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    
    context.allocator = context.temp_allocator
    
    finfo, err := os.lstat(string(file_name))
    if err != os.ERROR_NONE {
        log_os("Failed to stat", file_name, "got error", err)
        return File_Attributes {}
    }
    
    finfo_internal := odin_file_info_to_internal_file_info(scratch, finfo)
    return finfo_internal.attributes
}

System_Load_Handle_Sig :: #type proc "cdecl" (
    scratch: ^Arena,
    file_name: cstring,
    out: ^File_Handle
    ) -> b32
system_load_handle :: proc "cdecl"(
    scratch: ^Arena,
    file_name: cstring,
    out: ^File_Handle
    ) -> b32
{
    context = default_context()
    handle, err := os.open(string(file_name), os.O_RDONLY)
    if err != os.ERROR_NONE {
        log_os("Failed to open file, got errno:", err)
        return b32false
    }
    out.handle = handle
    return b32true
}

System_Load_Attributes_Sig :: #type proc "cdecl" (handle: File_Handle) -> File_Attributes
system_load_attributes :: proc "cdecl" (handle: File_Handle) -> (res: File_Attributes) {
    context = default_context()
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
    
    context.allocator = context.temp_allocator
    
    finfo, err := os.fstat(handle.handle)
    if err != os.ERROR_NONE {
        log_os("Failed to stat handle", handle.handle, "; got errno", err)
        return
    }
  
    res.size = u64(finfo.size)
    res.last_write_time = u64(finfo.modification_time._nsec) / 1000
    res.flags = .IsDirectory if finfo.is_dir else File_Attribute_Flag(0)
        
    return
}

System_Load_File_Sig :: #type proc "cdecl" (
    handle: File_Handle,
    buffer: cstring,
    size: u32
    ) -> b32
system_load_file :: proc "cdecl"(
    handle: File_Handle,
    buffer: cstring,
    size: u32
    ) -> b32
{
    context = default_context()
    
    bytes_read, err := os.read(handle.handle, (transmute([^]u8) buffer)[:size])
    if err != os.ERROR_NONE {
        log_os(
            "Failed to read",
            size,
            "bytes from handle",
            handle.handle,
            "; got errno",
            err
            )
        return b32false
    }
    
    return b32(bytes_read == int(size))
}

System_Load_Close_Sig :: #type proc "cdecl" (handle: File_Handle) -> b32
system_load_close :: proc "cdecl" (handle: File_Handle) -> b32 {
    context = default_context()
    err := os.close(handle.handle)
    if err != os.ERROR_NONE {
        log_os("Failed to close handle", handle.handle, "; got errno", err)
        return b32false
    }
    return b32true
}

System_Save_File_Sig :: #type proc "cdecl" (
    scratch: ^Arena,
    file_name: cstring,
    data: []u8
    ) -> File_Attributes
system_save_file :: proc "cdecl" (
    scratch: ^Arena,
    file_name: cstring,
    data: []u8
    ) -> (ret: File_Attributes)
{
    context = default_context()
    handle, err := os.open(string(file_name), os.O_TRUNC | os.O_WRONLY | os.O_CREATE, 0o666)
    if err != os.ERROR_NONE {
        log_os(
            "Failed to open file",
            file_name,
            "for writing/creating, truncating, with permission bits (a+rw, g+rw, u+rw)",
            "; got errno",
            err
            )
        return
    }
    defer os.close(handle)
        
    bytes_written: int
    bytes_written, err = os.write(handle, data)
    if err != os.ERROR_NONE {
        log_os(
            "Failed to write file contents of length",
            len(data),
            "to file",
            file_name,
            "; got errno",
            err
            )
        return
    }
    
    ret = system_load_attributes(File_Handle { handle, {} })
    
    return
}

System_Load_Library_Sig :: #type proc "cdecl" (
    scratch: ^Arena,
    file_name: String_Const_u8,
    out: ^System_Library
    ) -> b32
system_load_library :: proc "cdecl" (
    scratch: ^Arena,
    file_name: String_Const_u8,
    out: ^System_Library
    ) -> b32
{
    context = default_context()
    lib, ok := load_lib(string(file_name))
    if !ok {
        return b32false
    }
    
    out.data = lib
    return b32true
}

System_Release_Library_Sig :: #type proc "cdecl" (handle: System_Library) -> b32
system_release_library :: proc "cdecl" (handle: System_Library) -> b32 {
    context = default_context()
    return b32(dynlib.unload_library(handle.data))
}

System_Get_Proc_Sig :: #type proc "cdecl" (
    handle: System_Library,
    proc_name: cstring
    ) -> Void_Func
system_get_proc :: proc "cdecl" (handle: System_Library, proc_name: cstring) -> Void_Func
{
    context = default_context()
    addr, found := dynlib.symbol_address(handle.data, string(proc_name))
    if !found {
        addr = nil // ensure addr is, indeed, 0
    }
    
    return transmute(Void_Func) addr
}

// TODO (sio): implement actual timezone-related stuff properly

// unit: microseconds
System_Now_Time_Sig :: #type proc "cdecl" () -> u64
system_now_time :: proc "cdecl" () -> u64 {
    context = default_context()
    return u64(time.now()._nsec) / 1000
}

// "universal" as in "UTC"
System_Now_Date_Time_Universal_Sig :: #type proc "cdecl" () -> Date_Time
system_now_date_time_universal :: proc "cdecl" () -> Date_Time {
    context = default_context()
    return odin_time_to_date_time(time.now())
}

// convert from dt to time.Time, then do arithmetic, then back again
System_Local_Date_Time_From_Universal_Sig :: #type proc "cdecl" (
    date_time: ^Date_Time
    ) -> Date_Time
system_local_date_time_from_universal :: proc "cdecl"(date_time: ^Date_Time) -> Date_Time {
    context = default_context()
    tz_offset := localtime_offset() * 1000 * 1000 * 1000 // go from sec to nsec
    time_in, in_ok := date_time_to_odin_time(date_time^)
    assert(in_ok)
    return odin_time_to_date_time(time.Time { time_in._nsec + tz_offset })    
}

System_Universal_Date_Time_From_Local_Sig :: #type proc "cdecl" (
    date_time: ^Date_Time
    ) -> Date_Time
system_universal_date_time_from_local :: proc "cdecl"(date_time: ^Date_Time) -> Date_Time {
    context = default_context()
    tz_offset := localtime_offset() * 1000 * 1000 * 1000
    time_in, in_ok := date_time_to_odin_time(date_time^)
    assert(in_ok)
    return odin_time_to_date_time(time.Time { time_in._nsec - tz_offset })
}

// TODO: implement timers, OR implement something equivalent that solves the same problem
timer_mock_fd_counter: i32 = 0
timer_mock_fd_counter_mtx: sync.Mutex

// I may have to just keep a descriptor set? I dunno if actually implementing these is even important
System_Wake_Up_Timer_Create_Sig :: #type proc "cdecl" () -> System_Timer
system_wake_up_timer_create :: proc "cdecl" () -> (ret: System_Timer) {
    // TODO: implement this if I'm implementing timers
    context = default_context()
    if sync.mutex_guard(&timer_mock_fd_counter_mtx) {
        timer_mock_fd_counter += 1
        //ret.fd = timer_mock_fd_counter
    }
    
    log_os("Created mock timer object:", ret.fd)
    return        
}

System_Wake_Up_Timer_Release_Sig :: #type proc "cdecl" (handle: System_Timer)
system_wake_up_timer_release :: proc "cdecl" (handle: System_Timer) {
    // TODO: implement this if I'm implementing timers
    context = default_context()
    log_os("Released mock timer object:", handle.fd)
}

System_Wake_Up_Timer_Set_Sig :: #type proc "cdecl" (
    handle: System_Timer,
    time_milliseconds: u32
    )
system_wake_up_timer_set :: proc "cdecl" (
    handle: System_Timer,
    time_milliseconds: u32
    )
{
    // TODO: implement this if I'm implementing timers
    context = default_context()
    log_os(
        "Setting wake up timer on mock timer object",
        handle.fd,
        "to",
        time_milliseconds,
        "milliseconds"
        )
}

System_Signal_Step_Sig :: #type proc "cdecl" (code: u32)
system_signal_step :: proc "cdecl" (code: u32)
{
    context = default_context()
    log_os("Timer update step signalled with code", code)
    // TODO: implement this if I'm implementing timers
    /*
        u64 now  = system_now_time();
        u64 diff = (now - system_globals.last_step_time);
    
        struct itimerspec its = {};
        timerfd_gettime(system_globals.step_timer_fd, &its);
    
        if (diff > frame_useconds) {
            its.it_value.tv_nsec = 1;
            timerfd_settime(system_globals.step_timer_fd, 0, &its, NULL);
        } else {
            if (its.it_value.tv_sec == 0 && its.it_value.tv_nsec == 0){
                its.it_value.tv_nsec = (frame_useconds - diff) * 1000UL;
                timerfd_settime(system_globals.step_timer_fd, 0, &its, NULL);
            }
        }
    */
}

System_Sleep_Sig :: #type proc "cdecl" (microseconds: u64)
system_sleep :: proc "cdecl" (microseconds: u64)
{
    context = default_context()
    time.sleep(time.Duration(i64(microseconds) * 1000))
    // NOTE: time.accurate_sleep() also exists, but I likely don't need that
}

// FIXME: these can fail, signature should reflect that
// NOTE (sio): purpose of index arg is unknown
System_Get_Clipboard_Sig :: #type proc "cdecl" (
    arena: ^Arena,
    index: i32
    ) -> String_Const_u8
system_get_clipboard :: proc "cdecl" (arena: ^Arena, index: i32) -> String_Const_u8 {
    context = default_context()
    context.allocator = odin_alloc_from_arena(arena)
    return String_Const_u8(strings.clone_from_cstring(sdl2.GetClipboardText()))
}

// NOTE (sio): purpose of index arg is unknown
System_Post_Clipboard_Sig :: #type proc "cdecl" (str: String_Const_u8, index: i32)
system_post_clipboard :: proc "cdecl" (str: String_Const_u8, index: i32) {
    context = default_context()
    context.allocator = context.temp_allocator
        
    save_point := default_temp_allocator_temp_begin()
    defer default_temp_allocator_temp_end(save_point)
        
    // NOTE: this works because SDL_SetClipboardText creates a copy of whatever it's handed
    sdl2.SetClipboardText(strings.clone_to_cstring(string(str)))
}

System_Set_Clipboard_Catch_All_Sig :: #type proc "cdecl" (enabled: b32)
system_set_clipboard_catch_all :: proc "cdecl" (enabled: b32) {
    context = default_context()
    log_os("unimplemented: tried to enable clipboard catch all")
}

System_Get_Clipboard_Catch_All_Sig :: #type proc "cdecl" () -> b32
system_get_clipboard_catch_all :: proc "cdecl" () -> b32 {
    context = default_context()
    log_os("unimplemented: tried to disable clipboard catch all")
    return true
}

// NOTE (sio): for the system_cli_* family of functions, please refer to platform_{linux, windows,freebsd}.odin

System_Open_Color_Picker_Sig :: #type proc "cdecl" (color_picker: ^Color_Picker)
system_open_color_picker :: proc "cdecl" (color_picker: ^Color_Picker) {
    // NOTE (sio): this should probably do something (iff it is ever even used, which
    // I strongly suspect it isn't, seeing as the original platform layer does not
    // implement it, either)
}

System_Get_Screen_Scale_Factor_Sig :: #type proc "cdecl" () -> f32
system_get_screen_scale_factor :: proc "cdecl" () -> f32 {
    // NOTE (sio): this should probably return a useful value. Not that it can do that
    // on X11 anyway?
    // TODO (sio): make this return a useful value on Wayland
    // NOTE (sio): the original implementation tries to make sense of the info it
    // gets from X11, but this is basically never set to anything useful
    return 1.0
}

System_Thread_Launch_Sig :: #type proc "cdecl" (
    procedure: Thread_Function,
    ptr: rawptr
    ) -> System_Thread
system_thread_launch :: proc "cdecl" (
    procedure: Thread_Function,
    ptr: rawptr
    ) -> (thrd: System_Thread)
{
    context = default_context()
    
    // NOTE (sio): the following combination is necessary to avoid potential issues with
    // an Odin context being passed as an extra argument to a cdecl function   
    thread_init_fn :: proc(argp: rawptr) {
        args := transmute(^Thread_Start_Args) argp
        args.procedure(args.arg)
    }
    
    args, err := new(Thread_Start_Args, allocator = rt.default_allocator())
    assert(err == .None)
    args.procedure = procedure
    args.arg = ptr
    
    thrd.args = args
    thrd.thrd = thread.create_and_start_with_data(transmute(rawptr) args, thread_init_fn)
        
    return
}

System_Thread_Join_Sig :: #type proc "cdecl" (thrd: System_Thread)
system_thread_join :: proc "cdecl" (thrd: System_Thread)
{
    context = default_context()
    thread.join(thrd.thrd)
    free(thrd.args, allocator = rt.default_allocator())
}

System_Thread_Free_Sig :: #type proc "cdecl" (thread: System_Thread)
system_thread_free :: proc "cdecl" (thrd: System_Thread)
{
    context = default_context()
    if !thread.is_done(thrd.thrd) {
        thread.terminate(thrd.thrd, 1)
    }
    thread.destroy(thrd.thrd)
}

System_Thread_Get_ID_Sig :: #type proc "cdecl" () -> i32
system_thread_get_id :: proc "cdecl" () -> i32 {
    return i32(sync.current_thread_id())
}

System_Acquire_Global_Frame_Mutex_Sig :: #type proc "cdecl" (tctx: ^Thread_Context)
system_acquire_global_frame_mutex :: proc "cdecl" (tctx: ^Thread_Context) {
    context = default_context()
    if tctx.kind == .AsyncTasks || tctx.kind == .Main {
        sync.mutex_lock(&system_globals.global_frame_mutex)
    }
}

System_Release_Global_Frame_Mutex_Sig :: #type proc "cdecl" (tctx: ^Thread_Context)
system_release_global_frame_mutex :: proc "cdecl" (tctx: ^Thread_Context) {
    context = default_context()
    if tctx.kind == .AsyncTasks || tctx.kind == .Main {
        sync.mutex_unlock(&system_globals.global_frame_mutex)
    }
}

System_Mutex_Make_Sig :: #type proc "cdecl" () -> System_Mutex
system_mutex_make :: proc "cdecl" () -> (mutex: System_Mutex) {
    context = default_context()
    mutex = mutex_alloc()
    return
}

System_Mutex_Free_Sig :: #type proc "cdecl" (mutex: System_Mutex)
system_mutex_free :: proc "cdecl" (mutex: System_Mutex) {
    context = default_context()
    if !sync.mutex_try_lock(mutex.mutex) {
        log_os("Freeing locked mutex")
    }
    mutex_free(mutex)
}

System_Mutex_Acquire_Sig :: #type proc "cdecl" (mutex: System_Mutex)
system_mutex_acquire :: proc "cdecl" (mutex: System_Mutex) {
    context = default_context()
    sync.mutex_lock(mutex.mutex)
}

System_Mutex_Release_Sig :: #type proc "cdecl" (mutex: System_Mutex)
system_mutex_release :: proc "cdecl" (mutex: System_Mutex) {
    context = default_context()
    sync.mutex_unlock(mutex.mutex)
}

System_Condition_Variable_Make_Sig :: #type proc "cdecl" () -> System_Condition_Variable
system_condition_variable_make :: proc "cdecl" () -> (cv: System_Condition_Variable) {
    context = default_context()
    cv = cond_alloc()
    return
}

System_Condition_Variable_Free_Sig :: #type proc "cdecl" (cv: System_Condition_Variable)
system_condition_variable_free :: proc "cdecl" (cv: System_Condition_Variable) {
    context = default_context()
    cond_free(cv)
}

System_Condition_Variable_Wait_Sig :: #type proc "cdecl" (
    cv: System_Condition_Variable,
    mutex: System_Mutex
    )
system_condition_variable_wait :: proc "cdecl" (
    cv: System_Condition_Variable,
    mutex: System_Mutex
    )
{
    context = default_context()
    sync.cond_wait(cv.cond, mutex.mutex)
}

System_Condition_Variable_Signal_Sig :: #type proc "cdecl" (cv: System_Condition_Variable)
system_condition_variable_signal :: proc "cdecl" (cv: System_Condition_Variable) {
    context = default_context()
    sync.cond_signal(cv.cond)
}

System_Show_Mouse_Cursor_Sig :: #type proc "cdecl"(show: i32)
system_show_mouse_cursor :: proc "cdecl" (show: i32) {
    context = default_context()
        
    // NOTE (sio): it doesn't appear this routine ever gets called properly,
    // so I figure let's just print what's going on and figure things out from there
    log_os(#procedure, ": show =", show)
}

System_Set_Fullscreen_Sig :: #type proc "cdecl"(full_screen: b32) -> b32
system_set_fullscreen :: proc "cdecl" (full_screen: b32) -> b32 {
    // TODO (sio): I just mostly don't care about this, so just pretend for now
    return b32false
}

System_Is_Fullscreen_Sig :: #type proc "cdecl"() -> b32
system_is_fullscreen :: proc "cdecl" () -> b32 {
    // TODO (sio): I just mostly don't care about this, so just pretend for now
    return b32false
}

System_Get_Keyboard_Modifiers_Sig :: #type proc "cdecl"(
    arena: ^Arena
    ) -> Input_Modifier_Set
system_get_keyboard_modifiers :: proc "cdecl" (arena: ^Arena) -> (ims: Input_Modifier_Set) {
    context = default_context()
    imsf := &system_globals.input.trans.modifier_set
    if imsf.count > 0 {
        rawimsf := Raw_Slice {
            data = transmute(rawptr) &imsf.mods,
            len = int(imsf.count)
        }
        mods, err :=
            slice.clone(
                transmute([]Key_Code) rawimsf,
                allocator = odin_alloc_from_arena(arena)
                )
        log_if(
            rt.Allocator_Error.None,
            err,
            "failed to alloc input modifier set using arena"
            )
        ims.mods = raw_data(mods)
        ims.count = imsf.count
    }
    return
}

System_Set_Key_Mode_Sig :: #type proc "cdecl" (mode: Key_Mode)
system_set_key_mode :: proc "cdecl" (mode: Key_Mode) {
    system_globals.key_mode = mode
}

System_Set_Source_Mixer_Sig :: #type proc "cdecl"(_: rawptr, _: Void_Func)
system_set_source_mixer :: proc "cdecl" (_: rawptr, _: Void_Func) {}

System_Set_Destination_Mixer_Sig :: #type proc "cdecl"(_: Void_Func)
system_set_destination_mixer :: proc "cdecl" (_: Void_Func) {}

API_VTable_system :: struct {
    error_box:                      System_Error_Box_Sig,
    get_path:                       System_Get_Path_Sig,
    get_canonical:                  System_Get_Canonical_Sig,
    get_file_list:                  System_Get_File_List_Sig,
    quick_file_attributes:          System_Quick_File_Attributes_Sig,
    load_handle:                    System_Load_Handle_Sig,
    load_attributes:                System_Load_Attributes_Sig,
    load_file:                      System_Load_File_Sig,
    load_close:                     System_Load_Close_Sig,
    save_file:                      System_Save_File_Sig,
    load_library:                   System_Load_Library_Sig,
    release_library:                System_Release_Library_Sig,
    get_proc:                       System_Get_Proc_Sig,
    now_time:                       System_Now_Time_Sig,
    now_date_time_universal:        System_Now_Date_Time_Universal_Sig,
    local_date_time_from_universal: System_Local_Date_Time_From_Universal_Sig,
    universal_date_time_from_local: System_Universal_Date_Time_From_Local_Sig,
    wale_up_timer_create:           System_Wake_Up_Timer_Create_Sig,
    wake_up_timer_release:          System_Wake_Up_Timer_Release_Sig,
    wake_up_timer_set:              System_Wake_Up_Timer_Set_Sig,
    signal_step:                    System_Signal_Step_Sig,
    sleep:                          System_Sleep_Sig,
    get_clipboard:                  System_Get_Clipboard_Sig,
    post_clipboard:                 System_Post_Clipboard_Sig,
    set_clipboard_catch_all:        System_Set_Clipboard_Catch_All_Sig,
    get_clipboard_catch_all:        System_Get_Clipboard_Catch_All_Sig,
    cli_call:                       System_CLI_Call_Sig,
    cli_begin_update:               System_CLI_Begin_Update_Sig,
    cli_update_step:                System_CLI_Update_Step_Sig,
    cli_end_update:                 System_CLI_End_Update_Sig,
    open_color_picker:              System_Open_Color_Picker_Sig,
    get_screen_scale_factor:        System_Get_Screen_Scale_Factor_Sig,
    thread_launch:                  System_Thread_Launch_Sig,
    thread_join:                    System_Thread_Join_Sig,
    thread_free:                    System_Thread_Free_Sig,
    thread_get_id:                  System_Thread_Get_ID_Sig,
    acquire_global_frame_mutex:     System_Acquire_Global_Frame_Mutex_Sig,
    release_global_frame_mutex:     System_Release_Global_Frame_Mutex_Sig,
    mutex_make:                     System_Mutex_Make_Sig,
    mutex_acquire:                  System_Mutex_Acquire_Sig,
    mutex_release:                  System_Mutex_Release_Sig,
    mutex_free:                     System_Mutex_Free_Sig,
    condition_variable_make:        System_Condition_Variable_Make_Sig,
    contition_variable_wait:        System_Condition_Variable_Wait_Sig,
    condition_variable_signal:      System_Condition_Variable_Signal_Sig,
    condition_variable_free:        System_Condition_Variable_Free_Sig,
    memory_allocate:                System_Memory_Allocate_Sig,
    memory_set_protection:          System_Memory_Set_Protection_Sig,
    memory_free:                    System_Memory_Free_Sig,
    memory_annotation:              System_Memory_Annotation_Sig,
    show_mouse_cursor:              System_Show_Mouse_Cursor_Sig,
    set_fullscreen:                 System_Set_Fullscreen_Sig,
    is_fullscreen:                  System_Is_Fullscreen_Sig,
    get_keyboard_modifiers:         System_Get_Keyboard_Modifiers_Sig,
    set_key_mode:                   System_Set_Key_Mode_Sig,
    set_source_mixer:               System_Set_Source_Mixer_Sig,
    set_destination_mixer:          System_Set_Destination_Mixer_Sig,
}

system_api_fill_vtable :: proc "cdecl" (vtbl: ^API_VTable_system) {
    using vtbl
    error_box =                      system_error_box
    get_path =                       system_get_path
    get_canonical =                  system_get_canonical
    get_file_list =                  system_get_file_list
    quick_file_attributes =          system_quick_file_attributes
    load_handle =                    system_load_handle
    load_attributes =                system_load_attributes
    load_file =                      system_load_file
    load_close =                     system_load_close
    save_file =                      system_save_file
    load_library =                   system_load_library
    release_library =                system_release_library
    get_proc =                       system_get_proc
    now_time =                       system_now_time
    now_date_time_universal =        system_now_date_time_universal
    local_date_time_from_universal = system_local_date_time_from_universal
    universal_date_time_from_local = system_universal_date_time_from_local
    wale_up_timer_create =           system_wake_up_timer_create
    wake_up_timer_release =          system_wake_up_timer_release
    wake_up_timer_set =              system_wake_up_timer_set
    signal_step =                    system_signal_step
    sleep =                          system_sleep
    get_clipboard =                  system_get_clipboard
    post_clipboard =                 system_post_clipboard
    set_clipboard_catch_all =        system_set_clipboard_catch_all
    get_clipboard_catch_all =        system_get_clipboard_catch_all
    cli_call =                       system_cli_call
    cli_begin_update =               system_cli_begin_update
    cli_update_step =                system_cli_update_step
    cli_end_update =                 system_cli_end_update
    open_color_picker =              system_open_color_picker
    get_screen_scale_factor =        system_get_screen_scale_factor
    thread_launch =                  system_thread_launch
    thread_join =                    system_thread_join
    thread_free =                    system_thread_free
    thread_get_id =                  system_thread_get_id
    acquire_global_frame_mutex =     system_acquire_global_frame_mutex
    release_global_frame_mutex =     system_release_global_frame_mutex
    mutex_make =                     system_mutex_make
    mutex_acquire =                  system_mutex_acquire
    mutex_release =                  system_mutex_release
    mutex_free =                     system_mutex_free
    condition_variable_make =        system_condition_variable_make
    contition_variable_wait =        system_condition_variable_wait
    condition_variable_signal =      system_condition_variable_signal
    condition_variable_free =        system_condition_variable_free
    memory_allocate =                system_memory_allocate
    memory_set_protection =          system_memory_set_protection
    memory_free =                    system_memory_free
    memory_annotation =              system_memory_annotation
    show_mouse_cursor =              system_show_mouse_cursor
    set_fullscreen =                 system_set_fullscreen
    is_fullscreen =                  system_is_fullscreen
    get_keyboard_modifiers =         system_get_keyboard_modifiers
    set_key_mode =                   system_set_key_mode
    set_source_mixer =               system_set_source_mixer
    set_destination_mixer =          system_set_destination_mixer
}
