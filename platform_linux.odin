//+build linux
package main

import "core:dynlib"
import "core:fmt"
import "core:mem"
import "core:mem/virtual"
import "core:os"
import rt "core:runtime"
import "core:time"
import "core:sys/linux"
import "core:sys/unix"

foreign import syslibc "system:c"

Process_ID :: linux.Pid

// TODO (sio): fix this once I've fixed the virtual memory allocation stuff
DEFAULT_PAGE_SIZE :: 64
//DEFAULT_PAGE_SIZE :: virtual.DEFAULT_PAGE_SIZE

virtual_alloc :: proc "contextless" (
    size: u64
    // TODO: consider allowing allocating not just read-write
    ) -> (mem: []u8, err: rt.Allocator_Error) 
{
    context = default_context()
    context.allocator = rt.default_allocator()
    // TODO: use virtual.reserve(uint(size))
    // TODO: use madvise(2) to turn on kernel same page merging, then zero out memory
    // idea here is to preserve the semantics that we have for virtual_alloc while
    // ensuring we always return zeroed memory
    return rt.mem_alloc(int(size), alignment = 64, allocator = rt.default_allocator())
    
}

// TODO: way to change memory protection that works

virtual_free :: proc "contextless" (addr: rawptr, count: u64) {
    context = default_context()
    context.allocator = rt.default_allocator()
    // TODO: virtual.release(addr, uint(count))
    rt.mem_free(addr)
}

time_now_utc :: proc()

make_target_so_name :: proc(version: u64) -> string {
    return fmt.tprintf("/tmp/game_%d.so", version)
}

kill :: proc "contextless" (pid: linux.Pid, signal: linux.Signal) -> int {
    return linux.syscall(linux.SYS_kill, pid, signal)
}

// so basically, applied to bit fields, these numbers just indicate which bit get set
// source: core:sys/linux/bits.odin (rev 92d3a68)
Wait_Flag :: enum {
    WNOHANG     = 0,
	WUNTRACED   = 1,
	WSTOPPED    = 1,
	WEXITED     = 2,
	WCONTINUED  = 3,
	WNOWAIT     = 24, 
	// // For processes created using clone
    __WNOTHREAD = 29,
    __WALL      = 30,
    __WCLONE    = 31,
}

Wait_Flags :: bit_set[Wait_Flag; u32]

bare_waitpid :: proc "contextless" (
                                    pid: linux.Pid,
                                    flags: Wait_Flags = nil
                                    ) -> (status: i32, pid_ret: int)
{
    pid_ret = linux.syscall(
                            linux.SYS_wait4,
                            /*linux.Pid(-1)*/ pid, // TODO (sio): figure out why this was -1 before
                            &status,
                            transmute(u32) flags,
                            rawptr(nil)
                            )
        return
}

waitpid :: proc(pid: linux.Pid) -> i32 {
    status, pid_ret := bare_waitpid(pid)
        fmt.println("Waiting on pid", pid, ", returned pid", pid_ret, "errno TODO, status", status)
        if status != 0 && pid_ret > 0 {
        fmt.eprintln("waitpid (wait4 syscall): got status", status, "returned pid", pid_ret, "for pid", pid)
    }
    return status
}

run_process :: proc(
                    name: string,
                    args: []string,
                    silent: bool = false,
                    loc := #caller_location
                    ) -> (status: u32)
{
    pid, err := linux.fork()
        if pid < 0 {
        log_os("Failed to fork:", err, "; run_process called from", loc)
            os.exit(1)
    } else if pid == 0 {
        // child, exec
        log_if(
               os.ERROR_NONE,
               os.close(os.stdin),
               "run_process: failed to close child process stdin",
               loc = loc
               )
            
            if silent {
            log_if(
                   os.ERROR_NONE,
                   os.close(os.stdout),
                   "failed to close child process stdout",
                   loc = loc
                   )
                log_if(
                       os.ERROR_NONE,
                       os.close(os.stderr),
                       "failed to close child process stderr",
                       loc = loc
                       )
        }
        
        err := os.execvp(name, args)
            if err != os.ERROR_NONE {
            log_os("Failed to run:", name, args,", got error:", err)
                os.exit(1)
        } else {
            os.exit(0)
        }
    } else {
        // parent, wait on child
        status = u32(waitpid(pid))
    }
    return
}


// overwrites target; I was gonna do this properly, but there's a bug in the syscall somewhere and that causes it to hang? I'm not sure what's going on, and this works fine for my purposes right now.
copy_file :: proc(target_name: string, source_name: string) -> bool {
    status := run_process("cp", {"-f", source_name, target_name})
        
        if status != 0 {
        fmt.eprintln("Failed to copy", source_name, "to", target_name, ". Got cp(1) exit status", status)
    }
    
    return status == 0
}

load_lib :: proc(name: string) -> (dynlib.Library, bool) {
    when #defined(dynlib.last_error) {
    lib, ok := dynlib.load_library(name)
        if !ok {
        fmt.eprintln("Failed to load dynlib", name, "error:", dynlib.last_error())
    }
    return lib, ok
} else {
    flags := os.RTLD_NOW // | os.RTLD_DEEPBIND
        lib := os.dlopen(name, flags)
        if lib == nil {
        fmt.eprintln("Failed to load dynlib", name, "error:", os.dlerror())
    }
    return dynlib.Library(lib), lib != nil
}
}

System_CLI_Call_Sig :: #type proc "cdecl" (
scratch: ^Arena,
path: cstring,
script: cstring,
cli_out: ^CLI_Handles
) -> b32
system_cli_call :: proc "cdecl" (
                                 scratch: ^Arena,
                                 path: cstring,
                                 script: cstring,
                                 cli_out: ^CLI_Handles
                                 ) -> b32
{
    context = default_context()
        
        Pipe_Fd :: enum u32 { READ, WRITE }
    pipes: [Pipe_Fd]os.Handle
        // NOTE (sio): a linux.Fd and an os.Handle must have the same size/alignment for this to work, compilation should fail otherwise
#assert(size_of(os.Handle) == size_of(linux.Fd) && align_of(os.Handle) == align_of(linux.Fd))
    err := linux.pipe2(transmute(^[2]linux.Fd) &pipes, linux.Open_Flags {})
        if err != .NONE {
        log_os(
               "Failed to create pipes for CLI call \"/bin/sh sh -c",
               script,
               "\" in dir",
               path,
               "; got errno",
               err
               )
            return b32false
    }
    
    // NOTE (sio): originally, vfork(2) was used, which is more efficient, but the syscall is wrapped differently in Odin, which inconveniences the implementation noticably
    // Should this turn out to be a performance issue of some sort, or noticeable,
    // I'd advise changing to vfork(2) instead. Otherwise, just leave as-is.
    pid: linux.Pid
        pid, err = linux.fork()
        if pid < 0 {
        log_os("Failed to fork(2), got errno:", err)
            return b32false
    } else if pid == 0 { // child, set up pipes and close stdin
        log_if(
               linux.Errno.NONE,
               linux.Errno(os.close(os.stdin)),
               "failed to close child process stdin"
               )
            
            log_if(
                   linux.Errno.NONE,
                   linux.Errno(os.close(pipes[.READ])),
                   "failed to close read end of child process pipe"
                   )
            
            ignore_result :: proc(_anfd: linux.Fd, err: linux.Errno) -> linux.Errno {
            return err
        }
        
        log_if(
               linux.Errno.NONE,
               ignore_result(linux.dup2(linux.Fd(pipes[.WRITE]), linux.Fd(os.stdout))),
               "failed to redirect stdout to calling process pipe"
               )
            
            log_if(
                   linux.Errno.NONE,
                   ignore_result(linux.dup2(linux.Fd(pipes[.WRITE]), linux.Fd(os.stderr))),
                   "failed to redirect stderr to calling process pipe"
                   )
            
            err := linux.chdir(path)
            if err != .NONE {
            log_os(
                   linux.Errno.NONE,
                   err,
                   "failed to chdir to",
                   path,
                   "to run script",
                   script,
                   "; got errno:",
                   err
                   )
                os.exit(1)
        }
        
        argv := [?]cstring { "sh", "-c", script, nil }
        
        foreign syslibc { environ: [^]cstring }
        
        err =
            linux.Errno(
                        -(linux.syscall(
                                        linux.SYS_execve,
                                        transmute(rawptr) cstring("/bin/sh"),
                                        transmute(rawptr) &argv,
                                        transmute(rawptr) environ
                                        )
                          ))
            if err != .NONE {
            log_os(
                   "Failed to exevp(2):",
                   script, 
                   ", in directory",
                   path,
                   ", got errno:",
                   err
                   )
                os.exit(1)
        }
        linux.exit(63) // NOTE (sio): should be unreachable, emit different exit status
    }
    
    // parent
    log_if(
           linux.Errno.NONE,
           linux.Errno(os.close(pipes[.WRITE])),
           "failed to close write end of pipe on parent process side"
           )
        
        cli_out.process.pid = pid
        cli_out.out_read.handle = pipes[.READ]
        cli_out.out_write.handle = pipes[.WRITE]
        
        // TODO (sio): epoll(2) with EPOLLIN | EPOLLET on read pipe?
    
        return b32true
}

System_CLI_Begin_Update_Sig :: #type proc "cdecl" (cli: ^CLI_Handles)
system_cli_begin_update :: proc "cdecl" (cli: ^CLI_Handles) {
    // NOTE (sio): no-op
}

System_CLI_Update_Step_Sig :: #type proc "cdecl" (
cli: ^CLI_Handles,
dest: cstring,
max: u32,
amount: ^u32
) -> b32
system_cli_update_step :: proc "cdecl" (
                                        cli: ^CLI_Handles,
                                        dest: cstring,
                                        max: u32,
                                        amount: ^u32
                                        ) -> b32
{
    context = default_context()
        // NOTE (sio): the original implementation uses select(2) to poll for available bytes to read in the pipe, and then does a blocking read of up to buffer_size
        // we poll(2) instead, because select(2) isn't implemented in Odin's stdlib, but poll is
    
        read_fd := linux.Fd(cli.out_read.handle)
        
        pollfds := [1]linux.Poll_Fd {
        linux.Poll_Fd {
            fd = read_fd,
            events = { .IN, .ERR, .NVAL, .PRI },
            revents = nil
        }
    }
    
    tv: linux.Time_Spec = { 0, 0 }
    
    
    space_left := int(max)
        fill: [^]u8 = transmute([^]u8) dest
        
        for space_left > 0 && linux.ppoll(pollfds[:], &tv, nil) == .NONE {
        rslice: Raw_Slice = { data = rawptr(fill), len = int(space_left) }
        num, err := linux.read(read_fd, transmute([]u8) rslice)
            if num < 0 {
            ;           log_os("Failed to read from pipe; got errno", err)
                break
        } else if num == 0 {
            // EOF
            break
        } else {
            fill = &fill[num]
                space_left -= num
        }
        
        pollfds[0].revents = nil
    }
    
    count := mem.ptr_sub(transmute(^u8) fill, transmute(^u8) dest)
        amount^ = u32(count)
        return b32(count > 0)
}

System_CLI_End_Update_Sig :: #type proc "cdecl" (cli: ^CLI_Handles) -> b32
system_cli_end_update :: proc "cdecl" (cli: ^CLI_Handles) -> b32 {
    context = default_context();
    exited := false;
    
    if cli.process.pid > 0 {
        status, pid_ret := bare_waitpid(cli.process.pid, { .WNOHANG })
            // TODO (sio): handle pid_ret containing an errno value
            cli.exit = status
            
            exited = true
            
            log_if(linux.Errno.NONE, linux.Errno(os.close(cli.out_read.handle)))
            cli.out_read.handle = os.Handle(-1)
            log_if(linux.Errno.NONE, linux.Errno(os.close(cli.out_write.handle)))
            cli.out_write.handle = os.Handle(-1)
    }
    
    cli.process.pid = linux.Pid(-1); // NOTE (sio): this only works out if this is the only way we're going to deal with these pids
    
    return b32(exited)
}