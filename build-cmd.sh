#!/bin/sh

cd "$(dirname "$(realpath "$0")")"

./nix-shell-launcher.sh 'sh -c "./configure.sh && ./build.sh"'
