package main

import "core:mem"
import rt "core:runtime"

DEFAULT_SLAB_SIZE: u32 : 1 * rt.Megabyte

Slab_Freelist :: struct($T: typeid) {
    next: ^Slab_Freelist(T),
}

slab_elem_size :: proc "contextless" ($S: typeid/Slab($T, $SLAB_SIZE)) -> u32 {
    if size_of(T) >= size_of(Slab_Freelist(T)) {
        return size_of(T)
    } else {
        return size_of(Slab_Freelist(T))
    }
}

Slab :: struct($T: typeid, $SLAB_SIZE: u32 = DEFAULT_SLAB_SIZE) {
    mem: ^[SLAB_SIZE]u8, // from the virtual memory allocator, page aligned
    fill: u32, // in elements
    cap: u32, // in elements; NOTE: this exists as a convenience thing because we have the space to store it and this'd otherwise be dead space
    freelist: ^Slab_Freelist(T)
}

make_slab :: proc(
    $S: typeid/Slab($T, $SLAB_SIZE)
    ) -> (slab: S, err: rt.Allocator_Error)
{
    mem.zero(transmute(rawptr) &slab, size_of(Slab(T, SLAB_SIZE)))
    u8m: []u8
    u8m, err = virtual_alloc(u64(SLAB_SIZE))
    if err != .None {
        return
    }
    
    slab.cap = SLAB_SIZE / slab_elem_size(S)
    raw_mem := transmute(Raw_Slice) u8m
    
    slab.mem = transmute(^[SLAB_SIZE]u8) raw_mem.data
    return
}

free_slab :: proc(slab: ^$S/Slab($T, $SLAB_SIZE)) {
    virtual_free(transmute(rawptr) slab.mem, u64(SLAB_SIZE))
    mem.zero(slab, size_of(Slab(T, SLAB_SIZE)))
}

slab_alloc :: proc(slab: ^$S/Slab($T, $SLAB_SIZE)) -> (x: ^T, err: rt.Allocator_Error) {
    if (slab.fill >= slab.cap && slab.freelist == nil) || slab.mem == nil {
        err = .Out_Of_Memory
        return
    }
    
    if slab.freelist != nil {
        x = transmute(^T) slab.freelist
        slab.freelist = slab.freelist.next
    } else {
        x = transmute(^T) &(slab.mem[slab.fill * slab_elem_size(S)])
        slab.fill += 1
    }
    
    // zero out returned memory
    mem.zero(transmute(rawptr) x, size_of(T))
    
    return
}

slab_free :: proc(slab: ^$S/Slab($T, $SLAB_SIZE), x: ^T) -> rt.Allocator_Error {
    memstart := transmute(uintptr) slab.mem
    xstart := transmute(uintptr) x
    if xstart < memstart || xstart > (transmute(uintptr) &(slab.mem[slab.cap - 1])) {
        return .Invalid_Argument
    }
    
    // NOTE: I can't actually detect a double free reliably here I think
    
    new_freelist := transmute(^Slab_Freelist(T)) x
    new_freelist.next = slab.freelist
    slab.freelist = new_freelist
    
    // zero out thing, resetting it
    mem.zero(transmute(rawptr) x, size_of(T))
        
    return .None
}
