#!/usr/bin/env bash

SUPERFLAGS="-std=gnu++11 -O0 -g3 -fpermissive -Wno-write-strings -Wno-writable-strings -Wno-unused-result -D_GNU_SOURCE -fPIC -fno-threadsafe-statics -pthread -fwrapv -fno-strict-aliasing -fno-exceptions -fno-rtti -DOS_LINUX=1 -DOS_WINDOWS=0 -DOS_MAC=0 -m64 -DFTECH_64_BIT -Icustom -I. -Wno-null-dereference -Wno-comment -Wno-switch -Wno-missing-declarations -Wno-logical-op-parentheses -Wno-writable-strings -Wno-writable-strings"

set -e

ls -Rl custom | sort > build/buildsuper_tmp
if [ -e build/custom_sgedit.so ] && [ -e build/buildsuper_sav ]; then
	# if there are no changes, no rebuild is necessary
	if diff -Bq build/buildsuper_tmp build/buildsuper_sav; then
		echo "- have a built build/custom_sgedit.so"
		echo "- there have been no changes to custom/ or metadata since last build"
		echo "=> no rebuild necessary, skipping custom_sgedit.so"
		exit 0
	fi

	cp -f build/buildsuper_tmp build/buildsuper_sav
else
	cp -f build/buildsuper_tmp build/buildsuper_sav
fi

echo "generating metadata"
${CXX} -DMETA_PASS ${SUPERFLAGS} "custom/4coder_default_bindings.cpp" -E -o 4coder_command_metadata.i

echo "building metadata generator"
${CXX} ${SUPERFLAGS} "custom/4coder_metadata_generator.cpp" -o metadata_generator

echo "generating metadata"
./metadata_generator -R "$(pwd)" "$(pwd)/4coder_command_metadata.i"

echo "building custom"
${CXX} ${SUPERFLAGS} -shared "custom/4coder_default_bindings.cpp" -o build/custom_sgedit.so -fPIC
chmod +x build/custom_sgedit.so
rm -f metadata_generator
rm -f 4coder_command_metadata.i
