package main

import "core:mem"
import "core:mem/virtual"
import rt "core:runtime"

Custom_Global_Set_Setting_Sig :: #type proc "cdecl" (app: ^Application_Links, setting: Global_Setting_ID, value: i64) -> b32

Custom_Global_Get_Screen_Rectangle_Sig :: #type proc "cdecl" (app: ^Application_Links) -> Rect(f32)

Custom_Get_Thread_Context_Sig :: #type proc "cdecl" (app: ^Application_Links) -> ^Thread_Context

Custom_Create_Child_Process_Sig :: #type proc "cdecl" (app: ^Application_Links, path: String_Const_u8, command: String_Const_u8) -> Child_Process_ID

Custom_Child_Process_Set_Target_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, child_process_id: Child_Process_ID, buffer_id: Buffer_ID, flags: Child_Process_Set_Target_Flags) -> b32

Custom_Buffer_Get_Attached_Child_Process_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> Child_Process_ID

Custom_Child_Process_Get_Attached_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, child_process_id: Child_Process_ID) -> Buffer_ID

Custom_Child_Process_Get_State_Sig :: #type proc "cdecl" (app: ^Application_Links, child_process_id: Child_Process_ID) -> Process_State

Custom_Enqueue_Virtual_Event_Sig :: #type proc "cdecl" (app: ^Application_Links, event: ^Input_Event) -> b32

Custom_Get_Buffer_Count_Sig :: #type proc "cdecl" (app: ^Application_Links) -> i32

Custom_Get_Buffer_Next_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, access: Access_Flag) -> Buffer_ID

Custom_Get_Buffer_By_Name_Sig :: #type proc "cdecl" (app: ^Application_Links, name: String_Const_u8, access: Access_Flag) -> Buffer_ID

Custom_Get_Buffer_By_File_Name_Sig :: #type proc "cdecl" (app: ^Application_Links, file_name: String_Const_u8, access: Access_Flag) -> Buffer_ID

Custom_Buffer_Read_Range_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, range: Range(i64), out: [^]u8) -> b32

Custom_Buffer_Replace_Range_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, range: Range(i64), str: String_Const_u8) -> b32

Custom_Buffer_Batch_Edit_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, batch: ^Batch_Edit) -> b32

Custom_Buffer_Seek_String_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, needle: String_Const_u8, direction: Scan_Direction, start_pos: i64) -> String_Match

Custom_Buffer_Seek_Character_Class_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer: Buffer_ID, predicate: ^Character_Predicate, direction: Scan_Direction, start_pos: i64) -> String_Match

Custom_Buffer_Line_Y_Difference_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, line_a: i64, line_b: i64) -> f32

Custom_Buffer_Line_Shift_Y_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, line: i64, y_shift: f32) -> Line_Shift_Vertical

Custom_Buffer_Pos_At_Relative_XY_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, base_line: i64, relative_xy: [2]f32) -> i64

Custom_Buffer_Relative_Box_Of_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, base_line: i64, pos: i64) -> Rect(f32)

Custom_Buffer_Padded_Box_Of_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, base_line: i64, pos: i64) -> Rect(f32)

Custom_Buffer_Relative_Character_From_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, bae_line: i64, pos: i64) -> i64

Custom_Buffer_Pos_From_Relative_Character_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, width: f32, face_id: Face_ID, base_line: i64, relative_character: i64) -> i64

Custom_View_Line_Y_Difference_Sig :: #type proc "cdecl" (app:^Application_Links, view_id: View_ID, line_a: i64, line_b: i64) -> f32

Custom_View_Line_Shift_Y_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, line: i64, y_shift: f32) -> Line_Shift_Vertical

Custom_View_Pos_At_Relative_XY_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, base_line: i64, relative_xy: [2]f32) -> i64

Custom_View_Relative_Box_Of_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, base_line: i64, pos: i64) -> Rect(f32)

Custom_View_Padded_Box_Of_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, base_line: i64, pos: i64) -> Rect(f32)

Custom_View_Relative_Character_From_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, base_line: i64, pos: i64) -> i64

Custom_View_Pos_From_Relative_Character_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, base_line: i64, character: i64) -> i64

Custom_Buffer_Exists_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> b32

Custom_Buffer_Get_Access_Flags_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> Access_Flag

Custom_Buffer_Get_Size_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> i64

Custom_Buffer_Get_Line_Count_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> i64

Custom_Push_Buffer_Base_Name_Sig :: #type proc "cdecl" (app: ^Application_Links, arena: ^Arena, buffer_id: Buffer_ID) -> String_Const_u8

Custom_Push_Buffer_Unique_Name_Sig :: #type proc "cdecl" (app: ^Application_Links,out: ^Arena, buffer_id: Buffer_ID) -> String_Const_u8

Custom_Push_Buffer_File_Name_Sig :: #type proc "cdecl" (app: ^Application_Links, arena: ^Arena, buffer_id: Buffer_ID) -> String_Const_u8

Custom_Buffer_Get_Dirty_State_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> Dirty_State

Custom_Buffer_Set_Dirty_State_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, dirt_state: Dirty_State) -> b32

Custom_Buffer_Set_Layout_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, layout_func: Layout_Function) -> b32

Custom_Buffer_Clear_Layout_Cache_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> b32

Custom_Buffer_Get_Layout_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> Layout_Function

Custom_Buffer_Get_Setting_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, setting: Buffer_Setting_ID, value_out: ^i64) -> b32

Custom_Buffer_Set_Setting_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, setting: Buffer_Setting_ID, value: i64) -> b32

Custom_Buffer_Get_Managed_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> Managed_Scope

Custom_Buffer_Send_End_Signal_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> b32

Custom_Create_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, file_name: String_Const_u8, flags: Buffer_Create_Flag) -> Buffer_ID

Custom_Buffer_Save_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, file_name: String_Const_u8, flags: Buffer_Save_Flag) -> b32

Custom_Buffer_Kill_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, flags: Buffer_Kill_Flag) -> Buffer_Kill_Result

Custom_Buffer_Reopen_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, flags: Buffer_Reopen_Flag) -> Buffer_Reopen_Result

Custom_Buffer_Get_File_Attributes_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> File_Attributes

Custom_Get_View_Next_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, access: Access_Flag) -> View_ID

Custom_Get_View_Prev_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, access: Access_Flag) -> View_ID

Custom_Get_This_Ctx_View_Sig :: #type proc "cdecl" (app: ^Application_Links, access: Access_Flag) -> View_ID

Custom_Get_Active_View_Sig :: #type proc "cdecl" (app: ^Application_Links, access: Access_Flag) -> View_ID

Custom_View_Exists_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> b32

Custom_View_Get_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, access: Access_Flag) -> Buffer_ID

Custom_View_Get_Cursor_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> i64

Custom_View_Get_Mark_Pos_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> i64

Custom_View_Get_Preferred_X_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> f32

Custom_View_Set_Preferred_X_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, x: f32) -> b32

Custom_View_Get_Screen_Rect_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> Rect(f32)

Custom_View_Get_Panel_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> Panel_ID

Custom_Panel_Get_View_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID, access: Access_Flag) -> View_ID

Custom_Panel_Is_Split_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID) -> b32

Custom_Panel_Is_Leaf_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID) -> b32

Custom_Panel_Split_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID, split_dim: Dimension) -> b32

Custom_Panel_Set_Split_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID, kind: Panel_Split_Kind, t: f32) -> b32

Custom_Panel_Swap_Children_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID) -> b32

Custom_Panel_Get_Root_Sig :: #type proc "cdecl" (app: ^Application_Links) -> Panel_ID

Custom_Panel_Get_Parent_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID) -> Panel_ID

Custom_Panel_Get_Child_Sig :: #type proc "cdecl" (app: ^Application_Links, panel_id: Panel_ID, which_child: Side) -> Panel_ID

Custom_View_Close_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> b32

Custom_View_Get_Buffer_Region_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> Rect(f32)

Custom_View_Get_Buffer_Scroll_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> Buffer_Scroll

Custom_View_Set_Active_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> b32

Custom_View_Enqueue_Command_Function_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, custom_func: Custom_Command_Function) -> b32

Custom_View_Get_Setting_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, setting: View_Setting_ID, value_out: ^i64) -> b32

Custom_View_Set_Setting_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, setting: View_Setting_ID, value: i64) -> b32

Custom_View_Get_Managed_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> Managed_Scope

Custom_Buffer_Compute_Cursor_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer: Buffer_ID, seek: Buffer_Seek) -> Buffer_Cursor

Custom_View_Compute_Cursor_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, seek: Buffer_Seek) -> Buffer_Cursor

Custom_View_Set_Camera_Bounds_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, margin: [2]f32, push_in_multiplier: [2]f32) -> b32

Custom_View_Get_Camera_Bounds_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, margin: ^[2]f32, push_in_multiplier: ^[2]f32) -> b32

Custom_View_Set_Cursor_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, seek: Buffer_Seek) -> b32

Custom_View_Set_Buffer_Scroll_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, scroll: Buffer_Scroll, set_rule: Buffer_Scroll_Rule) -> b32

Custom_View_Set_Mark_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, seek: Buffer_Seek) -> b32

Custom_View_Quit_UI_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> b32

Custom_View_Set_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, buffer_id: Buffer_ID, flag: Set_Buffer_Flag) -> b32

Custom_View_Push_Context_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, ctx: ^View_Context) -> b32

Custom_View_Pop_Context_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> b32

Custom_View_Alter_Context_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, ctx: ^View_Context) -> b32

Custom_View_Current_Context_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID) -> View_Context

Custom_View_Current_Context_Hook_Memory_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, hook_id: Hook_ID) -> String_Const_u8

Custom_Create_User_Managed_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links) -> Managed_Scope

Custom_Destroy_User_Managed_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope) -> b32

Custom_Get_Global_Managed_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links) -> Managed_Scope

Custom_Get_Managed_Scope_With_Multiple_Dependencies_Sig :: #type proc "cdecl" (app: ^Application_Links, scopes: [^]Managed_Scope, count: i32) -> Managed_Scope

Custom_Managed_Scope_Clear_Contents_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope) -> b32

Custom_Managed_Scope_Clear_Self_All_Dependent_Scopes_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope) -> b32

Custom_Managed_Scope_Allocator_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope) -> ^Base_Allocator

Custom_Managed_ID_Group_Highest_ID_Sig :: #type proc "cdecl" (app: ^Application_Links, group: String_Const_u8) -> u64

Custom_Managed_ID_Declare_Sig :: #type proc "cdecl" (app: ^Application_Links, group: String_Const_u8, name: String_Const_u8) -> Managed_ID

Custom_Managed_ID_Get_Sig :: #type proc "cdecl" (app: ^Application_Links, group: String_Const_u8, name: String_Const_u8) -> Managed_ID

Custom_Managed_Scope_Get_Attachment_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope, id: Managed_ID, size: u64) -> rawptr

Custom_Managed_Scope_Attachment_Erase_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope, id: Managed_ID) -> b32

Custom_Alloc_Managed_Memory_In_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links, scope: Managed_Scope, item_size: i32, count: i32) -> Managed_Object

Custom_Alloc_Buffer_Markers_On_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, count: i32, optional_extra_scope: [^]Managed_Scope) -> Managed_Object

Custom_Managed_Object_Get_Item_Size_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object) -> u32

Custom_Managed_Object_Get_Item_Count_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object) -> u32

Custom_Managed_Object_Get_Pointer_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object) -> rawptr

Custom_Managed_Object_Get_Type_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object) -> Managed_Object_Type

Custom_Managed_Object_Get_Containing_Scope_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object) -> Managed_Scope

Custom_Managed_Object_Free_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object) -> b32

Custom_Managed_Object_Store_Data_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object, first_index: u32, count: u32, mem: rawptr) -> b32

Custom_Managed_Object_Load_Data_Sig :: #type proc "cdecl" (app: ^Application_Links, object: Managed_Object, first_index: u32, count: u32, mem_out: rawptr) -> b32

Custom_Get_Next_Input_Raw_Sig :: #type proc "cdecl" (app: ^Application_Links) -> User_Input

Custom_Get_Current_Input_Sequence_Number_Sig :: #type proc "cdecl" (app: ^Application_Links) -> i64

Custom_Get_Current_Input_Sig :: #type proc "cdecl" (app: ^Application_Links) -> User_Input

Custom_Set_Current_Input_Sig :: #type proc "cdecl" (app: ^Application_Links, input: ^User_Input)

Custom_Leave_Current_Input_Unhandled_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Set_Custom_Hook_Sig :: #type proc "cdecl" (app: ^Application_Links, hook_id: Hook_ID, func_ptr: Void_Func)

Custom_Get_Custom_Hook_Sig :: #type proc "cdecl" (app: ^Application_Links, hook_id: Hook_ID) -> Void_Func

Custom_Set_Custom_Hook_Memory_Size_Sig :: #type proc "cdecl" (app: ^Application_Links, hook_id: Hook_ID, size: u64) -> b32

Custom_Get_Mouse_State_Sig :: #type proc "cdecl" (app: ^Application_Links) -> Mouse_State

Custom_Get_Active_Query_Bars_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID, max_result_count: i32, array_out: [^]Query_Bar_Ptr_Array) -> b32

Custom_Start_Query_Bar_Sig :: #type proc "cdecl" (app: ^Application_Links, bar: ^Query_Bar, flags: u32) -> b32

Custom_End_Query_Bar_Sig :: #type proc "cdecl" (app: ^Application_Links, bar: ^Query_Bar, flags: u32)

Custom_Clear_All_Query_Bars_Sig :: #type proc "cdecl" (app: ^Application_Links, view_id: View_ID)

Custom_Print_Message_Sig :: #type proc "cdecl" (app: ^Application_Links, message: String_Const_u8)

Custom_Log_String_Sig :: #type proc "cdecl" (app: ^Application_Links, str: String_Const_u8) -> b32

Custom_Get_Largest_Face_ID_Sig :: #type proc "cdecl" (app: ^Application_Links) -> Face_ID

Custom_Set_Global_Face_Sig :: #type proc "cdecl" (app: ^Application_Links, id: Face_ID) -> b32

Custom_Buffer_History_Get_Max_Record_Index_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> History_Record_Index

Custom_Buffer_History_Get_Record_Info_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, index: History_Record_Index) -> Record_Info

Custom_Buffer_History_Get_Group_Sub_Record_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, index: History_Record_Index, sub_index: i32) -> Record_Info

Custom_Buffer_History_Get_Current_State_Index_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> History_Record_Index

Custom_Buffer_History_Set_Current_State_Index_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, index: History_Record_Index) -> b32

Custom_Buffer_History_Merge_Record_Range_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, first_index: History_Record_Index, last_index: History_Record_Index , flags: Record_Merge_Flag) -> b32

Custom_Buffer_History_Clear_After_Current_State_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> b32

Custom_Global_History_Edit_Group_Begin_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Global_History_Edit_Group_End_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Buffer_Set_Face_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, id: Face_ID) -> b32

Custom_Get_Face_Description_Sig :: #type proc "cdecl" (app: ^Application_Links, face_id: Face_ID) -> Face_Description

Custom_Get_Face_Metrics_Sig :: #type proc "cdecl" (app: ^Application_Links, face_id: Face_ID) -> Face_Metrics

Custom_Get_Face_Advance_Map_Sig :: #type proc "cdecl" (app: ^Application_Links, face_id: Face_ID) -> Face_Advance_Map

Custom_Get_Face_ID_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID) -> Face_ID

Custom_Try_Create_New_Face_Sig :: #type proc "cdecl" (app: ^Application_Links, description: ^Face_Description) -> Face_ID

Custom_Try_Modify_Face_Sig :: #type proc "cdecl" (app: ^Application_Links, id: Face_ID, description: ^Face_Description) -> b32

Custom_Try_Release_Face_Sig :: #type proc "cdecl" (app: ^Application_Links, id: Face_ID, replacement_id: Face_ID) -> b32

Custom_Push_Hot_Directory_Sig :: #type proc "cdecl" (app: ^Application_Links, arena: ^Arena) -> String_Const_u8

Custom_Set_Hot_Directory_Sig :: #type proc "cdecl" (app: ^Application_Links, str: String_Const_u8)

Custom_Send_Exit_Signal_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Hard_Exit_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Set_Window_Title_Sig :: #type proc "cdecl" (app: ^Application_Links, title: String_Const_u8)

Custom_Acquire_Global_Frame_Mutex_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Release_Global_Frame_Mutex_Sig :: #type proc "cdecl" (app: ^Application_Links)

Custom_Draw_String_Oriented_Sig :: #type proc "cdecl" (app: ^Application_Links, font_id: Face_ID, color: ARGB_Color, str: String_Const_u8, point: [2]f32, flags: u32, delta: [2]f32) -> [2]f32

Custom_Get_String_Advance_Sig :: #type proc "cdecl" (app: ^Application_Links, font_id: Face_ID, str: String_Const_u8) -> f32

Custom_Draw_Rectangle_Sig :: #type proc "cdecl" (app: ^Application_Links, rect: Rect(f32), roundness: f32, color: ARGB_Color)

Custom_Draw_Rectangle_Outline_Sig :: #type proc "cdecl" (app: ^Application_Links, rect: Rect(f32), roundness: f32, thickness: f32, color: ARGB_Color)

Custom_Draw_Set_Clip_Sig :: #type proc "cdecl" (app: ^Application_Links, new_clip: Rect(f32)) -> Rect(f32)

Custom_Text_Layout_Create_Sig :: #type proc "cdecl" (app: ^Application_Links, buffer_id: Buffer_ID, rect: Rect(f32), buffer_point: Buffer_Point) -> Text_Layout_ID

Custom_Text_Layout_Region_Sig :: #type proc "cdecl" (app: ^Application_Links, text_layout_id: Text_Layout_ID) -> Rect(f32)

Custom_Text_Layout_Get_Buffer_Sig :: #type proc "cdecl" (app: ^Application_Links, text_layout_id: Text_Layout_ID) -> Buffer_ID

Custom_Text_Layout_Get_Visible_Range_Sig :: #type proc "cdecl" (app: ^Application_Links, text_layout_id: Text_Layout_ID) -> Range(i64)

Custom_Text_Layout_Line_On_Screen_Sig :: #type proc "cdecl" (app: ^Application_Links, layout_id: Text_Layout_ID, line_number: i64) -> Range(f32)

Custom_Text_Layout_Character_On_Screen_Sig :: #type proc "cdecl" (app: ^Application_Links, layout_id: Text_Layout_ID, pos: i64) -> Rect(f32)

Custom_Paint_Text_Color_Sig :: #type proc "cdecl" (app: ^Application_Links, layout_id: Text_Layout_ID, range: Range(i64), color: ARGB_Color)

Custom_Paint_Text_Color_Blend_Sig :: #type proc "cdecl" (app: ^Application_Links, layout_id: Text_Layout_ID, range: Range(i64), color: ARGB_Color, blend: f32)

Custom_Text_Layout_Free_Sig :: #type proc "cdecl" (app: ^Application_Links, text_layout_id: Text_Layout_ID) -> b32

Custom_Draw_Text_Layout_Sig :: #type proc "cdecl" (app: ^Application_Links, layout_id: Text_Layout_ID, special_color: ARGB_Color, ghost_color: ARGB_Color)

Custom_Open_Color_Picker_Sig :: #type proc "cdecl" (app: ^Application_Links, picker: ^Color_Picker)

Custom_Animate_In_N_Milliseconds_Sig :: #type proc "cdecl" (app: ^Application_Links, n: u32)

Custom_Buffer_Find_All_Matches_Sig :: #type proc "cdecl" (app: ^Application_Links, arena: ^Arena, buffer: Buffer_ID, string_id: i32, range: Range(i64), needle: String_Const_u8, predicate: ^Character_Predicate, direction: Scan_Direction) -> String_Match_List

Custom_Get_Core_Profile_List_Sig :: #type proc "cdecl" (app: ^Application_Links) -> rawptr
// NOTE (sio): actually returns a ^Profile_Global_List

Custom_Get_Custom_Layer_Boundary_Docs_Sig :: #type proc "cdecl" (app: ^Application_Links, arena: ^Arena) -> rawptr
// NOTE (sio): actually returns a ^Doc_Cluster

API_VTable_custom :: struct {
    global_set_setting: Custom_Global_Set_Setting_Sig,
    global_get_screen_rectangle: Custom_Global_Get_Screen_Rectangle_Sig,
    get_thread_context: Custom_Get_Thread_Context_Sig,
    create_child_process: Custom_Create_Child_Process_Sig,
    child_process_set_target_buffer: Custom_Child_Process_Set_Target_Buffer_Sig,
    buffer_get_attached_child_process: Custom_Buffer_Get_Attached_Child_Process_Sig,
    child_process_get_attached_buffer: Custom_Child_Process_Get_Attached_Buffer_Sig,
    child_process_get_state: Custom_Child_Process_Get_State_Sig,
    enqueue_virtual_event: Custom_Enqueue_Virtual_Event_Sig,
    get_buffer_count: Custom_Get_Buffer_Count_Sig,
    get_buffer_next: Custom_Get_Buffer_Next_Sig,
    get_buffer_by_name: Custom_Get_Buffer_By_Name_Sig,
    get_buffer_by_file_name: Custom_Get_Buffer_By_File_Name_Sig,
    buffer_read_range: Custom_Buffer_Read_Range_Sig,
    buffer_replace_range: Custom_Buffer_Replace_Range_Sig,
    buffer_batch_edit: Custom_Buffer_Batch_Edit_Sig,
    buffer_seek_string: Custom_Buffer_Seek_String_Sig,
    buffer_seek_character_class: Custom_Buffer_Seek_Character_Class_Sig,
    buffer_line_y_difference: Custom_Buffer_Line_Y_Difference_Sig,
    buffer_line_shift_y: Custom_Buffer_Line_Shift_Y_Sig,
    buffer_pos_at_relative_xy: Custom_Buffer_Pos_At_Relative_XY_Sig,
    buffer_relative_box_of_pos: Custom_Buffer_Relative_Box_Of_Pos_Sig,
    buffer_padded_box_of_pos: Custom_Buffer_Padded_Box_Of_Pos_Sig,
    buffer_relative_character_from_pos: Custom_Buffer_Relative_Character_From_Pos_Sig,
    buffer_pos_from_relative_character: Custom_Buffer_Pos_From_Relative_Character_Sig,
    view_line_y_difference: Custom_View_Line_Y_Difference_Sig,
    view_line_shift_y: Custom_View_Line_Shift_Y_Sig,
    view_pos_at_relative_xy: Custom_View_Pos_At_Relative_XY_Sig,
    view_relative_box_of_pos: Custom_View_Relative_Box_Of_Pos_Sig,
    view_padded_box_of_pos: Custom_View_Padded_Box_Of_Pos_Sig,
    view_relative_character_from_pos: Custom_View_Relative_Character_From_Pos_Sig,
    view_pos_from_relative_character: Custom_View_Pos_From_Relative_Character_Sig,
    buffer_exists: Custom_Buffer_Exists_Sig,
    buffer_get_access_flags: Custom_Buffer_Get_Access_Flags_Sig,
    buffer_get_size: Custom_Buffer_Get_Size_Sig,
    buffer_get_line_count: Custom_Buffer_Get_Line_Count_Sig,
    push_buffer_base_name: Custom_Push_Buffer_Base_Name_Sig,
    push_buffer_unique_name: Custom_Push_Buffer_Unique_Name_Sig,
    push_buffer_file_name: Custom_Push_Buffer_File_Name_Sig,
    buffer_get_dirty_state: Custom_Buffer_Get_Dirty_State_Sig,
    buffer_set_dirty_state: Custom_Buffer_Set_Dirty_State_Sig,
    buffer_set_layout: Custom_Buffer_Set_Layout_Sig,
    buffer_clear_layout_cache: Custom_Buffer_Clear_Layout_Cache_Sig,
    buffer_get_layout: Custom_Buffer_Get_Layout_Sig,
    buffer_get_setting: Custom_Buffer_Get_Setting_Sig,
    buffer_set_setting: Custom_Buffer_Set_Setting_Sig,
    buffer_get_managed_scope: Custom_Buffer_Get_Managed_Scope_Sig,
    buffer_send_end_signal: Custom_Buffer_Send_End_Signal_Sig,
    create_buffer: Custom_Create_Buffer_Sig,
    buffer_save: Custom_Buffer_Save_Sig,
    buffer_kill: Custom_Buffer_Kill_Sig,
    buffer_reopen: Custom_Buffer_Reopen_Sig,
    buffer_get_file_attributes: Custom_Buffer_Get_File_Attributes_Sig,
    get_view_next: Custom_Get_View_Next_Sig,
    get_view_prev: Custom_Get_View_Prev_Sig,
    get_this_ctx_view: Custom_Get_This_Ctx_View_Sig,
    get_active_view: Custom_Get_Active_View_Sig,
    view_exists: Custom_View_Exists_Sig,
    view_get_buffer: Custom_View_Get_Buffer_Sig,
    view_get_cursor_pos: Custom_View_Get_Cursor_Pos_Sig,
    view_get_mark_pos: Custom_View_Get_Mark_Pos_Sig,
    view_get_preferred_x: Custom_View_Get_Preferred_X_Sig,
    view_set_preferred_x: Custom_View_Set_Preferred_X_Sig,
    view_get_screen_rect: Custom_View_Get_Screen_Rect_Sig,
    view_get_panel: Custom_View_Get_Panel_Sig,
    panel_get_view: Custom_Panel_Get_View_Sig,
    panel_is_split: Custom_Panel_Is_Split_Sig,
    panel_is_leaf: Custom_Panel_Is_Leaf_Sig,
    panel_split: Custom_Panel_Split_Sig,
    panel_set_split: Custom_Panel_Set_Split_Sig,
    panel_swap_children: Custom_Panel_Swap_Children_Sig,
    panel_get_root: Custom_Panel_Get_Root_Sig,
    panel_get_parent: Custom_Panel_Get_Parent_Sig,
    panel_get_child: Custom_Panel_Get_Child_Sig,
    view_close: Custom_View_Close_Sig,
    view_get_buffer_region: Custom_View_Get_Buffer_Region_Sig,
    view_get_buffer_scroll: Custom_View_Get_Buffer_Scroll_Sig,
    view_set_active: Custom_View_Set_Active_Sig,
    view_enqueue_command_function: Custom_View_Enqueue_Command_Function_Sig,
    view_get_setting: Custom_View_Get_Setting_Sig,
    view_Set_setting: Custom_View_Set_Setting_Sig,
    view_get_managed_scope: Custom_View_Get_Managed_Scope_Sig,
    buffer_compute_cursor: Custom_Buffer_Compute_Cursor_Sig,
    view_compute_cursor: Custom_View_Compute_Cursor_Sig,
    view_set_camera_bounds: Custom_View_Set_Camera_Bounds_Sig,
    view_get_camera_bounds: Custom_View_Get_Camera_Bounds_Sig,
    view_set_cursor: Custom_View_Set_Cursor_Sig,
    view_set_buffer_scroll: Custom_View_Set_Buffer_Scroll_Sig,
    view_set_mark: Custom_View_Set_Mark_Sig,
    view_quit_ui: Custom_View_Quit_UI_Sig,
    view_set_buffer: Custom_View_Set_Buffer_Sig,
    view_push_context: Custom_View_Push_Context_Sig,
    view_pop_context: Custom_View_Pop_Context_Sig,
    view_alter_context: Custom_View_Alter_Context_Sig,
    view_current_context: Custom_View_Current_Context_Sig,
    view_current_context_hook_memory: Custom_View_Current_Context_Hook_Memory_Sig,
    create_user_managed_scope: Custom_Create_User_Managed_Scope_Sig,
    destroy_user_managed_scope: Custom_Destroy_User_Managed_Scope_Sig,
    get_global_managed_scope: Custom_Get_Global_Managed_Scope_Sig,
    get_managed_scope_with_multiple_dependencies: Custom_Get_Managed_Scope_With_Multiple_Dependencies_Sig,
    managed_scope_clear_contents: Custom_Managed_Scope_Clear_Contents_Sig,
    managed_scope_clear_self_all_dependent_scopes: Custom_Managed_Scope_Clear_Self_All_Dependent_Scopes_Sig,
    managed_scope_allocator: Custom_Managed_Scope_Allocator_Sig,
    managed_id_group_highest_id: Custom_Managed_ID_Group_Highest_ID_Sig,
    managed_id_declare: Custom_Managed_ID_Declare_Sig,
    managed_id_get: Custom_Managed_ID_Get_Sig,
    managed_scope_get_attachment: Custom_Managed_Scope_Get_Attachment_Sig,
    managed_scope_attachment_erase: Custom_Managed_Scope_Attachment_Erase_Sig,
    alloc_managed_memory_in_scope: Custom_Alloc_Managed_Memory_In_Scope_Sig,
    alloc_buffer_markers_on_buffer: Custom_Alloc_Buffer_Markers_On_Buffer_Sig,
    managed_object_get_item_size: Custom_Managed_Object_Get_Item_Size_Sig,
    managed_object_get_item_count: Custom_Managed_Object_Get_Item_Count_Sig,
    managed_object_get_pointer: Custom_Managed_Object_Get_Pointer_Sig,
    managed_object_get_type: Custom_Managed_Object_Get_Type_Sig,
    managed_object_get_containing_scope: Custom_Managed_Object_Get_Containing_Scope_Sig,
    managed_object_free: Custom_Managed_Object_Free_Sig,
    managed_object_store_data: Custom_Managed_Object_Store_Data_Sig,
    managed_object_load_data: Custom_Managed_Object_Load_Data_Sig,
    get_next_input_raw: Custom_Get_Next_Input_Raw_Sig,
    get_current_input_sequence_number: Custom_Get_Current_Input_Sequence_Number_Sig,
    get_current_input: Custom_Get_Current_Input_Sig,
    set_current_input: Custom_Set_Current_Input_Sig,
    leave_current_input_unhandled: Custom_Leave_Current_Input_Unhandled_Sig,
    set_custom_hook: Custom_Set_Custom_Hook_Sig,
    get_custom_hook: Custom_Get_Custom_Hook_Sig,
    set_custom_hook_memory_size: Custom_Set_Custom_Hook_Memory_Size_Sig,
    get_mouse_state: Custom_Get_Mouse_State_Sig,
    get_active_query_bars: Custom_Get_Active_Query_Bars_Sig,
    start_query_bar: Custom_Start_Query_Bar_Sig,
    end_query_bar: Custom_End_Query_Bar_Sig,
    clear_all_query_bars: Custom_Clear_All_Query_Bars_Sig,
    print_message: Custom_Print_Message_Sig,
    log_string: Custom_Log_String_Sig,
    get_largest_face_id: Custom_Get_Largest_Face_ID_Sig,
    set_global_face: Custom_Set_Global_Face_Sig,
    buffer_history_get_max_record_index: Custom_Buffer_History_Get_Max_Record_Index_Sig,
    buffer_history_get_record_info: Custom_Buffer_History_Get_Record_Info_Sig,
    buffer_history_get_group_sub_record: Custom_Buffer_History_Get_Group_Sub_Record_Sig,
    buffer_history_get_current_state_index: Custom_Buffer_History_Get_Current_State_Index_Sig,
    buffer_history_set_current_state_index: Custom_Buffer_History_Set_Current_State_Index_Sig,
    buffer_history_merge_record_range: Custom_Buffer_History_Merge_Record_Range_Sig,
    buffer_history_clear_after_current_state: Custom_Buffer_History_Clear_After_Current_State_Sig,
    global_history_edit_group_begin: Custom_Global_History_Edit_Group_Begin_Sig,
    global_history_edit_group_end: Custom_Global_History_Edit_Group_End_Sig,
    buffer_set_face: Custom_Buffer_Set_Face_Sig,
    get_face_description: Custom_Get_Face_Description_Sig,
    get_face_metrics: Custom_Get_Face_Metrics_Sig,
    get_face_advance_map: Custom_Get_Face_Advance_Map_Sig,
    get_face_id: Custom_Get_Face_ID_Sig,
    try_create_new_face: Custom_Try_Create_New_Face_Sig,
    try_modify_face: Custom_Try_Modify_Face_Sig,
    try_release_face: Custom_Try_Release_Face_Sig,
    push_hot_directory: Custom_Push_Hot_Directory_Sig,
    set_hot_directory: Custom_Set_Hot_Directory_Sig,
    send_exit_signal: Custom_Send_Exit_Signal_Sig,
    hard_exit: Custom_Hard_Exit_Sig,
    set_window_title: Custom_Set_Window_Title_Sig,
    acquire_global_frame_mutex: Custom_Acquire_Global_Frame_Mutex_Sig,
    relase_global_frame_mutex: Custom_Release_Global_Frame_Mutex_Sig,
    draw_string_oriented: Custom_Draw_String_Oriented_Sig,
    get_string_advance: Custom_Get_String_Advance_Sig,
    draw_rectangle: Custom_Draw_Rectangle_Sig,
    draw_rectangle_outline: Custom_Draw_Rectangle_Outline_Sig,
    draw_set_clip: Custom_Draw_Set_Clip_Sig,
    text_layout_create: Custom_Text_Layout_Create_Sig,
    text_layout_region: Custom_Text_Layout_Region_Sig,
    text_layout_get_buffer: Custom_Text_Layout_Get_Buffer_Sig,
    text_layout_get_visible_range: Custom_Text_Layout_Get_Visible_Range_Sig,
    text_layout_line_on_screen: Custom_Text_Layout_Line_On_Screen_Sig,
    text_layout_character_on_screen: Custom_Text_Layout_Character_On_Screen_Sig,
    paint_text_color: Custom_Paint_Text_Color_Sig,
    paint_text_color_blend: Custom_Paint_Text_Color_Blend_Sig,
    text_layout_free: Custom_Text_Layout_Free_Sig,
    draw_text_layout: Custom_Draw_Text_Layout_Sig,
    open_color_picker: Custom_Open_Color_Picker_Sig,
    animate_in_n_milliseconds: Custom_Animate_In_N_Milliseconds_Sig,
    buffer_find_all_matches: Custom_Buffer_Find_All_Matches_Sig,
    get_core_profile_list: Custom_Get_Core_Profile_List_Sig,
    get_custom_layer_boundary_docs: Custom_Get_Custom_Layer_Boundary_Docs_Sig,
}

custom_api_read_vtable :: proc(vtable: ^API_VTable_custom) {
    mem.copy(&custom_vtable, vtable, size_of(API_VTable_custom));
}

custom_api_fill_vtable :: proc(vtable: ^API_VTable_custom) {
    assert(false) // dunno how to fill this
}