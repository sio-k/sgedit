package main

import "core:fmt"
import "core:mem"
import "core:os"
import rt "core:runtime"

import gl "vendor:OpenGL"
import "vendor:sdl2"

init_render_window :: proc() -> (ok: bool = true) {
    using system_globals
    window_extent: [4]i32 = { sdl2.WINDOWPOS_CENTERED, sdl2.WINDOWPOS_CENTERED, 640, 480 } // {x = x,y = y, z = w, w = h}
    if plat_settings.set_window_size == b8true {
        if plat_settings.window_w > 200 {
            window_extent.z = plat_settings.window_w
        }
        
        if plat_settings.window_h > 150 {
            window_extent.w = plat_settings.window_h
        }
    }
    
    if plat_settings.set_window_pos == b8true {
        if plat_settings.window_x >= 0 {
            window_extent.x = plat_settings.window_x
        }
        
        if plat_settings.window_y >= 0 {
            window_extent.y = plat_settings.window_y
        }
    }
    
    window_title = fmt.caprintf("sgedit v%d.%d.%d", MAJOR, MINOR, PATCH)
        
    window =
        sdl2.CreateWindow(
             window_title,
             window_extent.x, window_extent.y,
             window_extent.z, window_extent.w,
             { .RESIZABLE, .OPENGL, .SHOWN }
             )
        
    if window == nil {
        ok = false
        err_str := fmt.tprintln("Failed to create window. Got error:", sdl2.GetError())
        error_box(err_str, "Failed to create window", false)
        return
    }
    defer if !ok { sdl2.DestroyWindow(window) }
        
    // TODO (sio): best-guess, check if actually working
    // GL4.6 core, with back buffer
    sdl2.GL_SetAttribute(.CONTEXT_PROFILE_MASK, i32(sdl2.GLprofile.CORE))
    sdl2.GL_SetAttribute(.CONTEXT_MAJOR_VERSION, 4)
    sdl2.GL_SetAttribute(.CONTEXT_MINOR_VERSION, 6)
    sdl2.GL_SetAttribute(.DOUBLEBUFFER, 1) // turn on double buffering
  
    gl_context = sdl2.GL_CreateContext(window)
    if gl_context == nil {
        ok = false
        err_str :=
            fmt.tprintln("Failed to create GL context. Got error:", sdl2.GetError())
        error_box(err_str, "Failed to create OpenGL context", false)
        return
    }
    defer if !ok { sdl2.GL_DeleteContext(gl_context) }
        
    // set adaptive vsync if possible, standard vsync otherwise
    if sdl2.GL_SetSwapInterval(-1) < 0 {
        if sdl2.GL_SetSwapInterval(1) < 0 {
            // FIXME: vsync completely unsupported, this is an error for now
            error_box("vertical sync is unsupported on your system, please contact the developer so they implement an alternative method of synchronizing to about 60Hz", "vsync unsupported", false)
            ok = false
            return
        }
    }
    
    // TODO (sio): best-guess, check if actually working
    gl.load_up_to(4, 6, sdl2.gl_set_proc_address)
        
    
    // TODO: init fonts here instead of in app?
    
    init_render()
        
    // TODO: init font rendering here instead of in app?
        
    return
}

handle_window_event :: proc(window_event: ^sdl2.WindowEvent) {
    switch window_event.event {
        // unused/ignored events
        case .NONE, .MOVED, .FOCUS_GAINED, .FOCUS_LOST, .TAKE_FOCUS, .HIT_TEST: {
            log_os("Got window event of type", window_event.event, "; not handling it.")
        }
        
        case .ENTER: {
            system_globals.input.pers.mouse_out_of_window = false
        }
        
        case .LEAVE: {
            system_globals.input.pers.mouse_out_of_window = true
        }
        
        // TODO (sio) target framerate setting
        // need to figure out how that would interact with animations first before doing it
        case .SHOWN, .RESTORED, .MAXIMIZED: {
            // TODO (sio): increase target framerate to monitor refresh rate
        }
        
        case .HIDDEN, .MINIMIZED: {
            // TODO (sio): reduce target framerate to ~1fps to reduce CPU usage
        }
        
        case .EXPOSED: {
            // TODO (sio): SDL2 docs say:
            // *< Window has been exposed and should be redrawn
            // I've no idea what that maeans, and I should figure out what to do about this
        }
        
        case .RESIZED, .SIZE_CHANGED: {
            log_os("Window resize to", window_event.data1, "x", window_event.data2)
            render_target.width = window_event.data1
            render_target.height = window_event.data2
        }
        
        case .CLOSE: {
            system_globals.input.trans.trying_to_kill = true
            // TODO (sio): on multi-window, this doesn't mean a quit anymore, but for now
            // it does mean quit
        }
    }
}

destroy_render_window :: proc() {
    using system_globals
    // TODO (sio): renderer destroy? I think this all gets sorted out completely by just
    // deleting the GL context(s)
    
    sdl2.GL_DeleteContext(gl_context)
    gl_context = nil
        
    sdl2.DestroyWindow(window)
    window = nil
}

// NOTE (sio): following is heavily based on the old OpenGL renderer implementation
// by Allen Webster, currently with only minor modifications
// TODO (sio): shift all work to the GPU that we possibly can (and there's a lot)
// TODO (sio): get the vertex and fragment shaders out into separate files that we just
// load as part of renderer init (not so they can be changed more easily, but so they're
// highlighted properly and so I can develop them further)

bind_texture :: proc(t: ^Render_Target, texid: i32) {
    assert(texid >= 0)
    if t.bound_texture != u32(texid) {
        gl.BindTexture(gl.TEXTURE_2D_ARRAY, u32(texid))
        t.bound_texture = u32(texid)
    }
}

bind_any_texture :: proc(t: ^Render_Target) {
    if t.bound_texture == 0 {
        assert(t.fallback_texture_id != 0)
        gl.BindTexture(gl.TEXTURE_2D_ARRAY, t.fallback_texture_id)
        t.bound_texture = t.fallback_texture_id
    }
}



get_texture :: proc(dim: [3]i32, texture_kind: Texture_Kind) -> u32 {
    tex: u32 = 0
    gl.GenTextures(1, &tex)
    gl.BindTexture(gl.TEXTURE_2D_ARRAY, tex)
    gl.TexImage3D(
        gl.TEXTURE_2D_ARRAY, 0,
        gl.R8, dim.x, dim.y, dim.z, 0, gl.RED, gl.UNSIGNED_BYTE, nil
        )
    gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MAX_LEVEL, 0);
    return tex
}

fill_texture :: proc(
     texture_kind: Texture_Kind,
     texture: u32,
     p: [3]i32,
     dim: [3]i32,
     data: rawptr
     ) -> (result: b32)
{
    if texture != 0 {
        gl.BindTexture(gl.TEXTURE_2D_ARRAY, texture)
    }
    if dim.x > 0 && dim.y > 0 && dim.z > 0 {
        gl.PixelStorei(gl.UNPACK_ALIGNMENT, 1)
        gl.TexSubImage3D(
            gl.TEXTURE_2D_ARRAY,
            0,
            p.x, p.y, p.z,
            dim.x, dim.y, dim.z,
            gl.RED,
            gl.UNSIGNED_BYTE,
            data
            )
        result = b32true        
    }
    return
}



@(private="file")
error_callback :: proc "cdecl"(
    source: gl.GL_Enum,
    type: gl.GL_Enum,
    id: u32,
    severity: gl.GL_Enum,
    length: i32,
    message: cstring,
    userParam: rawptr
    ) {
    context = default_context()
    log_os(
        #procedure,
        "source =", source,
        ", type =", type,
        ", id =", id,
        ", severity =", severity,
        ", length =", length,
        ", message =", message,
        ", userParam =", userParam
        )
}

Shader_Attribute :: enum u32 {
    /* vec2 */ vertex_p,
    /* vec3 */ vertex_t,
    /* uint */ vertex_c,
    /* float */ vertex_ht,
}

Shader_Attributes :: [Shader_Attribute]u32

Shader_Uniform :: enum u32 {
    /* vec2 */ view_t,
    /* mat2x2 */ view_m,
    /* sampler2DArray */ sampler,
}

Shader_Uniforms :: [Shader_Uniform]u32

vertex_shader_source := #load("shaders/vert.glsl", cstring)
fragment_shader_source := #load("shaders/frag.glsl", cstring)

GL_Program :: struct {
    program: u32,
    attributes: Shader_Attributes,
    uniforms: Shader_Uniforms,
}

create_shader :: proc(source: cstring, type: u32) -> (result: u32) {
    result = gl.CreateShader(type)
    source_array := [1]cstring { source }
    source_lengths := [1]i32 { i32(len(string(source))) }
    gl.ShaderSource(
        result,
        1,
        transmute([^]cstring) &source_array,
        transmute([^]i32) &source_lengths
        );
    
    gl.CompileShader(result);
    print_shader_info_log(string(source), result);
    
    return;
}

print_shader_info_log :: proc(name: string, shader: u32) {
    success: b32 = gl.FALSE;
    gl.GetShaderiv(shader, gl.COMPILE_STATUS, transmute([^]i32) &success);
    if success == gl.TRUE {
        return;
    }
    
    ignore: i32 = 0;
    errors: [rt.Kilobyte * 4]u8;
    gl.GetShaderInfoLog(shader, rt.Kilobyte * 4 - 1, &ignore, transmute([^]u8) &errors);
    fmt.eprintln(name, "info log");
    fmt.eprintln(cstring(transmute([^]u8) &errors));
}

make_program :: proc(
    vertex_source: cstring,
    fragment_source: cstring
    ) -> (result: GL_Program)
{
    using result
    
    vertex_shader: u32 = create_shader(vertex_source, gl.VERTEX_SHADER)
    fragment_shader: u32 = create_shader(fragment_source, gl.FRAGMENT_SHADER)
    
    program = gl.CreateProgram()
    gl.AttachShader(program, vertex_shader)
    gl.AttachShader(program, fragment_shader)
    gl.LinkProgram(program)
    gl.ValidateProgram(program)

    success: b32 = gl.FALSE
     
    gl.GetProgramiv(program, gl.VALIDATE_STATUS, transmute([^]i32) &success)
        
    if success == gl.FALSE {
        fmt.eprintln("Shader program failed to validate");
        // TODO: validation log?
    } else { // let's see if there's any more errors
        gl.GetProgramiv(program, gl.LINK_STATUS, transmute([^]i32) &success)
    }
    if success == gl.FALSE { // failed to compile
        ignore: i32 = 0
        
        BUFFER_SIZE :: rt.Kilobyte * 4
        STRING_SIZE :: BUFFER_SIZE - 1 // ensure terminating null byte
        
        program_errors: [BUFFER_SIZE]u8
        
        gl.GetProgramInfoLog(program, STRING_SIZE, &ignore, transmute([^]u8) &program_errors)
            
        fmt.eprintln("Vertex shader", vertex_source, sep="\n");
        fmt.eprintln("Fragment shader", fragment_source, sep="\n");
            
        fmt.eprintln(
            "",
            "=====================",
            "Vertex shader errors:",
            "=====================",
            sep = "\n"
            )
        print_shader_info_log("vertex", vertex_shader)

        
        fmt.eprintln(
            "",
            "=======================",
            "Fragment shader errors:",
            "=======================",
            sep = "\n"
            )
        print_shader_info_log("fragment", fragment_shader)
        
        fmt.eprintln(
            "",
            "===============",
            "Program errors:",
            "===============",
            sep = "\n"
            )
        fmt.eprintln(cstring(transmute([^]u8) &program_errors))
        
        error_box("Shader compilation failed. See stderr for details.")
    }
    
    gl.DeleteShader(fragment_shader)
    gl.DeleteShader(vertex_shader)
    
    attributes[.vertex_p] = u32(gl.GetAttribLocation(program, "vertex_p"))
    attributes[.vertex_t] = u32(gl.GetAttribLocation(program, "vertex_t"))
    attributes[.vertex_c] = u32(gl.GetAttribLocation(program, "vertex_c"))
    attributes[.vertex_ht] = u32(gl.GetAttribLocation(program, "vertex_ht"))
    
    uniforms[.view_t] = u32(gl.GetUniformLocation(program, "view_t"))
    uniforms[.view_m] = u32(gl.GetUniformLocation(program, "view_m"))
    uniforms[.sampler] = u32(gl.GetUniformLocation(program, "sampler"))
    
    return
}

@(private="file")
attribute_buffer: u32 = 0

@(private="file")
gpu_program: GL_Program

// replaces first_opengl_call
init_render :: proc() {
    using render_target
    
    when !(#defined(SHIP_MODE)) {
        DO_DEBUG :: true
    } else when SHIP_MODE == false {
        DO_DEBUG :: true
    } else {
        DO_DEBUG :: false
    }

    when DO_DEBUG {
        gl.Enable(gl.DEBUG_OUTPUT)
        gl.Enable(gl.DEBUG_OUTPUT_SYNCHRONOUS)
        
        gl.DebugMessageControl(
            gl.DONT_CARE, gl.DONT_CARE,
            gl.DEBUG_SEVERITY_NOTIFICATION, 0, nil, false
            )
        gl.DebugMessageControl(
            gl.DONT_CARE, gl.DONT_CARE,
            gl.DEBUG_SEVERITY_LOW, 0, nil, false
            )
        gl.DebugMessageControl(
            gl.DONT_CARE, gl.DONT_CARE,
            gl.DEBUG_SEVERITY_MEDIUM, 0, nil, true
            )
        gl.DebugMessageControl(
            gl.DONT_CARE, gl.DONT_CARE,
            gl.DEBUG_SEVERITY_HIGH, 0, nil, true
            )
        
        // TODO (sio): this is fine, but should still not be necessary
        gl.DebugMessageCallback(transmute(gl.debug_proc_t) error_callback, nil)
    }
    
    dummy_vao: u32 = 0
    gl.GenVertexArrays(1, transmute([^]u32) &dummy_vao)
    gl.BindVertexArray(dummy_vao)
    
    gl.GenBuffers(1, transmute([^]u32) &attribute_buffer)
    gl.BindBuffer(gl.ARRAY_BUFFER, attribute_buffer)
       
    gl.Enable(gl.SCISSOR_TEST)
    gl.Enable(gl.BLEND)
    gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
    
    gpu_program = make_program(vertex_shader_source, fragment_shader_source)
    gl.UseProgram(gpu_program.program)
    
    fallback_texture_id = get_texture([3]i32 { 2, 2, 1 }, .Mono)
    white_block := [?]u8 { 0xFF, 0xFF, 0xFF, 0xFF }
    fill_texture(
        .Mono,
        0,
        [3]i32 { 0, 0, 0 },
        [3]i32 { 2, 2, 1 },
        transmute(rawptr) &white_block
        )
}

render :: proc(t: ^Render_Target) {
    using t
    
    gl.Viewport(0, 0, width, height)
    gl.Scissor(0, 0, width, height)
    gl.ClearColor(1.0, 0.0, 1.0, 1.0)
    gl.Clear(gl.COLOR_BUFFER_BIT)
    
    gl.BindTexture(gl.TEXTURE_2D, 0)
    bound_texture = 0
    
    for free_texture: ^Render_Free_Texture = free_texture_first;
        free_texture != nil;
        free_texture = free_texture.next
    {
        gl.DeleteTextures(1, transmute([^]u32) &(free_texture.tex_id))
    }
    
    free_texture_first = nil
    free_texture_last = nil
    
    for group: ^Render_Group = group_first;
        group != nil;
        group = group.next
    {
        box := Rect(i32) {
            x0 = i32(group.clip_box.x0), y0 = i32(group.clip_box.y0),
            x1 = i32(group.clip_box.x1), y1 = i32(group.clip_box.y1)
        }
        
        scissor_box := Rect(i32) {
            box.x0,          height - box.y1,
            box.x1 - box.x0, box.y1 - box.y0
        }
        
        scissor_box.x0 = max(0, scissor_box.x0)
        scissor_box.y0 = max(0, scissor_box.y0)
        scissor_box.x1 = max(0, scissor_box.x1)
        scissor_box.y1 = max(0, scissor_box.y1)
        gl.Scissor(
            scissor_box.x0,
            scissor_box.y0,
            scissor_box.x1,
            scissor_box.y1
            )
        
        vertex_count: i32 = group.vertex_list.vertex_count
        if vertex_count < 1 {
            continue
        }
        
        face: ^Face = font_set_face_from_id(font_set, group.face_id) // TODO: font_set_face_from_id
        if face != nil {
            bind_texture(t, i32(face.texture))
        } else {
            bind_any_texture(t)
        }
        
        gl.BufferData(
            gl.ARRAY_BUFFER,
            int( vertex_count * size_of(Render_Vertex)),
            nil,
            gl.STREAM_DRAW
            )
        cursor: i32 = 0
        for node: ^Render_Vertex_Array_Node = group.vertex_list.first;
            node != nil;
            node = node.next
        {
            size: i32 = node.vertex_count * size_of(Render_Vertex)
            gl.BufferSubData(gl.ARRAY_BUFFER, int(cursor), int(size), node.vertices)
            cursor += size
        }
        
        for attrib in gpu_program.attributes {
            gl.EnableVertexAttribArray(u32(attrib))
        }
        
        gl.VertexAttribPointer(
            gpu_program.attributes[.vertex_p], 2, gl.FLOAT, true,
            size_of(Render_Vertex), offset_of(Render_Vertex, xy)
            )
        gl.VertexAttribPointer(
            gpu_program.attributes[.vertex_t], 3, gl.FLOAT, true,
            size_of(Render_Vertex), offset_of(Render_Vertex, uvw)
            )
        gl.VertexAttribIPointer(
            gpu_program.attributes[.vertex_c], 1, gl.UNSIGNED_INT,
            size_of(Render_Vertex), offset_of(Render_Vertex, color)
            )
        gl.VertexAttribPointer(
            gpu_program.attributes[.vertex_ht], 1, gl.FLOAT, true,
            size_of(Render_Vertex), offset_of(Render_Vertex, half_thickness)
            )
        
        gl.Uniform2f(
            i32(gpu_program.uniforms[.view_t]),
            f32(width) / 2.0,
            f32(height) / 2.0
            )
        m := [4]f32 {
            2.0 / f32(width), 0.0,
            0.0, -2.0 / f32(height),
        }
        gl.UniformMatrix2fv(
            i32(gpu_program.uniforms[.view_m]),
            1,
            gl.FALSE,
            transmute([^]f32) &m
            )
        gl.Uniform1i(i32(gpu_program.uniforms[.sampler]), 0)
        
        gl.DrawArrays(gl.TRIANGLES, 0, vertex_count)
        
        for attrib in gpu_program.attributes {
            gl.DisableVertexAttribArray(attrib)
        }
    }
    
    gl.Flush();
}

