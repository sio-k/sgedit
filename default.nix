rec {
    pkgs   = import <nixpkgs> {};
    stdenv = pkgs.stdenv;

    # FIXME: need to put in all libs we actually need
    pkg-config     = pkgs.pkg-config;              # used by cmake
    binutils       = pkgs.binutils;                # used for build process
    gnumake        = pkgs.gnumake;                 # used by cmake
    clang          = pkgs.clang_6;                 # C/C++ compiler
    odin           = pkgs.odin;                    # Odin compiler
    ispc           = pkgs.ispc;                    # ISPC C compiler, based on LLVM (TODO: use this to speed up critical path stuff)
    cmake          = pkgs.cmake;                   # Build system generator
    SDL2           = pkgs.SDL2;                    # SDL2 comprehensive system abstraction layer
    ninja          = pkgs.ninja;                   # used by cmake
    compiler-rt = pkgs.llvmPackages.compiler-rt;

    # TODO: switch to stb_truetype.h
    freetype = pkgs.freetype;

    sgedit = stdenv.mkDerivation rec {
        inherit stdenv pkg-config binutils;
        inherit gnumake cmake ninja;
        inherit clang odin ispc;
        inherit SDL2;
        inherit compiler-rt;
        inherit freetype;
        buildInputs = [ stdenv pkg-config binutils
                        gnumake cmake ninja
                        clang odin ispc
                        SDL2
                        compiler-rt
                        freetype
                      ];
        name = "sgedit";
        description = "Graphical editor based on 4coder open source release";
        builder = ./nix-builder.sh;
        src = ./.;
    };
}
